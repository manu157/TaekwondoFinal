package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by jerin android on 30-01-2018.
 */

public class SplashScreen extends AppCompatActivity {
ImageView imagev;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.splashscreen);

        imagev=(ImageView)findViewById(R.id.imag) ;

      imagev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(SplashScreen.this,
                        IntroActivity.class);
                ActivityOptionsCompat optionsCompat =ActivityOptionsCompat.makeSceneTransitionAnimation(SplashScreen.this,imagev,"myimage");

                    startActivity(myIntent,optionsCompat.toBundle());

            }
        });



    }
}
