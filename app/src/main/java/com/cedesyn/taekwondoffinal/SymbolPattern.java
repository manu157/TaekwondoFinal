package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by jerin android on 03-02-2018.
 */

public class SymbolPattern extends AppCompatActivity {
    public String parent,child;
    TextView patternname;
    ImageView iiw;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.symbolpattern);
        iiw=(ImageView)findViewById(R.id.pattern_id);
        patternname=findViewById(R.id.pattern_name);
        Intent ii = getIntent();
        parent= ii.getStringExtra("parentpos");
        SharedPreferences sp = getDefaultSharedPreferences(getApplicationContext());
        final String parp=sp.getString("parentpos","");
        final String childp=sp.getString("childpos","");
      //  Toast.makeText(this, ""+parp, Toast.LENGTH_SHORT).show();
        switch (parp){

            case "0":
              iiw.setImageResource(R.drawable.cross);
              patternname.setText("White Belt 10'th kup");
                break;
            case "1":
               iiw.setImageResource(R.drawable.cross);
                patternname.setText("Yellow Stripe 9'th kup");
                break;
            case "2":
                iiw.setImageResource(R.drawable.iii);
                patternname.setText("Yellow Belt 8'th kup");
                break;
            case "3":
                iiw.setImageResource(R.drawable.lamda);
                patternname.setText("Green Stripe 7'th kup");
                break;
            case "4":
                iiw.setImageResource(R.drawable.iii);
                patternname.setText("Green Belt 6'th kup");
                break;
            case "5":
                iiw.setImageResource(R.drawable.plusminus);
                patternname.setText("Blue Stripe 5'th kup");
                break;
            case "6":
                iiw.setImageResource(R.drawable.iii);
                patternname.setText("Blue Belt 4'th kup");
                break;
            case "7":
                iiw.setImageResource(R.drawable.plusminus);
                patternname.setText("Red Stripe 3'rd kup");
                break;
            case "8":
                iiw.setImageResource(R.drawable.iii);
                patternname.setText("Red Belt 2'nd kup");
                break;
            case "9":
                iiw.setImageResource(R.drawable.iii);
                patternname.setText("Black Stripe 1'st kup");
                break;
            case "10":
                iiw.setImageResource(R.drawable.plusminus);
                patternname.setText("1'st Kup to 1'st Degree Black Belt");
                break;



        }
    }
}
