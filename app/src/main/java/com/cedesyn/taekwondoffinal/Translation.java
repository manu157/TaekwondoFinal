package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListView;

import java.util.ArrayList;

/**
 * Created by jerin android on 01-02-2018.
 */

public class Translation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.translate);

        //THE EXPANDABLE
        final ExpandableListView trans = (ExpandableListView) findViewById(R.id.translate);
        final ArrayList<Ctrans> ctrans =getData();


        final CustomTransAdapter transper =new CustomTransAdapter(Translation.this,ctrans);
         trans.setAdapter(transper);
    }



    private ArrayList<Ctrans> getData(){

        Ctrans t1 =new Ctrans("Counting");
        t1.translations.add("ONE");
        t1.translations.add("TWO");
        t1.translations.add("THREE");
        t1.translations.add("FOUR");
        t1.translations.add("FIVE");
        t1.translations.add("SIX");
        t1.translations.add("SEVEN");
        t1.translations.add("EIGHT");
        t1.translations.add("NINE");
        t1.translations.add("TEN");

        Ctrans t2 =new Ctrans("Body Section");
        t2.translations.add("HIGH   ");
        t2.translations.add("MIDDLE ");
        t2.translations.add("LOW ");



        Ctrans t3 =new Ctrans("Stance");
        t3.translations.add("PARALLEL");
        t3.translations.add("CLOSED");
        t3.translations.add("WALKING");
        t3.translations.add("L");
        t3.translations.add("X");
        t3.translations.add("SITTING");
        t3.translations.add("FIXED");
        t3.translations.add("ATTENTION");
        t3.translations.add("READY");
        t3.translations.add("BENDING");
        t3.translations.add("LOW");



        Ctrans t4 =new Ctrans("Action");
        t4.translations.add("THRUST");
        t4.translations.add("STRIKE");
        t4.translations.add("BLOCK");
        t4.translations.add("PUNCH");
        t4.translations.add("KICK");
        t4.translations.add("OUTSIDE");
        t4.translations.add("INSIDE");
        t4.translations.add("OUTWARD");
        t4.translations.add("INWARD");
        t4.translations.add("RISING");
        t4.translations.add("GUARDING");
        t4.translations.add("DOUBLE");
        t4.translations.add("TWIN");
        t4.translations.add("WEDGING");
        t4.translations.add("CIRCULAR");
        t4.translations.add("U-SHAPE");
        t4.translations.add("VERTICAL");
        t4.translations.add("PRESSING");
        t4.translations.add("PUSHING");
        t4.translations.add("W-SHAPE");
        t4.translations.add("FLAT");



        Ctrans t5 =new Ctrans("Punches");
        t5.translations.add("OBVERSE");
        t5.translations.add("REVERSE");
        t5.translations.add("UPWARD");
        t5.translations.add("UPSET");
        t5.translations.add("ANGLE");



        Ctrans t6 =new Ctrans("Kicks");
        t6.translations.add("RISING");
        t6.translations.add("PIERCING");
        t6.translations.add("THRUSTING");
        t6.translations.add("SNAP");
        t6.translations.add("HOOKING");
        t6.translations.add("TURNING");
        t6.translations.add("REVERSE");
        t6.translations.add("TWISTING");
        t6.translations.add("CRESCENT");
        t6.translations.add("DOWNWARD");
        t6.translations.add("CONSECUTIVE");


        Ctrans t7 =new Ctrans("Bodyparts");
        t7.translations.add("FIST ");
        t7.translations.add("KNIFEHAND");
        t7.translations.add("FINGERTIP");
        t7.translations.add("ELBOW");
        t7.translations.add("FOREARM");
        t7.translations.add("PALM");
        t7.translations.add("SOLE");
        t7.translations.add("FOOT");
        t7.translations.add("BACK HEEL");
        t7.translations.add("KNEE");


        Ctrans t8 =new Ctrans("Sparring");
        t8.translations.add("FREE");
        t8.translations.add("1-STEP");
        t8.translations.add("2-STEP");
        t8.translations.add("3-STEP");

        ArrayList<Ctrans> translates =new ArrayList<Ctrans>();
    translates.add(t1);
        translates.add(t2);
        translates.add(t3);
        translates.add(t4);
        translates.add(t5);
        translates.add(t6);
        translates.add(t7);
        translates.add(t8);


    return translates;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Translation.this,MainActivity.class));
        finish();
    }
}