package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by jerin android on 12-02-2018.
 */

public class Show extends AppCompatActivity {
    Button btn;
    ImageView img1,img2;
    int count=1;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show);

        snackbar0=(ConstraintLayout)findViewById(R.id.snacks0);

        btn=(Button)findViewById(R.id.ans);


        ques=(TextView)findViewById(R.id.qstn);
        ans1=(TextView) findViewById(R.id.o1);
        ans2=(TextView) findViewById(R.id.o2);
        ans3=(TextView) findViewById(R.id.o3);
        ans4=(TextView) findViewById(R.id.o4);

        img1=(ImageView)findViewById(R.id.ryt);

        img2=(ImageView)findViewById(R.id.left);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);


        count =1;
        ques.setText("Where does Taekwon-Do come from?");
        ans1.setText("Korea");
        ans2.setText("France");
        ans3.setText("Spain");
        ans4.setText("UK");




        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
;
          if (count==1){
              Snackbar snackbar = Snackbar.make(snackbar0, "Korea ", Snackbar.LENGTH_LONG);
               snackbar.show();
          }

             if(count==2){
                 Snackbar snackbar = Snackbar.make(snackbar0, "Forefist", Snackbar.LENGTH_LONG);
                 snackbar.show();

            } if(count==3){
                    Snackbar snackbar = Snackbar.make(snackbar0, "Dobok", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } if(count==4){
                    Snackbar snackbar = Snackbar.make(snackbar0, "Dojang", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } if(count==5){
                    Snackbar snackbar = Snackbar.make(snackbar0, "Eyes/Mind/body", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } if(count==6){
                    Snackbar snackbar = Snackbar.make(snackbar0, "Speed", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } if(count==7){
                    Snackbar snackbar = Snackbar.make(snackbar0, "Improve fitness", Snackbar.LENGTH_LONG);
                    snackbar.show();

                } if(count==8) {
                    Snackbar snackbar = Snackbar.make(snackbar0, "foot/hand/way", Snackbar.LENGTH_LONG);
                    snackbar.show();

                }

        }
});

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               count=count+1;
           //     Toast.makeText(Show.this, ""+String.valueOf(count), Toast.LENGTH_SHORT).show();

                if(count==1){
                   ques.setText("Where does Taekwon-Do come from?");
                   ans1.setText("Korea");
                   ans2.setText("France");
                   ans3.setText("Spain");
                   ans4.setText("UK");

               } if(count==2){
                    ques.setText("What part of the fist do we use to punch a pad with? ");
                    ans1.setText("Forefist");
                    ans2.setText("fingerstips");
                    ans3.setText("knuckles");
                    ans4.setText("wrist");

                } if(count==3){
                    ques.setText("What is your training suit in Korean");
                    ans1.setText("Dobok");
                    ans2.setText("Dojang");
                    ans3.setText("Gee");
                    ans4.setText("Tracksuit");

                } if(count==4){
                    ques.setText(" What is the training hall in Korean?");
                    ans1.setText("Base");
                    ans2.setText("Suite");
                    ans3.setText("Dojang");
                    ans4.setText("Makgi");

                } if(count==5){
                    ques.setText(" What are the three rules of concentration?");
                    ans1.setText("Eyes/Mind/body");
                    ans2.setText("head/eyes/hands");
                    ans3.setText("eyes/mind/head");
                    ans4.setText("None the above");

                } if(count==6){
                    ques.setText("Can you provide one method to get power in our movements?");
                    ans1.setText("Speed");
                    ans2.setText("slow movements");
                    ans3.setText("poor aim");
                    ans4.setText("lose fist");

                } if(count==7){
                    ques.setText("Can you provide one benefit from doing Taekwon-Do?");
                    ans1.setText(" Improve fitness");
                    ans2.setText("learn nothing");
                    ans3.setText("fight with others");
                    ans4.setText("learn nothing");

                } if(count==8) {
                    img1.setVisibility(View.GONE);
                    ques.setText("What are Tae, Kwon and Do in English?");
                    ans1.setText("foot/hand/way");
                    ans2.setText("foot/hand/time");
                    ans3.setText("hand/time/way");
                    ans4.setText("foot/way/try");

//                    img1.setClickable(false);
//                    img2.setClickable(true);
                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }


            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
           //     Toast.makeText(Show.this, ""+String.valueOf(count), Toast.LENGTH_SHORT).show();
                if(count==1){
                    img2.setVisibility(View.GONE);
                    ques.setText("Where does Taekwon-Do come from?");
                    ans1.setText("Korea");
                    ans2.setText("France");
                    ans3.setText("Spain");
                    ans4.setText("UK");

                } if(count==2){
                    ques.setText("What part of the fist do we use to punch a pad with? ");
                    ans1.setText("Forefist");
                    ans2.setText("fingerstips");
                    ans3.setText("knuckles");
                    ans4.setText("wrist");

                } if(count==3){
                    ques.setText("What is your training suit in Korean");
                    ans1.setText("Dobok");
                    ans2.setText("Dojang");
                    ans3.setText("Gee");
                    ans4.setText("Tracksuit");

                } if(count==4){
                    ques.setText(" What is the training hall in Korean?");
                    ans1.setText("Base");
                    ans2.setText("Suite");
                    ans3.setText("Dojang");
                    ans4.setText("Makgi");

                } if(count==5){
                    ques.setText(" What are the three rules of concentration?");
                    ans1.setText("Eyes/Mind/body");
                    ans2.setText("head/eyes/hands");
                    ans3.setText("eyes/mind/head");
                    ans4.setText("None the above");

                } if(count==6){
                    ques.setText("Can you provide one method to get power in our movements?");
                    ans1.setText("Speed");
                    ans2.setText("slow movements");
                    ans3.setText("poor aim");
                    ans4.setText("lose fist");

                } if(count==7){
                    ques.setText("Can you provide one benefit from doing Taekwon-Do?");
                    ans1.setText(" Improve fitness");
                    ans2.setText("learn nothing");
                    ans3.setText("fight with others");
                    ans4.setText("learn nothing");

                } if(count==8) {
                    ques.setText("What are Tae, Kwon and Do in English?");
                    ans1.setText("foot/hand/way");
                    ans2.setText("foot/hand/time");
                    ans3.setText("hand/time/way");
                    ans4.setText("foot/way/try");
//                    img2.setClickable(false);
//                    img1.setClickable(true);
                }
                if(count >=8){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);
                }


            }
        });


    }
}
