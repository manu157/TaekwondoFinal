package com.cedesyn.taekwondoffinal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jerin android on 28-02-2018.
 */

class CustomTransAdapter extends BaseExpandableListAdapter {

    private Context c;
    private ArrayList<Ctrans> ctrans;
    private LayoutInflater inflater;


    public CustomTransAdapter(Context c, ArrayList<Ctrans> translate) {
        this.c =c;
        this.ctrans = translate;
        inflater =(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Ctrans getGroupCount(int groupPos) {
        return ctrans.get(groupPos);
    }


    @Override
    public int getGroupCount() {
        return ctrans.size();
    }

    @Override
    public int getChildrenCount(int groupPosw) {
        return ctrans.get(groupPosw).translations.size();
    }

    @Override
    public Object getGroup(int groupPos) {
        return ctrans.get(groupPos);
    }

    @Override
    public Object getChild(int groupPos, int childPos) {
        return ctrans.get(groupPos).translations.get(childPos) ;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int arg0, int arg1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        //ONLY INFLATE XML TEAM ROW MODEL IF ITS NOT PRESENT,OTHERWISE REUSE IT
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.ptrans, null);
        }

        //GET GROUP/TEAM ITEM
        Ctrans t = (Ctrans) getGroup(groupPosition);

        //SET GROUP NAME
        TextView nameTv = (TextView) convertView.findViewById(R.id.ptranslate);
      //  ImageView img = (ImageView) convertView.findViewById(R.id.imageview1);

        String name = t.Names;
        nameTv.setText(name);

        //ASSIGN TEAM IMAGES ACCORDING TO TEAM NAME



        //SET TEAM ROW BACKGROUND COLOR
      //  convertView.setBackgroundColor(Color.WHITE);

        return convertView;
    }

    @Override
    public View getChildView(int groupPos, int childPos, boolean isLastChild, View convertView, ViewGroup parent) {

        if (convertView==null){
            convertView =inflater.inflate(R.layout.ctranslation,null);
        }

    String child =(String) getChild(groupPos,childPos);
        TextView transp =(TextView) convertView.findViewById(R.id.translations);
        TextView transp1 =(TextView) convertView.findViewById(R.id.translations1);

        transp.setText(child);

        String teamName =getGroup(groupPos).toString();
        if(teamName =="Counting"){
            if(child=="ONE"){
                transp1.setText("HANA");
            }
            else if(child=="TWO"){
                transp1.setText("DOOL");
            }
            else if(child=="THREE"){
                transp1.setText("SET");
            }else if(child=="FOUR"){
                transp1.setText("NET");
            }else
            if(child=="FIVE"){
                transp1.setText("DASOT");
            }else
            if(child=="SIX"){
                transp1.setText("YASOT");
            }else
            if(child=="SEVEN"){
                transp1.setText("ILGOP");
            }else if(child=="EIGHT"){
                transp1.setText("YODUL");
            }else
            if(child=="NINE"){
                transp1.setText("AHOP");
            }else
            if(child=="TEN"){
                transp1.setText("YOL");
            }

        }else if(teamName =="Body Section"){
            if(child=="HIGH"){
                transp1.setText("NOPUNDE");
            }
            if(child=="MIDDLE"){
                transp1.setText("KAUNDE");
            }
            if(child=="LOW"){
                transp1.setText("NAJUNDE");
            }
        }else if(teamName =="Stance"){
            if(child=="PARALLEL"){
                transp1.setText("NARANI");
            }
            if(child=="CLOSED"){
                transp1.setText("MOA");
            }
            if(child=="WALKING"){
                transp1.setText("GUNNUN");
            }if(child=="L"){
                transp1.setText("NIUNJA");
            }
            if(child=="X"){
                transp1.setText("KYOCHA");
            }
            if(child=="SITTING"){
                transp1.setText("ANNUN");
            }
            if(child=="FIXED"){
                transp1.setText("GOJUNG");
            }
            if(child=="ATTENTION"){
                transp1.setText("CHARYOT");
            }
            if(child=="READY"){
                transp1.setText("JUNBI");
            }if(child=="BENDING"){
                transp1.setText("GOBURYO");
            }
            if(child=="LOW"){
                transp1.setText("NACHU");
            }

        }else if(teamName =="Action"){
            if(child=="THRUST"){
                transp1.setText("TULGI");
            }
            if(child=="STRIKE"){
                transp1.setText("TAERIGI");
            }
            if(child=="BLOCK"){
                transp1.setText("MAKGI");
            }if(child=="PUNCH"){
                transp1.setText("JIRUGI");
            }
            if(child=="KICK"){
                transp1.setText("CHAGI");
            }
            if(child=="OUTSIDE"){
                transp1.setText("BAKAT");
            }
            if(child=="INSIDE"){
                transp1.setText("AN");
            }
            if(child=="OUTWARD"){
                transp1.setText("BAKURO");
            }
            if(child=="INWARD"){
                transp1.setText("ANURO");
            }if(child=="RISING"){
                transp1.setText("CHUKYO");
            }
            if(child=="GUARDING"){
                transp1.setText("DAEBI");
            }
            if(child=="DOUBLE"){
                transp1.setText("DOO");
            }
            if(child=="TWIN"){
                transp1.setText("SANG");
            }
            if(child=="WEDGING"){
                transp1.setText("HECHYO");
            }
            if(child=="CIRCULAR"){
                transp1.setText("DOLLIMYO");
            }
            if(child=="U-SHAPE"){
                transp1.setText("DIGUTIA");
            }
            if(child=="VERTICAL"){
                transp1.setText("SEWO");
            }
            if(child=="PRESSING"){
                transp1.setText("MOOLYO");
            }
            if(child=="PUSHING"){
                transp1.setText("MIRO");
            }
            if(child=="W-SHAPE"){
                transp1.setText("SAN");
            }  if(child=="FLAT"){
                transp1.setText("OPUN");
            }
        }else if(teamName =="Punches"){
            if(child=="OBVERSE"){
                transp1.setText("BARO");
            }
            if(child=="REVERSE"){
                transp1.setText("BANDAE");
            }
            if(child=="UPWARD"){
                transp1.setText("OLLYO");
            }if(child=="UPSET"){
                transp1.setText("DWIJIBO");
            }
            if(child=="ANGLE"){
                transp1.setText("GIOKJA");
            }

        }else if(teamName =="Kicks"){
            if(child=="RISING"){
                transp1.setText("CHA OLLIGI");
            }
            if(child=="PIERCING"){
                transp1.setText("CHA JIRUGI");
            }
            if(child=="THRUSTING"){
                transp1.setText("CHA TULGI");
            }if(child=="SNAP"){
                transp1.setText("CHA BUSIGI");
            }
            if(child=="HOOKING"){
                transp1.setText("GOLCHA");
            }  if(child=="TURNING"){
                transp1.setText("DOLLYO");
            }  if(child=="REVERSE"){
                transp1.setText(" BANDAE DOLLYO");
            }  if(child=="TWISTING"){
                transp1.setText("BITURO");
            }  if(child=="CRESCENT"){
                transp1.setText("BANDAL");
            }
            if(child=="DOWNWARD"){
                transp1.setText("NAERO");
            }
            if(child=="CONSECUTIVE"){
                transp1.setText("YONSUK");
            }

        }else if(teamName =="Bodyparts"){
            if(child=="FIST"){
                transp1.setText("AP JOOMUK");
            }
            if(child=="KNIFEHAND"){
                transp1.setText("SONKAL");
            }
            if(child=="FINGERTIP"){
                transp1.setText("SONKUT");
            }if(child=="ELBOW"){
                transp1.setText("PALKUP (WI)");
            }
            if(child=="FOREARM"){
                transp1.setText("PALMOK");
            }  if(child=="PALM"){
                transp1.setText("SONBADAK");
            }  if(child=="SOLE"){
                transp1.setText("KUMCHEE");
            }  if(child=="FOOT"){
                transp1.setText("BAL");
            }  if(child=="BACK HEEL"){
                transp1.setText("DWICHOOK");
            }

        }else if(teamName =="Sparring"){
            if(child=="FREE"){
                transp1.setText("JAO");
            }
            if(child=="1-STEP"){
                transp1.setText("ILBO");
            }
            if(child=="2-STEP"){
                transp1.setText("IBO");
            }if(child=="3-STEP"){
                transp1.setText("SAMBO");
            }

        }


        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
