package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show7 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar7;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show7);
        snackbar7=(ConstraintLayout)findViewById(R.id.snacks7);

        btn=(Button)findViewById(R.id.ans7);


        ques=(TextView)findViewById(R.id.qstn7);
        ans1=(TextView) findViewById(R.id.o17);
        ans2=(TextView) findViewById(R.id.o27);
        ans3=(TextView) findViewById(R.id.o37);
        ans4=(TextView) findViewById(R.id.o47);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("BLUE – SIGNIFIES What?");
        ans1.setText("THE HEAVENS");
        ans2.setText("DANGER");
        ans3.setText("PASSION");
        ans4.setText("BEGINNER");



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "THE HEAVENS ", Snackbar.LENGTH_LONG);
                    snackBar7.show();
                }

                if(count==2){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "DANGER", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                } if(count==3){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "37", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                } if(count==4){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Yi-Hwang", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                } if(count==5){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Neo-Confucianism", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                } if(count==6){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Birthplace on the 37degree latitude", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                } if(count==7){
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Scholar", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                } if(count==8) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "I", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }
                if(count==9) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Dwijibun Sonkut Tulgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }
                if(count==10) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Lower Abdomen", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==11) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Sang Yop Palkup Tulgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==12) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "San makgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==13) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Kyocha Joomuk Noollo makgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }
                if(count==14) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Sang Ap Makgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }
                if(count==15) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Ollyo Moorup Chagi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }
                if(count==16) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Yop Chagi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==17) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Sonkul", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==18) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Nachuo", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==19) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Dung Joomuk", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==20) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Palkup", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==21) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Son Sonkut Tulgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==22) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "B", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==23) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Yop Sonkut Tulgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==24) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Najunde Doo Palmok Miro Makgi", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==25) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Flying", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==26) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "21", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==27) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "28", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==28) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "38", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==29) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "32", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==30) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "19", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==31) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "24", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==32) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "37", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==33) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "I", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==34) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "1501-1570", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==35) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Retreating Creek", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==36) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Yes", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==37) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "YES", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==38) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "Answer not given", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==39) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "316", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }if(count==40) {
                    Snackbar snackBar7 = Snackbar.make(snackbar7, "1000", Snackbar.LENGTH_LONG);
                    snackBar7.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){img2.setVisibility(View.GONE);
                    ques.setText("BLUE – SIGNIFIES What?");
                    ans1.setText("THE HEAVENS");
                    ans2.setText("DANGER");
                    ans3.setText("PASSION");
                    ans4.setText("BEGINNER");

                } if(count==2){
                    ques.setText("RED – SIGNIFIES What?");
                    ans1.setText("DANGER");
                    ans2.setText("MATURITY");
                    ans3.setText("GROWTH");
                    ans4.setText("HEART");

                } if(count==3){
                    ques.setText("How many moves in PATTERN TOI-GYE?");
                    ans1.setText("38");
                    ans2.setText("39");
                    ans3.setText("37");
                    ans4.setText("29");


                } if(count==4){
                    ques.setText("TOI-GYE is the penname of which noted scholar?");
                    ans1.setText("Dan-Gun");
                    ans2.setText("Yul-Gok");
                    ans3.setText("Yi-Hwang");
                    ans4.setText("Hwa-Rang");

                } if(count==5){
                    ques.setText("TOI-GYE was an authority on what?");
                    ans1.setText("Confucianism");
                    ans2.setText("The Arts");
                    ans3.setText("Taekwon-do");
                    ans4.setText("Neo-Confucianism");

                } if(count==6){
                    ques.setText("The 37 movements of the pattern TOI-GYE refer to what?");
                    ans1.setText("Birthplace on the 37degree latitude");
                    ans2.setText("Life");
                    ans3.setText("Opposites");
                    ans4.setText("Heaven & Earth");

                } if(count==7){
                    ques.setText("The pattern diagram for TOI-GYE represents what?");
                    ans1.setText("Honour");
                    ans2.setText("Loyalty");
                    ans3.setText("Respect");
                    ans4.setText("Scholar");

                } if(count==8) {
                    ques.setText("What is the PATTERN DIAGRAM for TOI-GYE ?");
                    ans1.setText("T");
                    ans2.setText("S");
                    ans3.setText("Y");
                    ans4.setText("I");

                }if(count==9) {
                    ques.setText("What is the 2nd move of Toi-Gye?");
                    ans1.setText("Dwijibun Sonkut Tulgi");
                    ans2.setText("Son Sonkut Tulgi");
                    ans3.setText("Opun Sonkut Tulgi");
                    ans4.setText("None of the above");

                }if(count==10) {
                    ques.setText("What is the target area for the 5th move from pattern Toi-Gye?");
                    ans1.setText("Head");
                    ans2.setText("Groin");
                    ans3.setText("Heart");
                    ans4.setText("Lower Abdomen");

                }if(count==11) {
                    ques.setText("What is the Korean for slow twin side elbow thrust?");
                    ans1.setText("Sang Yop Palkup Tulgi");
                    ans2.setText("Sang Palmok Makgi");
                    ans3.setText("Sang Sonkul Makgi");
                    ans4.setText("None of the above");

                }if(count==12) {
                    ques.setText(" What is the Korean for “W” shape block?");
                    ans1.setText("San makgi");
                    ans2.setText("Yop makgi");
                    ans3.setText("Sang sonkul makgi");
                    ans4.setText("Doo Palmuk Makgi");


                }if(count==13) {
                    ques.setText("What is the Korean for “X” fist pressing block?");
                    ans1.setText("Doo makgi");
                    ans2.setText("Kyocha Makgi");
                    ans3.setText("Kyocha Joomuk Noollo makgi");
                    ans4.setText("Noollo makgi");

                }if(count==14) {
                    ques.setText("What is the Korean for twin front grasp?");
                    ans1.setText("Sang Ap Makgi");
                    ans2.setText("Sang Ap Japki");
                    ans3.setText("Japki Sang");
                    ans4.setText("Sang Ap Makgi Bandae");

                }if(count==15) {
                    ques.setText("What is the Korean for upward Knee kick?");
                    ans1.setText("Noollyo Moorup Chagi");
                    ans2.setText("Ollyo Moorup Chagi");
                    ans3.setText("Ollyo chagi");
                    ans4.setText("Moorup Nollyo chagi");

                }if(count==16) {
                    ques.setText("Which is not a block?");
                    ans1.setText("Najunde Bakat Palmok makgi");
                    ans2.setText("Kaunde An palmok makgi");
                    ans3.setText("Najunde Sonkal makgi");
                    ans4.setText("Yop Chagi");

                }if(count==17) {
                    ques.setText("Which is not a kick?");
                    ans1.setText("Apcha busigi (front snap kick)");
                    ans2.setText("Sonkul");
                    ans3.setText("Dollyo chagi (turning kick)");
                    ans4.setText("Dwit chajirugi (back piercing kick)");

                }if(count==18) {
                    ques.setText(" Which is Low stance?");
                    ans1.setText("Gunnon");
                    ans2.setText("Annun");
                    ans3.setText("Niunja");
                    ans4.setText("Narani");

                }if(count==19) {
                    ques.setText("Which is Back Fist?");
                    ans1.setText("Ap Joomuk");
                    ans2.setText("Dung Joomuk");
                    ans3.setText("Yop Joomuk");
                    ans4.setText("Sonkal");

                }if(count==20) {
                    ques.setText("Which is not a foot parts ?");
                    ans1.setText("Ap kumchi");
                    ans2.setText("Palkup");
                    ans3.setText("Balkal, Baldung");
                    ans4.setText("Dwichook");


                }if(count==21) {
                    ques.setText("Which is not a hand attack from pattern Joong-Gun?");
                    ans1.setText("Wi Palkup Taerigi");
                    ans2.setText("Giokja Jirugi");
                    ans3.setText("Sang Dwijibo Jirugi");
                    ans4.setText("Son Sonkut Tulgi");

                }if(count==22) {
                    ques.setText("What is the ready position in Toi-Gye?");
                    ans1.setText("B");
                    ans2.setText("A");
                    ans3.setText("C");
                    ans4.setText("D");

                }if(count==23) {
                    ques.setText("Which is not a fingertip thrust?");
                    ans1.setText("Dwijibun Sonkut Tulgi");
                    ans2.setText("Yop Sonkut Tulgi");
                    ans3.setText("Opun Sonkut Tulgi");
                    ans4.setText("Sun Sonkut Tulgi");

                }if(count==24) {
                    ques.setText("What is the Korean for Low Double Forearm Pushing Block?");
                    ans1.setText("Najunde Doo Palmok Miro Makgi");
                    ans2.setText("Mori Makgi");
                    ans3.setText("Hans Makgi");
                    ans4.setText("Doo Miro Makgi");

                }if(count==25) {
                    ques.setText("What is TWIMYO?");
                    ans1.setText("Jumping");
                    ans2.setText("Running");
                    ans3.setText("Flying");
                    ans4.setText("Jogging");

                }if(count==26) {
                    ques.setText("NUMBER OF MOVES IN DAN-GUN?");
                    ans1.setText("23");
                    ans2.setText("25");
                    ans3.setText("21");
                    ans4.setText("19");

                }if(count==27) {
                    ques.setText("NUMBER OF MOVES IN WON-HYO?");
                    ans1.setText("28");
                    ans2.setText("25");
                    ans3.setText("21");
                    ans4.setText("19");


                }if(count==28) {
                    ques.setText("NUMBER OF MOVES IN YUL-GOK ?");
                    ans1.setText("24");
                    ans2.setText("28");
                    ans3.setText("38");
                    ans4.setText("21");

                }if(count==29) {
                    ques.setText("NUMBER OF MOVES IN JOONG-GUN ?");
                    ans1.setText("22");
                    ans2.setText("27");
                    ans3.setText("29");
                    ans4.setText("32");

                }if(count==30) {
                    ques.setText("NUMBER OF MOVES IN CHON-JI ?");
                    ans1.setText("18");
                    ans2.setText("23");
                    ans3.setText("25");
                    ans4.setText("19");

                }if(count==31) {
                    ques.setText("NUMBER OF MOVES IN DO-SAN ?");
                    ans1.setText("25");
                    ans2.setText("24");
                    ans3.setText("21");
                    ans4.setText("29");

                }if(count==32) {
                    ques.setText("NUMBER OF MOVES IN TOI-GYE ?");
                    ans1.setText("28");
                    ans2.setText("25");
                    ans3.setText("34");
                    ans4.setText("37");

                }if(count==33) {
                    ques.setText("What is the pattern diagram for DAN-GUN?");
                    ans1.setText("I");
                    ans2.setText("S");
                    ans3.setText("X");
                    ans4.setText("T");

                }if(count==34) {
                    ques.setText("Yi Hwang lived from?");
                    ans1.setText("1401-1470");
                    ans2.setText("1502-1567");
                    ans3.setText("1501-1570");
                    ans4.setText("1961-2012");

                }if(count==35) {
                    ques.setText("Yi Hwang is often referred to by his penname Toi-Gye, what is TOI-GYE also referred to?");
                    ans1.setText("Retreating Valley");
                    ans2.setText("Retreating Creek");
                    ans3.setText("Big Creek");
                    ans4.setText("Little Valley");

                }if(count==36) {
                    ques.setText("Was Toi-Gye a government official?");
                    ans1.setText("Yes");
                    ans2.setText("No");
//                    ans3.setText("21");
//                    ans4.setText("29");

                }if(count==37) {
                    ques.setText("Was Toi-Gye appointed to work at the Royal court?");
                    ans1.setText("No");
                    ans2.setText("YES");
//                    ans3.setText("34");
//                    ans4.setText("37");

                }if(count==38) {
                    ques.setText("Toi-Gye became a governor of where?");
                    ans1.setText("Danyang & Punggi");
                    ans2.setText("Punggi & Soel");
                    ans3.setText("Danyang & Soel");
                    ans4.setText("None of the above");

                }if(count==39) {
                    ques.setText("Toi-Gye was the author of how many books?");
                    ans1.setText("316");
                    ans2.setText("12");
                    ans3.setText("145");
                    ans4.setText("216");

                }if(count==40) {img1.setVisibility(View.GONE);
                    ques.setText("Yi Hwang is depicted on which current South Korean won note?");
                    ans1.setText("5000");
                    ans2.setText("10000");
                    ans3.setText("2000");
                    ans4.setText("1000");

                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){img2.setVisibility(View.GONE);
                    ques.setText("BLUE – SIGNIFIES What?");
                    ans1.setText("THE HEAVENS");
                    ans2.setText("DANGER");
                    ans3.setText("PASSION");
                    ans4.setText("BEGINNER");

                } if(count==2){
                    ques.setText("RED – SIGNIFIES What?");
                    ans1.setText("DANGER");
                    ans2.setText("MATURITY");
                    ans3.setText("GROWTH");
                    ans4.setText("HEART");

                } if(count==3){
                    ques.setText("How many moves in PATTERN TOI-GYE?");
                    ans1.setText("38");
                    ans2.setText("39");
                    ans3.setText("37");
                    ans4.setText("29");


                } if(count==4){
                    ques.setText("TOI-GYE is the penname of which noted scholar?");
                    ans1.setText("Dan-Gun");
                    ans2.setText("Yul-Gok");
                    ans3.setText("Yi-Hwang");
                    ans4.setText("Hwa-Rang");

                } if(count==5){
                    ques.setText("TOI-GYE was an authority on what?");
                    ans1.setText("Confucianism");
                    ans2.setText("The Arts");
                    ans3.setText("Taekwon-do");
                    ans4.setText("Neo-Confucianism");

                } if(count==6){
                    ques.setText("The 37 movements of the pattern TOI-GYE refer to what?");
                    ans1.setText("Birthplace on the 37degree latitude");
                    ans2.setText("Life");
                    ans3.setText("Opposites");
                    ans4.setText("Heaven & Earth");

                } if(count==7){
                    ques.setText("The pattern diagram for TOI-GYE represents what?");
                    ans1.setText("Honour");
                    ans2.setText("Loyalty");
                    ans3.setText("Respect");
                    ans4.setText("Scholar");

                } if(count==8) {
                    ques.setText("What is the PATTERN DIAGRAM for TOI-GYE ?");
                    ans1.setText("T");
                    ans2.setText("S");
                    ans3.setText("Y");
                    ans4.setText("I");

                }if(count==9) {
                    ques.setText("What is the 2nd move of Toi-Gye?");
                    ans1.setText("Dwijibun Sonkut Tulgi");
                    ans2.setText("Son Sonkut Tulgi");
                    ans3.setText("Opun Sonkut Tulgi");
                    ans4.setText("None of the above");

                }if(count==10) {
                    ques.setText("What is the target area for the 5th move from pattern Toi-Gye?");
                    ans1.setText("Head");
                    ans2.setText("Groin");
                    ans3.setText("Heart");
                    ans4.setText("Lower Abdomen");

                }if(count==11) {
                    ques.setText("What is the Korean for slow twin side elbow thrust?");
                    ans1.setText("Sang Yop Palkup Tulgi");
                    ans2.setText("Sang Palmok Makgi");
                    ans3.setText("Sang Sonkul Makgi");
                    ans4.setText("None of the above");

                }if(count==12) {
                    ques.setText(" What is the Korean for “W” shape block?");
                    ans1.setText("San makgi");
                    ans2.setText("Yop makgi");
                    ans3.setText("Sang sonkul makgi");
                    ans4.setText("Doo Palmuk Makgi");


                }if(count==13) {
                    ques.setText("What is the Korean for “X” fist pressing block?");
                    ans1.setText("Doo makgi");
                    ans2.setText("Kyocha Makgi");
                    ans3.setText("Kyocha Joomuk Noollo makgi");
                    ans4.setText("Noollo makgi");

                }if(count==14) {
                    ques.setText("What is the Korean for twin front grasp?");
                    ans1.setText("Sang Ap Makgi");
                    ans2.setText("Sang Ap Japki");
                    ans3.setText("Japki Sang");
                    ans4.setText("Sang Ap Makgi Bandae");

                }if(count==15) {
                    ques.setText("What is the Korean for upward Knee kick?");
                    ans1.setText("Noollyo Moorup Chagi");
                    ans2.setText("Ollyo Moorup Chagi");
                    ans3.setText("Ollyo chagi");
                    ans4.setText("Moorup Nollyo chagi");

                }if(count==16) {
                    ques.setText("Which is not a block?");
                    ans1.setText("Najunde Bakat Palmok makgi");
                    ans2.setText("Kaunde An palmok makgi");
                    ans3.setText("Najunde Sonkal makgi");
                    ans4.setText("Yop Chagi");

                }if(count==17) {
                    ques.setText("Which is not a kick?");
                    ans1.setText("Apcha busigi (front snap kick)");
                    ans2.setText("Sonkul");
                    ans3.setText("Dollyo chagi (turning kick)");
                    ans4.setText("Dwit chajirugi (back piercing kick)");

                }if(count==18) {
                    ques.setText(" Which is Low stance?");
                    ans1.setText("Gunnon");
                    ans2.setText("Annun");
                    ans3.setText("Niunja");
                    ans4.setText("Narani");

                }if(count==19) {
                    ques.setText("Which is Back Fist?");
                    ans1.setText("Ap Joomuk");
                    ans2.setText("Dung Joomuk");
                    ans3.setText("Yop Joomuk");
                    ans4.setText("Sonkal");

                }if(count==20) {
                    ques.setText("Which is not a foot parts ?");
                    ans1.setText("Ap kumchi");
                    ans2.setText("Palkup");
                    ans3.setText("Balkal, Baldung");
                    ans4.setText("Dwichook");


                }if(count==21) {
                    ques.setText("Which is not a hand attack from pattern Joong-Gun?");
                    ans1.setText("Wi Palkup Taerigi");
                    ans2.setText("Giokja Jirugi");
                    ans3.setText("Sang Dwijibo Jirugi");
                    ans4.setText("Son Sonkut Tulgi");

                }if(count==22) {
                    ques.setText("What is the ready position in Toi-Gye?");
                    ans1.setText("B");
                    ans2.setText("A");
                    ans3.setText("C");
                    ans4.setText("D");

                }if(count==23) {
                    ques.setText("Which is not a fingertip thrust?");
                    ans1.setText("Dwijibun Sonkut Tulgi");
                    ans2.setText("Yop Sonkut Tulgi");
                    ans3.setText("Opun Sonkut Tulgi");
                    ans4.setText("Sun Sonkut Tulgi");

                }if(count==24) {
                    ques.setText("What is the Korean for Low Double Forearm Pushing Block?");
                    ans1.setText("Najunde Doo Palmok Miro Makgi");
                    ans2.setText("Mori Makgi");
                    ans3.setText("Hans Makgi");
                    ans4.setText("Doo Miro Makgi");

                }if(count==25) {
                    ques.setText("What is TWIMYO?");
                    ans1.setText("Jumping");
                    ans2.setText("Running");
                    ans3.setText("Flying");
                    ans4.setText("Jogging");

                }if(count==26) {
                    ques.setText("NUMBER OF MOVES IN DAN-GUN?");
                    ans1.setText("23");
                    ans2.setText("25");
                    ans3.setText("21");
                    ans4.setText("19");

                }if(count==27) {
                    ques.setText("NUMBER OF MOVES IN WON-HYO?");
                    ans1.setText("28");
                    ans2.setText("25");
                    ans3.setText("21");
                    ans4.setText("19");


                }if(count==28) {
                    ques.setText("NUMBER OF MOVES IN YUL-GOK ?");
                    ans1.setText("24");
                    ans2.setText("28");
                    ans3.setText("38");
                    ans4.setText("21");

                }if(count==29) {
                    ques.setText("NUMBER OF MOVES IN JOONG-GUN ?");
                    ans1.setText("22");
                    ans2.setText("27");
                    ans3.setText("29");
                    ans4.setText("32");

                }if(count==30) {
                    ques.setText("NUMBER OF MOVES IN CHON-JI ?");
                    ans1.setText("18");
                    ans2.setText("23");
                    ans3.setText("25");
                    ans4.setText("19");

                }if(count==31) {
                    ques.setText("NUMBER OF MOVES IN DO-SAN ?");
                    ans1.setText("25");
                    ans2.setText("24");
                    ans3.setText("21");
                    ans4.setText("29");

                }if(count==32) {
                    ques.setText("NUMBER OF MOVES IN TOI-GYE ?");
                    ans1.setText("28");
                    ans2.setText("25");
                    ans3.setText("34");
                    ans4.setText("37");

                }if(count==33) {
                    ques.setText("What is the pattern diagram for DAN-GUN?");
                    ans1.setText("I");
                    ans2.setText("S");
                    ans3.setText("X");
                    ans4.setText("T");

                }if(count==34) {
                    ques.setText("Yi Hwang lived from?");
                    ans1.setText("1401-1470");
                    ans2.setText("1502-1567");
                    ans3.setText("1501-1570");
                    ans4.setText("1961-2012");

                }if(count==35) {
                    ques.setText("Yi Hwang is often referred to by his penname Toi-Gye, what is TOI-GYE also referred to?");
                    ans1.setText("Retreating Valley");
                    ans2.setText("Retreating Creek");
                    ans3.setText("Big Creek");
                    ans4.setText("Little Valley");

                }if(count==36) {
                    ques.setText("Was Toi-Gye a government official?");
                    ans1.setText("Yes");
                    ans2.setText("No");
//                    ans3.setText("21");
//                    ans4.setText("29");

                }if(count==37) {
                    ques.setText("Was Toi-Gye appointed to work at the Royal court?");
                    ans1.setText("No");
                    ans2.setText("YES");
//                    ans3.setText("34");
//                    ans4.setText("37");

                }if(count==38) {
                    ques.setText("Toi-Gye became a governor of where?");
                    ans1.setText("Danyang & Punggi");
                    ans2.setText("Punggi & Soel");
                    ans3.setText("Danyang & Soel");
                    ans4.setText("None of the above");

                }if(count==39) {
                    ques.setText("Toi-Gye was the author of how many books?");
                    ans1.setText("316");
                    ans2.setText("12");
                    ans3.setText("145");
                    ans4.setText("216");

                }if(count==40) {img1.setVisibility(View.GONE);
                    ques.setText("Yi Hwang is depicted on which current South Korean won note?");
                    ans1.setText("5000");
                    ans2.setText("10000");
                    ans3.setText("2000");
                    ans4.setText("1000");

                }
                if(count ==40){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }


            }
        });


    }
}
