package com.cedesyn.taekwondoffinal;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

/**
 * Created by jerin android on 12-02-2018.
 */

public class Team extends AppCompatActivity {
    public String Name;
    public String Image;
    public ArrayList<String> players=new ArrayList<String>();

    public Team(String Name)
    {
        this.Name=Name;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return Name;
    }
}
