package com.cedesyn.taekwondoffinal;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by jerin android on 02-03-2018.
 */

@SuppressLint("ValidFragment")
public class FragmentOne extends Fragment {
    Button btn;
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //Inflate the layout for this fragment
        return inflater.inflate(
                R.layout.fragment_one, container, false);

    }
}
