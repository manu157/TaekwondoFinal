package com.cedesyn.taekwondoffinal;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by jerin android on 02-03-2018.
 */

@SuppressLint("ValidFragment")
public class FragmentTwo extends Fragment {


    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(

                R.layout.fragment_two, container, false);

    }
}
