package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DataPresentation1 extends AppCompatActivity {
    String value,
            tenq="Where does Taekwon-Do come from?\n ANSWER : Korea (1950's)\n" +
                    "\nWhat is the name of your instructor?\n\n ANSWER :YOUR ANSWER" +
                    "\nWhat is your training suit in Korean? \n ANSWER :Dobok\n" +
                    "\nWhat is the training hall in Korean?\n ANSWER : Dojang\n" +
                    "\nWhat are the three rules of concentration?\n ANSWER : Focus your eyes, mind &" +
                    "body\n" +
                    "\nHow do we get power in our movements?\n ANSWER : Sign wave, Ki-ap, Timing," +
                    "Speed, Excelleration\n" +
                    "\nWhat are the benefits from doing Taekwon-Do?\n ANSWER : YOUR ANSWER\n" +
                    "\nWhat are Tae, Kwon and Do in English?\n ANSWER : Foot, Hand, Way",
            tent="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n"+"\nBOW WHEN ENTERING DOJANG\n" +
                    "LINE UP\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\n10 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\nSITTING STANCE PUNCHES\n" +
                    "\nRISING BLOCKS\n" +
                    "\nMIDDLE BLOCK\n" +
                    "\nWALKING STANCE PUNCH + MIDDLE REVERSE PUNCH\n" +
                    "\nSAJO JURIGI NO 1\n" +
                    "\nDAJO JURIGI NO 2\n" +
                    "\nSAJO MAKI";
    TextView text;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.datapresentation2);
        text=(TextView)findViewById(R.id.data_to_show1);
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        text.setTypeface(tf);
        Intent intent =getIntent();

     value=   intent.getStringExtra("val");
      //  Toast.makeText(this, "val"+value, Toast.LENGTH_SHORT).show();
        if (value.equals("0")){
            text.setText(tenq);

        }
        if(value.equals("1")){
            text.setText(tent);
        }
    }
}
