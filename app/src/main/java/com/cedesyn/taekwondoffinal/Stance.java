package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by jerin android on 22-03-2018.
 */

public class Stance extends AppCompatActivity {
    Button first , second, third , fourth,five,six,seven;
    String count;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stance);
        first=(Button)findViewById(R.id.attention);
        second=(Button)findViewById(R.id.closed);
        third=(Button)findViewById(R.id.walking);
        fourth=(Button)findViewById(R.id.l);
        five=(Button)findViewById(R.id.parallel);
        six=(Button)findViewById(R.id.rear);
        seven=(Button)findViewById(R.id.sit);
        
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="1";
            //    Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);
            }
        });
        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="2";
             //   Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);
            }
        });
        third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="3";
             //   Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);
            }
        });
        fourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="4";
            //    Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="5";
           //     Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);

            }

        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="6";
             //   Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count="7";
             //   Toast.makeText(Stance.this, ""+count, Toast.LENGTH_SHORT).show();
                Intent yu=new Intent(Stance.this,StanceShow.class);
                yu.putExtra("A", count);
                startActivity(yu);
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Stance.this,MainActivity.class));
        finish();
    }
}
