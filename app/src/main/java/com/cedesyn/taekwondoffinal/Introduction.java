package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by jerin android on 06-03-2018.
 */

public class Introduction extends AppCompatActivity {
    TextView introd,introhd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introduction);
        introd=(TextView)findViewById(R.id.itrotext);
        introhd=(TextView)findViewById(R.id.introhead);

        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        introd.setTypeface(tf);
        introhd.setTypeface(tf);
        introhd.setText("TAE KWON DO INTRODUCTION");

        introd.setText("\n\nTaekwondo is a Korean Martial art. It combines combat and self defense techniques with exercise. Taekwondo sparring has been an Olympic event since 2000. \n" +
                "\nTaekwondo was developed by a variety of Korean masters during the 1940s as partial combination of Taekkyeon, Okinawan Karate and other traditions.\n" +
                "\nThe name Taekwondo was introduced by General Choi Hung Hi who is claimed to be the founder and creator of taekwondo by the International Taekwondo Federation. (ITF).\n" +
                "\nPlease note this app has been published for ITF style Taekwondo students, however WTF (World Taekwondo Federation) students will find this app has very useful information.\n" +
                "\nThis app refers to the Traditional Taekwondo, typically the martial art established in the 1950s and 1960s in the South Korean Military and in various civilian organisations, including schools and universities. \n" +
                "\nThe names and symbolism of the traditional patterns often refer to elements of Korean people or history, culture and religious philosophy. Traditional Taekwon-Do may refer to the International Taekwondo Federation (ITF).\n" +
                "\nAlthough there are doctrinal and technical differences between sparring in the two main styles (ITF & WTF) among the various organizations, the art in general emphasizes kicks and punches thrown from a mobile stance. Taekwondo training generally includes a system of blocks, kicks, punches, and open-handed strikes.\n" +
                "\nThis app will provide new, existing & experienced Taekwondo students with plenty of information/theory. The app provides theory only, however this is a big part of your journey to become a good Taekwondo student.\n" +
                "\nYou may be the 1 in 10,000 student that gains his/her black belt, however Taekwondo is not just about getting your Black Belt. \n" +
                "\nYou may choose to develop as an individual or become a 3rd Kup, 5th Kup etc… This app will aid your knowledge along your journey. \n" +
                "\nIncluded in this app is ITF Theory, Introduction & overview of Taekwondo, theory for belts, symbol of patterns, history of patterns, extended history of the patterns, Taekwondo quiz for every belt grade, Korean translations, expectations of a typical grading, tips and recommendations.\n" +
                "\nThis app has a lot of very useful information and has been designed to help young children & adults learn more than is expected. The more you understand the theory the more the practical makes sense and the more competent you will become.  \n" +
                "\nThis app has been produced based on ITF style Taekwondo and the information provided has been based on the experience of completing a 1st Degree Black Belt. \n" +
                "\nIt is also important to understand that different organizations may ask additional questions or may request slightly different information from the ITF student, however generally the vast majority of information provided in this app will be generic on a world-wide basis.\n" +
                "\nGood luck in your training, work hard, train hard but importantly enjoy your training & respect the Taekwondo art.\n" +
                "\nWe will from time to time add additional information to the APP, if you have purchased this app, thank you. If you have any comments or suggestions to improve the app please do comment and we will try to add where possible.\n" +
                "\nIf you like the ITF Theory app please do recommend to your colleagues, friends & fellow Taekwondo students.\n" +
                "\nTae-kwon !!!\n");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Introduction.this,MainActivity.class));
        finish();
    }
}
