package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show2 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar2;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show2);
        snackbar2=(ConstraintLayout)findViewById(R.id.snacks2);

        btn=(Button)findViewById(R.id.ans2);


        ques=(TextView)findViewById(R.id.qstn2);
        ans1=(TextView) findViewById(R.id.o12);
        ans2=(TextView) findViewById(R.id.o22);
        ans3=(TextView) findViewById(R.id.o32);
        ans4=(TextView) findViewById(R.id.o42);


        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("How many moves in Dan-Gun?");
        ans1.setText("18");
        ans2.setText("17");
        ans3.setText("20");
        ans4.setText("21");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "21 ", Snackbar.LENGTH_LONG);
                    snackBar2.show();
                }

                if(count==2){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Nopunde", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==3){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Ap-joomuk", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==4){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Palmok", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==5){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "1967", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==6){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Ap KUMCHI", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==7){
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Balkal", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==8) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Sang Palmok Makgi", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==9) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "19", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==10) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Sonkal Daebi Makgi", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==11) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Anaero", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==12) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Ollyo", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==13) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Naeryo", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==14) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Knee section outer forearm block (Moorup Bakat Palmok makgi).\n", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==15) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "21", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==16) {
                    Snackbar snackBar2 = Snackbar.make(snackbar2, "Holy Dan Gun", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){
                    img2.setVisibility(View.GONE);
                    ques.setText("How many moves in Dan-Gun?");
                    ans1.setText("18");
                    ans2.setText("17");
                    ans3.setText("20");
                    ans4.setText("21");

                } if(count==2){
                    ques.setText("Name the High section of the body in Korean?");
                    ans1.setText("Nopunde");
                    ans2.setText("Kaunde");
                    ans3.setText("Najunde");
                    ans4.setText("Sadunde");

                } if(count==3){
                    ques.setText("Forefist in Korean?");
                    ans1.setText("Ap-joomuk");
                    ans2.setText("Palkup");
                    ans3.setText("Balkal");
                    ans4.setText("Moorup");


                } if(count==4){
                    ques.setText("Forearm in Korean?");
                    ans1.setText("Palmok");
                    ans2.setText("Palkup");
                    ans3.setText("Balkal");
                    ans4.setText("Moorup");

                } if(count==5){
                    ques.setText(" When was Taekwon-Do brought to England?");
                    ans1.setText("1967");
                    ans2.setText("1969");
                    ans3.setText("1971");
                    ans4.setText("1955");


                } if(count==6){
                    ques.setText("What part of the foot is used for front snap kick?");
                    ans1.setText("Ap KUMCHI");
                    ans2.setText("Balkal");
                    ans3.setText("Dwit chook");
                    ans4.setText("Ap- joomuk");

                } if(count==7){
                    ques.setText("What part of the foot is used for side piercing kick?");
                    ans1.setText("Gunnun Sogi");
                    ans2.setText("Dwit chook");
                    ans3.setText("Balkal");
                    ans4.setText("Bandalson");

                } if(count==8) {
                    ques.setText("Name twin forearm block in Korean?");
                    ans1.setText("Sang Palmok Makgi");
                    ans2.setText("Sang Joomuk");
                    ans3.setText("Sonkal Deabi Maki");
                    ans4.setText("None of the above");

                }if(count==9) {
                    ques.setText("How many movements in Chon-Ji?");
                    ans1.setText("24");
                    ans2.setText("16");
                    ans3.setText("18");
                    ans4.setText("19");

                }if(count==10) {
                    ques.setText("Name knife-hand guarding block in Korean?");
                    ans1.setText("Sonkal Daebi Makgi");
                    ans2.setText("Sang Palmok Maki");
                    ans3.setText("Ap chagi");
                    ans4.setText("Sang Sonkul Makgi");

                }if(count==11) {
                    ques.setText("What is inwards in Korean?");
                    ans1.setText("Anaero");
                    ans2.setText("Bakaero");
                    ans3.setText("Bituro");
                    ans4.setText("Neverhereo");

                }if(count==12) {
                    ques.setText("What is upward in Korean?");
                    ans1.setText("Ollyo");
                    ans2.setText("Naeryo");
                    ans3.setText("Anaero");
                    ans4.setText("Bakaero");

                }if(count==13) {
                    ques.setText("What is downward in Korean?");
                    ans1.setText("Ollyo");
                    ans2.setText("Naeryo");
                    ans3.setText("Bakaero");
                    ans4.setText("Anaero");

                }if(count==14) {
                    ques.setText("Which is not a Taekwon-do block?");
                    ans1.setText("Twin forearm block (Sang Palmok Makgi)");
                    ans2.setText("Knife-hand Guarding Block (Sonkal Daebi Makgi)");
                    ans3.setText("Middle section Inner Forearm Block (Kaunde an Palmok makgi)");
                    ans4.setText("Knee section outer forearm block (Moorup Bakat Palmok makgi)");

                }if(count==15) {
                    ques.setText("How many moves in Dan-Gun");
                    ans1.setText("23");
                    ans2.setText("18");
                    ans3.setText("19");
                    ans4.setText("21");

                }if(count==16) {
                    img1.setVisibility(View.GONE);
                    ques.setText("What name is Dan-Gun referred to?");
                    ans1.setText("Holy Dan Gun");
                    ans2.setText("Lord Dan Gun");
                    ans3.setText("Sir Dan Gun");
                    ans4.setText("None of the above");

                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){
                    img2.setVisibility(View.INVISIBLE);
                    ques.setText("How many moves in Dan-Gun?");
                    ans1.setText("18");
                    ans2.setText("17");
                    ans3.setText("20");
                    ans4.setText("21");

                } if(count==2){
                    ques.setText("Name the High section of the body in Korean?");
                    ans1.setText("Nopunde");
                    ans2.setText("Kaunde");
                    ans3.setText("Najunde");
                    ans4.setText("Sadunde");

                } if(count==3){
                    ques.setText("Forefist in Korean?");
                    ans1.setText("Ap-joomuk");
                    ans2.setText("Palkup");
                    ans3.setText("Balkal");
                    ans4.setText("Moorup");


                } if(count==4){
                    ques.setText("Forearm in Korean?");
                    ans1.setText("Palmok");
                    ans2.setText("Palkup");
                    ans3.setText("Balkal");
                    ans4.setText("Moorup");

                } if(count==5){
                    ques.setText(" When was Taekwon-Do brought to England?");
                    ans1.setText("1967");
                    ans2.setText("1969");
                    ans3.setText("1971");
                    ans4.setText("1955");


                } if(count==6){
                    ques.setText("What part of the foot is used for front snap kick?");
                    ans1.setText("Ap KUMCHI");
                    ans2.setText("Balkal");
                    ans3.setText("Dwit chook");
                    ans4.setText("Ap- joomuk");

                } if(count==7){
                    ques.setText("What part of the foot is used for side piercing kick?");
                    ans1.setText("Gunnun Sogi");
                    ans2.setText("Dwit chook");
                    ans3.setText("Balkal");
                    ans4.setText("Bandalson");

                } if(count==8) {
                    ques.setText("Name twin forearm block in Korean?");
                    ans1.setText("Sang Palmok Makgi");
                    ans2.setText("Sang Joomuk");
                    ans3.setText("Sonkal Deabi Maki");
                    ans4.setText("None of the above");

                }if(count==9) {
                    ques.setText("How many movements in Chon-Ji?");
                    ans1.setText("24");
                    ans2.setText("16");
                    ans3.setText("18");
                    ans4.setText("19");

                }if(count==10) {
                    ques.setText("Name knife-hand guarding block in Korean?");
                    ans1.setText("Sonkal Daebi Makgi");
                    ans2.setText("Sang Palmok Maki");
                    ans3.setText("Ap chagi");
                    ans4.setText("Sang Sonkul Makgi");

                }if(count==11) {
                    ques.setText("What is inwards in Korean?");
                    ans1.setText("Anaero");
                    ans2.setText("Bakaero");
                    ans3.setText("Bituro");
                    ans4.setText("Neverhereo");

                }if(count==12) {
                    ques.setText("What is upward in Korean?");
                    ans1.setText("Ollyo");
                    ans2.setText("Naeryo");
                    ans3.setText("Anaero");
                    ans4.setText("Bakaero");

                }if(count==13) {
                    ques.setText("What is downward in Korean?");
                    ans1.setText("Ollyo");
                    ans2.setText("Naeryo");
                    ans3.setText("Bakaero");
                    ans4.setText("Anaero");

                }if(count==14) {
                    ques.setText("Which is not a Taekwon-do block?");
                    ans1.setText("Twin forearm block (Sang Palmok Makgi)");
                    ans2.setText("Knife-hand Guarding Block (Sonkal Daebi Makgi)");
                    ans3.setText(" Middle section Inner Forearm Block (Kaunde an Palmok makgi)");
                    ans4.setText("Knee section outer forearm block (Moorup Bakat Palmok makgi)");

                }if(count==15) {
                    ques.setText("How many moves in Dan-Gun");
                    ans1.setText("23");
                    ans2.setText("18");
                    ans3.setText("19");
                    ans4.setText("21");

                }if(count==16) {
                   // img1.setVisibility(View.GONE);
                    ques.setText("What name is Dan-Gun referred to?");
                    ans1.setText("Holy Dan Gun");
                    ans2.setText("Lord Dan Gun");
                    ans3.setText("Sir Dan Gun");
                    ans4.setText("None of the above");

                }
                if(count >=16){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }


            }
        });


    }
}
