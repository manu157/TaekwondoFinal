package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by jerin android on 05-02-2018.
 */

public class BeltColorMeaning extends AppCompatActivity {
    public String parent,child;
    String wh,yes,yeb,ges,geb,bls,blb,res,reb,bs,bdb,wh1,yes1,yeb1,ges1,geb1,bls1,blb1,res1,reb1,bs1,bdb1;

    TextView show,showsub;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beltcolormeaning);


     showsub=(TextView)findViewById(R.id.showsub);
    show =(TextView)findViewById(R.id.cmean) ;

    String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        show.setTypeface(tf);
        showsub.setTypeface(tf);

        wh1="White Belt means Signifies innocence, as that of the beginning student who has no previous knowledge " +
                "of Taekwon-Do.\n";
    yes1="CHON-JI LITERALLY MEANS HEAVON & EARTH. IN THE ORIENT IT IS " +
            "INTERPRETED AS THE CREATION OF THE WORLD OR THE BEGINNING OF " +
            "HUMAN HISTORY, THEREFORE IT IS THE INITIAL PATTERN PLAYED BY THE" +
            "BEGINEER. THIS PATTERN CONSISTS OF TWO SIMILAR PARTS, ONE TO" +
            "REPRESENT HEAVEN AND THE OTHER TO REPRESENT THE EARTH.";
        yeb1="DAN GUN is named after the Holy Dan Gun, the legendary founder of korea in the year 2333 B.C. ";
        ges1="DO SAN is the pseudonym of the patriot An Ch'ang-Ho(1876-1938)who dedicated his entire life to furthering the education of korea" +
                "and its independant movement. ";
        geb1="WON HYO was the noted monk who introduced Buddhism into the silla Dynasty in the year 686.";
        bls1="YUL GOK is the pseudonym of the great philosopher and scholar 'Yi 1',(1536-1584)nicknamed  the 'Confucious of Korea. " +
                "The 38 movements represent his birthplace on the 38th parallel and the diagram represents scholar";
        blb1="JOONG GUN is named after the patriot 'An Joong Gun' who assassinated hiro Bumi ito the first japanese Governor General of Korea, Known as " +
                "the man who played the leading role in the japan - Korea merger. There are 32 movements in the pattern  to represent " +
                "Mr An's age when he was executed at Lui Shung Prison in 1910.";
        res1="TOI GYE is the pen name of the noted scholar Ti Hwang(16th Century A.D), an authority on neo -confuscionism. The 37 movements of this " +
                "pattern refer to his birthplace on 37th parallel.";
        reb1="Hwa Rang is named after the Hwa Rang youth group which originated in the Silla Dynasty around 600AD." +
                " This group eventually became the actual driving force for the unification of the three Kingdoms of Korea." +
                "The  29 movements refer to the 29th Infantry Division, where Tae Kwon-Do developed into maturity.";
        bs1="CHOONG MOO was the given name to the great Admiral YI SUN-SIN of the YI Dynasty.He was reputed to have invented the first " +
                "armoured battleship(Kobuskon),which was the precursor to the present day submarine, in 1592 A.D. the reason why this pattern ends with a left hand " +
                "attack is to symbolise his regrattable death, having no chance to show his unrestrained potentiality, checked by the forced reservation of " +
                "his loyalty to the King. ";
        bdb1="Black Dan belt";



        wh="White Belt10'th Kup";
    yes="CHON JI (Yellow Stripe) 9'th Kup ";
    yeb="DAN GUN (Yellow Belt) 8'th Kup ";
    ges="DO SAN (Green Stripe) 7'th Kup ";
    geb="WON HYO(Green belt) 6'th kup";
    bls="YUL GOK(Blue Stripe) 5'th kup ";
    blb="JOONG GUN(Blue Belt) 4'th kup";
    res="TOI GYE(Red Stripe) 3'rd kup";
    reb="HWA RANG(Red Belt) 2'nd kup";
    bs="CHOONG MOO(Black Stripe) 1'st kup ";
    bdb="1'st Degree Black Belt";
    Intent ii = getIntent();
    parent= ii.getStringExtra("parentpos");
//    child= ii.getStringExtra("chldpos");
//        Toast.makeText(this, ""+child, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ""+parent, Toast.LENGTH_SHORT).show();
        SharedPreferences sp = getDefaultSharedPreferences(getApplicationContext());
        final String parp=sp.getString("parentpos","");
        final String childp=sp.getString("childpos","");


        switch (parp){

        case "0":
            show.setText(wh);
            showsub.setText(wh1);
            break;
        case "1":
            show.setText(yes);
            showsub.setText(yes1);
            break;
        case "2":
            show.setText(yeb);
            showsub.setText(yeb1);
            break;
        case "3":
            show.setText(ges);
            showsub.setText(ges1);
            break;
        case "4":
            show.setText(geb);
            showsub.setText(geb1);
            break;
        case "5":
            show.setText(bls);
            showsub.setText(bls1);
            break;
        case "6":
            show.setText(blb);
            showsub.setText(blb1);
            break;
        case "7":
            show.setText(res);
            showsub.setText(res1);
            break;
        case "8":
            show.setText(reb);
            showsub.setText(reb1);
            break;
        case "9":
            show.setText(bs);
            showsub.setText(bs1);
            break;
        case "10":
            show.setText(bdb);
            showsub.setText(bdb1);
            break;



    }

}}
