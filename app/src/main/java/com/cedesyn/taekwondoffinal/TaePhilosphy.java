package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by jerin android on 31-01-2018.
 */

public class TaePhilosphy extends AppCompatActivity {
    TextView head,phils,pintro,onep,twop,threeo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.philosph);
        head=(TextView)findViewById(R.id.head);
        pintro=(TextView)findViewById(R.id.intro);
        onep =(TextView)findViewById(R.id.one);
        twop =(TextView)findViewById(R.id.two);
        threeo =(TextView)findViewById(R.id.three);

        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        head.setTypeface(tf);
        pintro.setTypeface(tf);
        onep.setTypeface(tf);
        twop.setTypeface(tf);
        threeo.setTypeface(tf);

        twop.setText("\nWhat is Philosophy? \n" +
                "\nHow can my research influence my Taekwondo Philosophy?\n" +
                "\nWhat is my Taekwondo Philosophy? \n" +
                "\nWill my Philosophy evolve/modify?\n" +
                "\nWhat is Philosophy? - Wikipedia Definition   \n");

        threeo.setText( "\nPhilosophy is the study of the general and fundamental nature of reality," +
                " existence, knowledge, values, reason, mind, and language." +
                " The Ancient Greek word φιλοσοφία (philosophia) was probably coined by Pythagoras" +
                " and literally means \"love of wisdom\" or \"friend of wisdom\"." +
                "Philosophy has been divided into many sub-fields. It has been divided chronologically " +
                "(e.g., ancient and modern); by topic (the major topics being epistemology, logic, metaphysics," +
                " ethics, and aesthetics); and by style (e.g., analytic philosophy).\n" +
                "\nAs a method, philosophy is often distinguished from other ways of addressing such problems by " +
                "its questioning, critical, generally systematic approach and its reliance on rational argument." +
                " As a noun, the term \"philosophy\" can refer to any body of knowledge." +
                " Historically, these bodies of knowledge were commonly divided into natural philosophy," +
                " moral philosophy, and metaphysical philosophy. In casual speech, the term can refer" +
                " to any of \"the most basic beliefs, concepts, and attitudes of an individual or group" +
                ",\" (e.g., \"Dr. Smith's philosophy of parenting\").\n" +
                "\nI came across “ETHICS” on a number of occasions. In short ethics " +
                "involves defending and recommending concepts of right and wrong conduct." +
                " Ethics investigates the questions \"What is the best way for people to live?\" " +
                "and \"What actions are right or wrong in particular circumstances?\" In practice, ethics " +
                "seeks to resolve questions of human morality, by defining concepts such as good and evil, " +
                "right and wrong, justice and crime. \n" +
                "\nMy Taekwondo Philosophy is based around ETHICS.   \n" +
                "\nHow can my research influence my Taekwondo Philosophy?\n" +
                "\nRecently my instructor asked me what my philosophy on taekwondo was? My answer was as follows: \n" +
                "\nI wanted to enjoy my taekwondo\n" +
                "\nUnderstand the theory of power\n" +
                "\nSet example by way of good knowledge of theory\n" +
                "\nBe as technically correct as possible\n" +
                "\nHaving reviewed my answer it was probably acceptable and achievable. However, on reflection it " +
                "is all a bit self-centred.    \n" +
                "\nI started to try and think differently, how can my research help me incorporate good ethics into" +
                " my life? How can I change my taekwondo philosophy and develop as an individual? How can I make my" +
                " life and my family’s life more rewarding? I wanted to consider other people in my Philosophy with " +
                "the intention of influencing and helping others develop etc....  \n" +
                "\nFurther thought was as follows: \n" +
                "\nWhen people refer to “their philosophy” do they really understand what it means?\n" +
                "\nCan I really achieve my philosophy? \n" +
                "\nIs my philosophy realistic?\n" +
                "\nIs my philosophy based on opinion?\n" +
                "\nIs it based on fact?\n" +
                "\nCan we ask others to abide by our philosophy?\n" +
                "\nWho is responsible for managing this philosophy?\n" +
                "\nCan my philosophy affect others?\n" +
                "\nHaving spent many weeks considering the above I started to write my Philosophy on " +
                "Taekwondo. My focus is based on the way I conduct myself, my values, rational for doing " +
                "something, giving back and considering others.   \n" +
                "\nWhat is my Taekwondo Philosophy\n" +
                "\nConsidering all of the above keys points this is my Philosophy.  \n" +
                "\nGive back – by way of supporting my instructors where possible\n" +
                "\nSupport others – where feasibly possible\n" +
                "\nBe polite\n" +
                "\nBecome a good role model\n" +
                "\nAlways do the best based on my capabilities\n" +
                "\nListen, watch & learn\n" +
                "\nNever become a know it all\n" +
                "\nLead by example\n" +
                "\nDevelop tenets into my family life\n" +
                "\nTry to incorporate tenets as a way of life\n" +
                "\nHave a realistic and achievable philosophy\n" +
                "\nBe prepared to evolve & modify my philosophy\n" +
                "\nConsider the meaning of patterns further \n" +
                "\nEnjoy my taekwondo\n" +
                "\nUnderstand the theory of power\n" +
                "\nSet example by way of good knowledge of theory\n" +
                "\nBe as technically correct as possible\n" +
                "\nLead by example inside & outside of the Do-Jang\n" +
                "\nInspire others\n" +
                "\nWill my philosophy evolve/modify?\n" +
                "\nYes. I will review where possible. In particular, where can I give back? How can I help others? Can I try to use the tenets more to become a better person? Will the ITF code of conduct help me to help others? How do I become a better role model for others? What influence can the patterns give to me? What inspiration can I draw from the people or history of the patterns? What can I give back to PUMA? How can I inspire others?  How can I conduct myself better? Do I always support others?  Etc....\n" +
                "\nConclusion\n" +
                "\nMy original taekwondo Philosophy has changed. My new Taekwondo Philosophy is far more generic, however far more thought has developed it for the better. \n" +
                "\nMy better understanding of the true meaning of Philosophy has encouraged me to evolve my Taekwondo philosophy. Benefitting others inside and outside the do-jang.\n" +
                "\nThis essay has helped me consider many ethical principles and incorporate them into my Philosophy. \n" +
                "\nMy research will help me incorporate good ethics into my life, develop me as an individual and will make my life and my family’s life more rewarding. \n" +
                "\nMy understanding will now help me to evolve and modify my Philosophy on a regular basis. \n" +
                "\nThis is a good document full of information. Hopefully other Martial Artists may read it and gain benefit.\n" +
                "\nI am no philosopher!!!\n" +
                "\n\nRecommendations\n" +
                "\nMy first recommendation is that Martial Artists should take the time and look deeper into their philosophy for their chosen art. I am confident they will start to look at themselves differently and may even look differently at life.\n" +
                "\nSecondly I recommend any Martial Artist to read my Taekwondo Philosophy. Any individual new to their" +
                " chosen art will gain some benefit from reading. \n" );
        pintro.setText("INTRODUCTION");
        head.setText("TAEKWON-DO PHILOSPHY");
//        onep.setMovementMethod(new ScrollingMovementMethod());
        onep.setText("\nI started Taekwondo for fitness, another challenge and to spend time with my children who also train. Having attended for a few years it is very clear to me that Taekwondo can have a positive effect on life. This essay has helped me consider many ethical principles and changed my original Taekwondo Philosophy. Here we go I hope you enjoy: \n" +
                        "\n"+"During my life I have heard the word “Philosophy” used by people. It is not a word people use often and from observation/experience I would say it is typically used by large corporate companies, often trying to get their staff buying into the “company philosophy”. Very rarely do you speak with individuals who have a philosophy on a topic/subject etc.... they may have a few ideas and opinions but most have not thought deep enough to really have a philosophy. \n" +
                        "\nI am sure if you search any large corporate company on the www you will find their philosophy. \n" +
                        "\nMy essay is based around the following 4 points:\n"
                );

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(TaePhilosphy.this,MainActivity.class));
        finish();
    }
}
