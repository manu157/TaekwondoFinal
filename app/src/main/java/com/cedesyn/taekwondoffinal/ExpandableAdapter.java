package com.cedesyn.taekwondoffinal;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jerin android on 01-02-2018.
 */

public class ExpandableAdapter extends BaseExpandableListAdapter {

    private Context c;
    private ArrayList<Teams>  teams;
    private LayoutInflater inflater;

    public ExpandableAdapter(Context c, ArrayList<Teams> teams) {
        this.c = c;
        this.teams = teams;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //GET A SINGLE PLAYER
    @Override
    public Object getChild(int groupPos, int childPos) {
        // TODO Auto-generated method stub
        return teams.get(groupPos).Players.get(childPos);
    }

    //GET PLAYER ID
    @Override
    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }

    //GET PLAYER ROW
    @Override
    public View getChildView(int groupPos, int childPos, boolean isLastChild, View convertView,
                             ViewGroup parent) {

        //ONLY INFLATER XML ROW LAYOUT IF ITS NOT PRESENT,OTHERWISE REUSE IT

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.players, null);
        }

        //GET CHILD/PLAYER NAME
        String child = (String) getChild(groupPos, childPos);

        //SET CHILD NAME
        TextView nameTv = (TextView) convertView.findViewById(R.id.textview1);
     //   ImageView img = (ImageView) convertView.findViewById(R.id.imageview1);

        nameTv.setText(child);

        //GET TEAM NAME
        String teamName = getGroup(groupPos).toString();


        return convertView;
    }

    //GET NUMBER OF PLAYERS
    @Override
    public int getChildrenCount(int groupPosw) {
        // TODO Auto-generated method stub
        return teams.get(groupPosw).Players.size();
    }

    //GET TEAM
    @Override
    public Object getGroup(int groupPos) {
        // TODO Auto-generated method stub
        return teams.get(groupPos);
    }

    //GET NUMBER OF TEAMS
    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return teams.size();
    }

    //GET TEAM ID
    @Override
    public long getGroupId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    //GET TEAM ROW
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        //ONLY INFLATE XML TEAM ROW MODEL IF ITS NOT PRESENT,OTHERWISE REUSE IT
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.teams, null);
        }

        //GET GROUP/TEAM ITEM
        Teams t = (Teams) getGroup(groupPosition);

        //SET GROUP NAME
        TextView nameTv = (TextView) convertView.findViewById(R.id.textview1);
        ImageView img = (ImageView) convertView.findViewById(R.id.imageview1);

        String name = t.Names;
        nameTv.setText(name);

        //ASSIGN TEAM IMAGES ACCORDING TO TEAM NAME

        if(name=="White Belt 10'th kup")
        {
            img.setImageResource(R.drawable.whitequ);
        }else if(name=="Yellow Stripe 9'th kup")
        {
            img.setImageResource(R.drawable.whiteyellowstripe);
        }else if(name=="Yellow Belt 8'th kup")
        {
            img.setImageResource(R.drawable.yellow);
        }
        else if(name=="Green Stripe 7'th kup")
        {
            img.setImageResource(R.drawable.yellowgreen);
        }
        else if(name=="Green Belt 6'th kup")
        {
            img.setImageResource(R.drawable.green);
        }else if(name=="Blue Stripe 5'th kup")
        {
            img.setImageResource(R.drawable.greenblue);
        }else if(name=="Blue Belt 4'th kup")
        {
            img.setImageResource(R.drawable.blue);
        }else if(name=="Red Stripe 3'rd kup")
        {
            img.setImageResource(R.drawable.bluered);
        }else if(name=="Red Belt 2'nd kup")
        {
            img.setImageResource(R.drawable.red);
        }else if(name=="Black Stripe 1'st kup") {
            img.setImageResource(R.drawable.redblack);
        }
//        else if(name=="1'st Kup to 1'st Degree Black Belt") {
//            img.setImageResource(R.drawable.black);
//        }

        //SET TEAM ROW BACKGROUND COLOR
        convertView.setBackgroundColor(Color.WHITE);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }

}
