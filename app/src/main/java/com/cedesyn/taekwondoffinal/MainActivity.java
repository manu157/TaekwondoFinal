package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
Button recom,philos,hist,belt,quiz,transl,typgrad,intodc,stance;
ImageView itf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Font path
        String fontPath = "ArialBold.ttf";

        // text view label
     //   TextView txtGhost = (TextView) findViewById(R.id.ghost);

        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

        // Applying font
    //    txtGhost.setTypeface(tf);
        itf=(ImageView)findViewById(R.id.itf1);
        quiz=(Button)findViewById(R.id.qui);
        typgrad=(Button)findViewById(R.id.typgrad);
        typgrad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yu=new Intent(MainActivity.this,TypeGrad.class);
                startActivity(yu);
            }
        });
        itf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yup=new Intent(MainActivity.this ,Extra.class);
                startActivity(yup);
            }
        });

        stance=(Button)findViewById(R.id.stances) ;
        intodc=(Button)findViewById(R.id.intro);
        transl=(Button)findViewById(R.id.trans);

        philos =(Button)findViewById(R.id.philo);
        hist =(Button)findViewById(R.id.hist) ;
        stance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Stance.class));
                finish();
            }
        });

        intodc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Introduction.class));
                finish();
            }
        });

        hist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,History.class));
                finish();
            }
        });
        philos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,TaePhilosphy.class));
                finish();
            }
        });
        recom =(Button)findViewById(R.id.rtips);
        recom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intro =new Intent(MainActivity.this,Recommended.class);
                startActivity(intro);
                finish();

            }
        });
        belt= (Button)findViewById(R.id.bel);
        belt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,Belts.class));
                finish();
            }
        });
        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent qu =new Intent(MainActivity.this,Quiz.class);
                startActivity(qu);
            }
        });
        transl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent qp =new Intent(MainActivity.this,Translation.class);
                startActivity(qp);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
