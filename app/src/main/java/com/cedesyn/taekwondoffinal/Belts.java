package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by jerin android on 01-02-2018.
 */

public class Belts extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.belts);

        //THE EXPANDABLE
        final ExpandableListView elv=(ExpandableListView) findViewById(R.id.exp);

        final ArrayList<Teams> teams=getData();

        //CREATE AND BIND TO ADAPTER
        final ExpandableAdapter adapter=new ExpandableAdapter(Belts.this, teams);
        elv.setAdapter(adapter);

        //SET ONCLICK LISTENER
        elv.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPos,
                                        int childPos, long id) {
                final String selected = (String) adapter.getChild(
                        groupPos, childPos);

           //     Toast.makeText(getBaseContext(), selected, Toast.LENGTH_LONG)
                     //   .show();
            //    Toast.makeText(Belts.this, ""+groupPos, Toast.LENGTH_SHORT).show();
            //    Toast.makeText(Belts.this, ""+childPos, Toast.LENGTH_SHORT).show();

                String grouppos = String.valueOf(groupPos);
                String childpos = String.valueOf(childPos);

                SharedPreferences sp =getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor=sp.edit();
                editor.putString("parentpos", grouppos);
                editor.putString("childpos",childpos);
                editor.commit();


                if(childPos==2){
                //    Toast.makeText(Belts.this, "Can be done", Toast.LENGTH_SHORT).show();
                    Intent ii =new Intent(Belts.this,NumberofMoves.class);
                    ii.putExtra("parentpos", grouppos);
                    ii.putExtra("chldpos", childpos);




                    ii.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(ii);
                    finish();
                }if(childPos==0){
                    startActivity(new Intent(Belts.this,BeltColorMeaning.class));

                   // Toast.makeText(Belts.this, ""+childPos, Toast.LENGTH_SHORT).show();

                }if(childPos==1){
                    startActivity(new Intent(Belts.this,SymbolPattern.class));

                }
                if(childPos==3){
                    startActivity(new Intent(Belts.this,IndHistory.class));
                }
                else{
                   //// Toast.makeText(Belts.this, "nothing done", Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });
    }

    //ADD AND GET DATA

    private ArrayList<Teams> getData(){

        Teams t1=new Teams("White Belt 10'th kup");
        t1.Players.add("Belt Colour Meaning");
        t1.Players.add("Symbol of pattern");
        t1.Players.add("No. of moves");
    //    t1.Players.add("History Reffering To Pattern");


        Teams t2=new Teams("Yellow Stripe 9'th kup");
        t2.Players.add("Belt Colour Meaning");
        t2.Players.add("Symbol of pattern");
        t2.Players.add("No. of moves");
        t2.Players.add("History Reffering To Pattern");


        Teams t3=new Teams("Yellow Belt 8'th kup");
        t3.Players.add("Belt Color Meaning");
        t3.Players.add("Symbol of pattern");
        t3.Players.add("No. of moves");
        t3.Players.add("History Reffering To Pattern");

        Teams t4=new Teams("Green Stripe 7'th kup");
        t4.Players.add("Belt Color Meaning");
        t4.Players.add("Symbol of pattern");
        t4.Players.add("No. of moves");
        t4.Players.add("History Reffering To Pattern");


        Teams t5=new Teams("Green Belt 6'th kup");

        t5.Players.add("Belt Colour Meaning");
        t5.Players.add("Symbol of pattern");
        t5.Players.add("No. of moves");
        t5.Players.add("History Reffering To Pattern");

        Teams t6=new Teams("Blue Stripe 5'th kup");

        t6.Players.add("Belt Color Meaning");
        t6.Players.add("Symbol of pattern");
        t6.Players.add("No. of moves");
        t6.Players.add("History Reffering To Pattern");

        Teams t7=new Teams("Blue Belt 4'th kup");
        t7.Players.add("Belt Color Meaning");
        t7.Players.add("Symbol of pattern");
        t7.Players.add("No. of moves");
        t7.Players.add("History Reffering To Pattern");


        Teams t8=new Teams("Red Stripe 3'rd kup");
        t8.Players.add("Belt Color Meaning");
        t8.Players.add("Symbol of pattern");
        t8.Players.add("No. of moves");
        t8.Players.add("History Reffering To Pattern");


        Teams t9=new Teams("Red Belt 2'nd kup");

        t9.Players.add("Belt Color Meaning");
        t9.Players.add("Symbol of pattern");
        t9.Players.add("No. of moves");
        t9.Players.add("History Reffering To Pattern");

        Teams t10=new Teams("Black Stripe 1'st kup");
        t10.Players.add("Belt Color Meaning");
        t10.Players.add("Symbol of pattern");
        t10.Players.add("No. of moves");
        t10.Players.add("History Reffering To Pattern");

//        Teams t11=new Teams("1'st Kup to 1'st Degree Black Belt");
//        t11.Players.add("Belt Color Meaning");
//        t11.Players.add("Symbol of pattern");
//        t11.Players.add("No. of moves");
//        t11.Players.add("History Reffering To Pattern");



        ArrayList<Teams> allTeams=new ArrayList<Teams>();
        allTeams.add(t1);
        allTeams.add(t2);
        allTeams.add(t3);
        allTeams.add(t4);
        allTeams.add(t5);
        allTeams.add(t6);
        allTeams.add(t7);
        allTeams.add(t8);
        allTeams.add(t9);
        allTeams.add(t10);
       // allTeams.add(t11);

        return allTeams;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Belts.this,MainActivity.class));
        finish();
    }
}