package com.cedesyn.taekwondoffinal;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 06-03-2018.
 */

public class Typegradpage extends AppCompatActivity {

    ImageView imageButton , textButton ;
    Button intro,quiz,tips,theory;
    String  fourthkuptext="BLUE – SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES.\n" +
            "\n" +
            "RED – SIGNIFIES DANGER, CAUTIONING THE STUDENT TO EXERCISE CONTROL WHILST WARNING THE OPPONENT TO STAY AWAY.\n" +
            "\n" +
            "PATTERN – JOONG-GUN\n" +
            "\n" +
            "JOONG-GUN is named after the patriot Ahn Joong-Gun who assassinated Hiro-Bumi ito, the first Japanese governor General of Korea, known as the man who played the leading role in the Korea-Japan merger. The 32 movements of this pattern refer to Mr Ahn’s age when he was executed in Lui-Shung prison in 1910. \n" +
            "\n" +
            "32 MOVEMENTS, PATTERN DIAGRAM CAPITAL I\n",                                                 //pattern diagram?????????????????????
            fourthkupdetail="PLEASE NOTE THIS INFORMATION HAS BEEN PUBLISHED FOR ITF STUDENTS," +
                    " HOWEVER WTF STUDENTS WILL GAIN BENEFIT FROM REVISING WITH THIS INFO.\n" +
                    "\n" +
                    "THIS APP HAS ALSO BEEN PRODUCED TO AID TAEKWON-DO STUDENTS WITH THEORY." +
                    " IT IS ALSO IMPORTANT TO UNDERSTAND THAT DIFFERENT ORGANISATIONS MAY ASK" +
                    " ADDITIONAL QUESTIONS OR MAY REQUESTS OR PROVIDE YOU WITH DIFFERENT QUESTIONS.\n" +
                    "\n" +
                    "PLEASE CONSIDER THIS INFORMATION HAS BEEN PROVIDED FROM THE EXPERIENCE OF COMPLETING " +
                    "A 1ST DEGREE BLACK BELT WITHIN AN ESTABLISHED ITF ORGANISATION",

            fourquizz="Why one step sparring?\n ANSWER : Main benefit is developing fast reflexes as the defending" +
                    "student you do not know what attack is coming.\n" +
                    "\nTell me more about historical figures?\n ANSWER : Your answer\n" +
                    "\nMeaning of Joong- Gun \n ANSWER : As above\n" +
                    "\nWhats the pattern diagram for Joong-Gun?\n ANSWER :CAPITAL I\n" +
                    "\nWhat does the diagram represent?\n ANSWER : Scholar\n" +
                    "\nWhat do we get from performing a pattern? \n ANSWER : Inspiration\n" +
                    "\nWho inspires you?\n Your answer\n" +
                    "\nWho is your Inspiration from Taekwon-do?\n Your answer\n" +
                    "\n" +
                    "\nWhy do you do TKWDO?\n ANSWER : Your answer\n" +
                    "\nName and explain all blocking & offensive moves up to your grade?\n ANSWER : See previous" +
                    "theory\n" +
                    "\nTell me more about Joong  Gun? \n ANSWER :See more info below\n" +
                    "\nHow many moves in Joong  Gun?\n ANSWER : 32\n" +
                    "\nExplain and name all moves in pattern Joong- Gun?"
            ,

                fourtipss="BOW WHEN ENTERING DOJANG\n" +
            "\nLINE UP\n" +
            "\nATTENTION STANCE\n" +
            "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
            "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
            "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
            "\nIDENTIFY YOURSELF\n" +
            "\n20 X PRESS UP'S, 10 X SIT UP'S\n" +
            "\n10 X RISING KICKS (BOTH LEGS)\n" +
            "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
            "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
            "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
            "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
            "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
            "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
            "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
            "\nPATTERN  JOONG-GUN\n" +
            "\nALL PREVIOUS GRADE PATTERNS\n" +
            "\nSAJO JURIGI NO 1, SAJO JURIGI NO 2, SAJO MAKI\n" +
            "\nL-STANCE TWIN FOREARM BLOCK\n" +
            "\nTURNING KICK, SIDE KICK, REVERSE TURNING KICK, BACK KICK POSSIBLE A COMBINATION" +
            "OF 3 KICKS\n" +
            "\nFRONT SNAP KICK\n" +
            "\n3-STEP SPARRING + 2-STEP SPARRING + 2 STEP SPARRING EXTRA + 1-STEP SPARRING\n" +
            "KNIFEHAND GUARDING BLOCK IN L-STANCE\n" +
            "\nSITTING STANCE DOUBLE PUNCH\n" +
            "\nFREE SPARRING\n" +
            "\nDEMONSTRATION OF KICKING A PAD\n" +
            "\nDEMONSTRATION OF STRIKING A PAD\n" +
            "\nPattern  Students Choice excluding Toi-Gye\n" +
            "\nPattern  Set by the Grading Examiner\n" +
            "\nBE PREPARED FOR QUESTIONS DURING GRADING\n" +
            "\nTHINGS TO FOCUS ON EXECUTION OF X STANCE, HIGH DOUBLE FOREARM BLOCKS," +
            "CORRECT HAND POSITION FOR HOOKING BLOCK, POWER, TIMING, CONCENTRATION," +
            "STRAIGHT FINGER TIP THRUST IN CORRECT POSITION, MAINTAIN GOOD CIRCULAR BLOCK," +
            "BENDING READY STANCE, TWIN VERTICAL PUNCH (2-STEP SPARRING), ACCURACY WHEN" +
            "SPARRING AND STEP SPARRING"
            ,
                        fourtheoryy="New:\n" +
            "\nReverse Knifehand \n ANSWER : Sonkal Dung\n" +
            "\nSide Sole \n ANSWER : Yop Balbadak\n" +
            "\nClosed Ready Stance B \n ANSWER : Moa Chunbi Sogi B\n" +
            "\nRear Foot Stance \n ANSWER : Dwit Bal Sogi\n" +
            "\nLow stance \n ANSWER : Nachuo Sogi\n" +
            "\nU-Shaped Block \n ANSWER : Digutcha Makgi\n" +
            "\nReverse Turning Hooking kick  \n ANSWER : Bandae Dollyo Golcho Chagi\n" +
            "\nUpper Elbow Strike  \n ANSWER : Wi Palkup Taerigi\n" +
            "\nPrevious:\n" +
            "\nSide back fist  \n ANSWER : Yop Dung Joomuk\n" +
            "\nStraight fingertip thrust  \n ANSWER : Son Sonkut Tulgi\n" +
            "\nMiddle obverse punch  \n ANSWER : Kaunde Baro Jurigi\n" +
            "\nHigh obverse punch  \n ANSWER : Nopunde Baro Jurigi\n" +
            "\nFront snap kick  \n ANSWER : Ap cha Busigi\n" +
            "\nKnife hand strike  \n ANSWER : Sonkal Taerigi\n" +
            "\nTwin upset punch  \n ANSWER : Sang Dwijibo Jirugi\n" +
            "\nExplain x-stance?\n ANSWER : To gather distance to get closer to your opponent\n" +
            "\nWhat is the meaning of blue belt?\n ANSWER : As above\n" +
            "\nWhat happened on April 11th 1955?\n ANSWER : General Choi Hung Hi presented the name" +
            "\nTaekwon-do to a leading board of Korean officials and high ranking politicians and" +
            "the name Taekwon-do was accepted.\n" +
            "\nTell me all you know about the following blocks:\n" +
            "\nDouble forearm block \n Your answer\n" +
            "\nTwin knife hand block \n Your answer\n" +
            "\nKnife hand guarding block  \n  Your answer\n" +
            "\nHooking block  \n Your answer\n" +
            "\nWhat is Korean for the following techniques:\n" +
            "\nFront elbow  :\n ANSWER : Ap Palkup\n" +
            "Hooking kick : \n ANSWER : Golcho Chagi\n" +
            "Reverse turning kick : \n ANSWER : Bandae Dollyo Chagi\n" +
            "Upward knee kick  :\n ANSWER : Ollyo Moorup Chagi\n" +
            "Front elbow strike : \n ANSWER : Wi Palkup Taerigi\n" +
            "Free sparring : \n ANSWER : Jao Matsoki\n" +
            "Two step sparring : \n ANSWER : Ibo Matsoki\n" +
            "\nBe prepared to answer any 3 questions from the previous belts theory questions.\n" +
            "\nWhy free sparring?\n ANSWER : JAO MATSOKI, TO PRACTISE CONTROLLED KICKING AND" +
            "PUNCHING AGAINST OTHER OPPONENTS, IN COMPETITION YOU WOULD" +
            "TRY TO WIN POINTS BY HITTING CERTAIN TARGET AREA. IT IS IMPORTANT\n" +
            "TO MAINTAIN CONTROL WHEN SPARRING\n" +
            "NUMBER OF MOVES & DIAGRAM OF PREVIOUS PATTERNS\n" +
            "\n" +
            "\nMEANING OF PREVIOUS BELT COLOURS - All students who are 4th Kup and above can be" +
            "asked the name, number of moves and meaning of their highest pattern. They can also be" +
            "asked the meaning of the colour of belt they are trying to achieve and any questions relating\n" +
            "to the gradings that have gone before.\n" +
            "\nFURTHER PATTERN INFORMATION \n JOONG-GUN ASSASSINATED Ito Hirobumi, a four time" +
            "prime minister of Japan and former resident general of Korea. He was an independence activist" +
            "and was a trained marksman. On 26 th October 1909 he assassinated Hiro Bumo Ito for reasons" +
            "such because the Japanese were forcing the use of Japanese bank notes in Korea, Disbanding" +
            "Korean army, Plundering Korean railroads, mines & forest, massacring innocent Koreans," +
            "dethroning the emperor Gojong. Joong-Gun used a gun and shot Ito 3 times at Harbin Railway" +
            "Station hiding a gun in his lunch box, he was arrested by Russian guards, handed over to the" +
            "Japanese and imprisoned for his actions. Executed whilst in prison, Joong-Gun requested his" +
            "execution be by a prisoner of war (firing squad as honour) however, he was refused this and" +
            "was hanged, this was on the basis the Japanese viewed him as a common criminal." +
            "\n\nFURTHER TERMINOLOGY \n ARC HAND = BANDALSON \nX BLOCK = KYOCHA MAKGI" +
            "\nDOWNWARD BLOCK = NAERYO MAKGI\n PRESSING BLOCK = NOOLYO MAKGI",

            fiveintro="GREEN – SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP\n" +
                    "\n" +
                    "BLUE – SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES.\n" +
                    "\n" +
                    "PATTERN – YUL-GOK\n" +
                    "\n" +
                    "YUL-GOK is the pseudonym of the great philosopher and scholar Yi I (Born 1536 -1584) nicknamed the " +
                    "“Confucius of Korea”. The 38 movements of this pattern refer to his birth place on the 38 degree latitude " +
                    "and the diagram represents “Scholar”. ",

    fivequiz="\nWas Yul-Gok?\n ANSWER : Great Philosopher, Great Taekwon-do student, Great Poet, Great\n" +
            "Speaker\n" +
            "\nNumbers of moves in pattern Yul-Gok?\n ANSWER : 37, 38, 28, 32\n" +
            "\nWhich target area is the focus when performing front snap kick?\n ANSWER : Groin, head, chest,\n" +
            "knee\n" +
            "\nWhy perform X-stance?\n ANSWER : To gain distance, to slide, to try it, to keep in time\n" +
            "\nWhat happened on April 11th 1955?\n ANSWER : TAEKWON-DO was formed, General Choi was\n" +
            "born, Taekwon-do was brought to England, ITF was formed\n" +
            "\nSymbol of pattern Yul-Gok? X, T, S, Capital I\n" +
            "\nName a defensive move from Yul-Gok?\n ANSWER : High Double Forearm block, low outer\n" +
            "forearm block, rising block, upper palm block\n" +
            "\nWhat is free sparring?\n ANSWER : ILBO matsoki, IBO Matsoki, SANG Matsoki, JAO Matsoki\n" +
            "\nName 7 hand parts?\n ANSWER : Ap Joomuk, Dung Joomuk, Yop Joomuk, Bandalson, Sonkut,\n" +
            "Sonkul, Sonbadak\n" +
            "\n" +
            "\nName 5 foot parts?\n ANSWER : Balkal, Balkal Dung, Dwit Chook, Dwit Kumchi, Ap Kumchi\n" +
            "\nWhat is the meaning of Yul-Gok?\n ANSWER : As above\n" +
            "\nExplain the meanings of any 3 (chosen be examiner) patterns prior to Yul- Gok?\n" +
            "\nName the following techniques in Korean:\n" +
            "\nSide back fist \n ANSWER : Yop Dung Joomuk\n" +
            "\nStraight fingertip thrust \n ANSWER : Son Sonkut Tulgi\n" +
            "\nMiddle obverse punch \n ANSWER : Kaunde Baro Jurigi\n" +
            "\nHigh obverse punch \n ANSWER : Nopunde Baro Jurigi\n" +
            "\nFront snap kick \n ANSWER : Ap cha Busigi\n" +
            "\nKnife hand strike \n ANSWER : Sonkal Taerigi\n" +
            "\nTwin upset punch \n ANSWER : Sang Dwijibo Jirugi\n" +
            "\nExplain x-stance?\n ANSWER : To gather distance to get closer to your opponent\n" +
            "\nWhat is the meaning of blue belt?\n ANSWER : As above\n" +
            "\nWhat happened on April 11th 1955?\n ANSWER : General Choi Hung Hi presented the name\n" +
            "Taekwon-do to a leading board of Korean officials and high ranking politicians and" +
            "the name Taekwon-do was accepted.\n" +
            "\nTell me all you know about the following blocks:\n" +
            "\nDouble forearm block \n ANSWER : Your answer\n" +
            "\nTwin knife hand block \n ANSWER : Your answer\n" +
            "\nKnife hand guarding block \n ANSWER : Your answer\n" +
            "\nHooking block \n ANSWER : Your answer\n" +
            "\nWhat is Korean for the following techniques:\n" +
            "\nFront elbow \n ANSWER : Ap Palkup\n" +
            "\nHooking kick \n ANSWER : Golcho Chagi\n" +
            "\nReverse turning kick \n ANSWER : Bandae Dollyo Chagi\n" +
            "\nUpward knee kick \n ANSWER : Ollyo Moorup Chagi\n" +
            "\nFront elbow strike \n ANSWER : Wi Palkup Taerigi\n" +
            "\nFree sparring \n ANSWER : Jao Matsoki\n" +
            "\nTwo step sparring \n ANSWER : Ibo Matsoki",

            fivetips="BOW WHEN ENTERING DOJANG\n" +
                    "\nLINE UP\n" +
                    "\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
                    "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
                    "\nIDENTIFY YOURSELF\n" +
                    "\n20 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
                    "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
                    "\nPATTERN : YUL-GOK\n" +
                    "\nALL PREVIOUS GRADE PATTERNS\n" +
                    "\nSAJO JURIGI NO 1, SAJO JURIGI NO 2, SAJO MAKI\n" +
                    "\nL-STANCE TWIN FOREARM BLOCK\n" +
                    "\nTURNING KICK, SIDE KICK, REVERSE TURNING KICK, BACK KICK POSSIBLE A COMBINATION\n" +
                    "OF 3 KICKS\n" +
                    "\nFRONT SNAP KICK\n" +
                    "\n3-STEP SPARRING + 2-STEP SPARRING + 2 STEP SPARRING EXTRA\n" +
                    "\nKNIFEHAND GUARDING BLOCK IN L-STANCE\n" +
                    "\nSITTING STANCE DOUBLE PUNCH\n" +
                    "\nFREE SPARRING\n" +
                    "\nDEMONSTRATION OF KICKING A PAD\n" +
                    "\nDEMONSTRATION OF STRIKING A PAD\n" +
                    "\nTHINGS TO FOCUS ON : EXECUTION OF X STANCE, HIGH DOUBLE FOREARM BLOCKS," +
                    "CORRECT HAND POSITION FOR HOOKING BLOCK, POWER, TIMING, CONCENTRATION," +
                    "STRAIGHT FINGER TIP THRUST IN CORRECT POSITION, MAINTAIN GOOD CIRCULAR BLOCK," +
                    "BENDING READY STANCE, TWIN VERTICAL PUNCH (2-STEP SPARRING), ACCURACY WHEN" +
                    "SPARRING AND STEP SPARRING",

            fivetheory="YUL-GUK WAS REGARDED AS ONE OF THE GREATEST\n" +
                    "SCHOLARS. HE WAS NICKNAMED THE CONFUSIUS OF KOREA. HE WROTE APPROX 280\n" +
                    "PUBLICATIONS AND WAS ALSO A GOVERNMENT OFFICIAL. HE WAS A GREAT CONFUSIAN\n" +
                    "SCHOLAR. BORN IN GANGWON PROVINCE, BY THE AGE OF 7 HE HAD COMPLETED HIS LESSONS\n" +
                    "ON CONFUCIAN CLASSICS. PASSED THE CIVIL SERVICE LITERARY EXAM BEFORE HE WAS 13,\n" +
                    "MARRIED AT 22, STUDIED CONFUCIANISM AND BEGAN WRITING WITH MUCH ACCLAIM. HE\n" +
                    "TOOK UP MANY MINISTERIAL POLITICAL POSITIONS IN KOREAN GOVERNMENT BUT LEFT\n" +
                    "OFFICE IN 1583 AND DIED THE FOLLOWING YEAR.\n" +
                    "FURTHER TERMINOLOGY \n HOOKING KICK = GOLCHO CHAGI, \nFRONT ELBOW STRIKE = AP\n" +
                    "PALKUP TAERIGI, \nINWARD PALM BLOCK = ANAERO SONBADAK MAKGI, \nX-STANCE =\n" +
                    "KYOCHA SOGI",

            sixintro="GREEN – SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP\n" +
                    "\n" +
                    "BLUE – SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES.\n" +
                    "\n" +
                    "PATTERN – WON-HYO\n" +
                    "\n" +
                    "WON-HYO WAS THE NOTED MONK WHO INTRODUCED BUDDHISM TO THE SILLA DYNASTY IN THE YEAR 686 A.D\n" +
                    "\n" +
                    "28 MOVEMENTS, PATTERN DIAGRAM CAPITAL I\n",
            sixtips="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n"+"\nBOW WHEN ENTERING DOJANG\n" +
                    "\nLINE UP\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
                    "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
                    "\nIDENTIFY YOURSELF\n" +
                    "\n20 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
                    "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
                    "\n" +
                    "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
                    "\nPATTERN : WON HYO\n" +
                    "\nALL PREVIOUS GRADE PATTERNS\n" +
                    "\nSAJO JURIGI NO 1\n" +
                    "\nDAJO JURIGI NO 2\n" +
                    "\nSAJO MAKI\n" +
                    "\nL-STANCE TWIN FOREARM BLOCK\n" +
                    "\nTURNING KICK, SIDE KICK, REVERSE TURNING KICK, BACK KICK POSSIBLE A COMBINATION\n" +
                    "OF 2 KICKS\n" +
                    "\nFRONT SNAP KICK\n" +
                    "\n3-STEP SPARRING + 2-STEP SPARRING\n" +
                    "\nKNIFEHAND GUARDING BLOCK IN L-STANCE\n" +
                    "\nSITTING STANCE DOUBLE PUNCH\n" +
                    "\nFREE SPARRING",
            sixquiz="What is 2 step sparring in Korean?\n ANSWER: IBO MATSOKI\n" +
                    "\nWhat/why do we learn 2 step sparring?\n ANSWER: FOR THE INTERMEDIATE STUDENT,\n" +
                    "TIMING, DIFFERENT TECHNIQUES, CO-ORDINATION, BLOCKING &" +
                    "ATTACKING MOVES\n" +
                    "\nExplain everything you know about bending stance. GUBURYO SOGI, BACK LEG" +
                    "SLIGHTLY BENT, KNEE FACING FORWARDS, ALLOWS KICK FROM FRONT" +
                    "LEG\n" +
                    "\nExplain everything you know about fixed stance.\n ANSWER: GOJONG SOGI, BODY WEIGHT" +
                    "50/50, GOOD STABILITY\n" +
                    "Six hand-parts in Korean\n ANSWER: AP JOOMUK (FOREFIST) DUNG JOOMUK (BACKFIST)\n" +
                    "YOP JOOMUK (SIDE FIST) SONKUT (FINGERTIPS) SONKUL (KNIFEHAND)" +
                    "BANDALSON (ARC HAND)\n" +
                    "Explain everything you know about the first 3 moves of Won Hyo.\n ANSWER: CLOSED READY" +
                    "STANCE A (MOA CHUNBI SOGI) L-STANCE TWIN FOREARM BLOCK (SANG" +
                    "PALMOK MAKGI), L-STANCE INWARD KNIFEHAND STRIKE (ANAERO SONKUL" +
                    "TAERIGI) FIXED STANCE MIDDLE PUNCH (BARO JIRUGI) READY STANCE \n" +
                    "\n" +
                    "SEE BELOW FOR FURTHER INFO, \nTO BLOCK AN ATTACK, PULL ATTACKER IN" +
                    "WITH ETENDED HAND THEN KHIFEHAND STRIKE TO NECK, MIDDLE PUNCH" +
                    "TO FORCE ATTACKER BACK." +
                    "NAME back kick DWIT CHAGI" +
                    "\nWhy are there meanings to patterns?\n ANSWER: PATTERNS REFER TO EITHER AN" +
                    "HISTORICAL EVENT OR A PERSON WITH GREAT IMPORTANCE FROM" +
                    "KOREAN HISTORY.\n" +
                    "\nWhat is a pattern?\n ANSWER: SET OF MOVEMENTS BOTH IN ATTACK & DEFENCE, SET IN" +
                    "A LOGICAL SEQUENCE DESIGNED TO DEAL WITH AN IMAGINARY" +
                    "OPPONENT. (OR TWO DEAL WITH MORE THAN ONE OPPONENT)" +
                    "\nWhy do we perform patterns? \n ANSWER:IMPROVE TECHNIQUES LEARNT IN CLASS," +
                    "IMPROVE TIMING, FLEXIBILITY, IMPROVE BREATHING AND BALANCE," +
                    "DEVELOP MUSCLES.\n" +
                    "\nWhat do you do for your TAEKWON-DO school?\n ANSWER: Your answer\n" +
                    "\nMeaning of Won-Hyo?\n ANSWER: AS ABOVE\n" +
                    "\nName four foot parts in Korean?\n ANSWER: AP KUMCHI (BALLS OF FEET), BALKAL" +
                    "(FOOTSWORD) DWIT CHOOK (BACK HEEL) BALKAL DUNG (REVERSE" +
                    "FOOTSWORD)\n" +
                    "\nWhy free sparring?\n ANSWER: JAO MATSOKI, TO PRACTISE CONTROLLED KICKING AND" +
                    "PUNCHING AGAINST OTHER OPPONENTS, IN COMPETITION YOU WOULD" +
                    "TRY TO WIN POINTS BY HITTING CERTAIN TARGET AREA. IT IS IMPORTANT" +
                    "TO MAINTAIN CONTROL WHEN SPARRING",

            sixtheory="WON-HYO WAS REGARDED AS ONE OF THE GREATEST\n" +
                    "THINKERS IN KAREAN BUDDISM. HE WAS ALSO A WRITER WHO PRODUCED EIGHTY-SIX WORKS," +
                    "MANY ARE STILL IN USE TODAY. HE WAS A SCHOLAR AND AN INFLUENTIAL FIGURE IN THE" +
                    "DEVELOPMENT OF THE EAST ASIAN BUDDIST INTELLECTUAL TRADITIONS. HE WAS ALSO" +
                    "FAMOUS FOR BRINGING BUDDISM TO THE GENERAL PUBLIC BY WAY OF SINGING AND" +
                    "DANCING IN THE STREETS. HE HAD A SON CALLED SOEL CHONG WHO BECAME ONE OF THE" +
                    "GREAT CONFICIAN SCHOLARS OF SILLA.\n" +
                    "\nREADY STANCE  OFTEN REFERRED TO AS REPRESENTING OPPOSITES (KOREAN FLAG)" +
                    "HOT/COLD, HARD/SOFT, FIRE/WATER, BUT THE COMMON ANSWER IS WAR/PEACE. (CLENCHED" +
                    "RIGHT FIST FOR WAR & SOFT LEFT HAND OVER FIST FOR PEACE. SHOULD BE HELD 30CM FROM" +
                    "YOUR PHILTRUM. THERE IS ALSO ANOTHER THEORY WHICH REFERS TO BUDDIST CHANTING" +
                    "WITH A WOODEN PERCUSSION INSTRUMENT CALLED A MOKTAK. CLOSED READY STANCE A,B" +
                    "& C ARE REFERRED TO AS THE DIFFERENT POSITIONS BUDDIST WOULD HOLD THE MOKTAK" +
                    "WHEN CHANTING OR RECITING. ANOTHER THEORY SUGGEST THE STANCES CONVEY A SENSE" +
                    "OF CALM, HUMILITY & MODESTY."+"\n\nFURTHER TERMINOLOGY – \nELBOW = PALKUP\nFREE SPARRING = JAO MATSOKI" +
                    " \nUPWARD PALM BLOCK = OLLYO SONBADAK MAKGI \nCIRCULAR BLOCK = DOLLYMIO MAKGI",

            sevnintro="KNOW YELLOW BELT & GREEN BELT MEANINGS\n" +
                    "\n" +
                    "YELLOW – SIGNIFIES EARTH, FROM WHICH A PLANT SPROUTS AND TAKES ROOT AS THE TAEKWON-DO FOUNDATION IS BEING LAID.\n" +
                    "\n" +
                    "GREEN – SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP\n" +
                    "\n" +
                    "PATTERN – DO SAN\n" +
                    "\n" +
                    "DO SAN IS THE PSEUDONYM OF THE PATRIOT AHN CH’ANG HO (1876-1938) WHO DEVOTED HIS ENTIRE LIFE FURTHERING THE EDUCATION OF KOREA AND IT’S INDEPENDENT MOVEMENT. \n" +
                    "\n" +
                    "24 MOVEMENTS, PATTERN DIAGRAM STRAIGHT EDGED S\n" +
                    "\n",
            sevnquiz="Why are there 24 patterns?\n ANSWER: EACH PATTERN REPRESTENTS 24 HOURS OF A DAY IN THE LIFE" +
                    "OF MAN.\n" +
                    "\nDemonstrate wedging block and name it in Korean?\n ANSWER: HECHYO MAKGI\n" +
                    "\nExplain the release move in your highest pattern?\n ANSWER: JAPPYOSOL TAE\n" +
                    "\nExplain turning kick?\n ANSWER: DOLLYO CHAGI\n" +
                    "\nWhat do we gain from doing 3 step sparring?\n ANSWER: TIMING, CO-ORDINATION, DISTANCE," +
                    "BALANCE, CONTROL, TARGET AREAS, DIFFERENT TECHNIQUES\n" +
                    "\nWhat is 3 step sparring in Korean? \n ANSWER:SAMBO MATSOKI\n" +
                    "\nName 3 hand-parts in English and Korean?\n ANSWER: AP JOOMUK (FOREFIST) DUNG JOOMUK (BACK" +
                    "FIST) SONKUL (KNIFEHAND)\n" +
                    "\nName 3 foot-parts in English and Korean?\n ANSWER: BALKAL (FOOTSWORD) AP KUMCHI (BALLS OF" +
                    "FEET) DWIT KUMCHI (BACK HEEL)\n" +
                    "\nWhat is free sparring in Korean?\n ANSWER: JAO MATSOKI" +
                    "\n" +
                    "\nWhat do we gain from learning to spar?\n ANSWER: DISTANCE, TIMING, SPEED, USE OF DIFFERENT" +
                    "TECHNIQUES, CO-ORDINATION, STAMINA, TIMING, BALANCE\n" +
                    "\nMeaning of Green belt? \n ANSWER:AS ABOVE\n" +
                    "\nName 3 different kicks in English & Korean?\n ANSWER: YOP CHAGI (SIDE KICK) DOLLYO CHAGI" +
                    "(TURNING KICK) DWIT CHAGI (BACK KICK)",

            sevntips="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n" +
                    "\n"+"BOW WHEN ENTERING DOJANG\n" +
                    "\nLINE UP\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
                    "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
                    "\nIDENTIFY YOURSELF\n" +
                    "\n20 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
                    "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
                    "\nPATTERN  DO SAN\n" +
                    "\nALL PREVIOUS GRADE PATTERNS\n" +
                    "\nSAJO JURIGI NO 1\n" +
                    "\nDAJO JURIGI NO 2\n" +
                    "\nSAJO MAKI\n" +
                    "\nL-STANCE TWIN FOREARM BLOCK\n" +
                    "\nTURNING KICK\n" +
                    "\nSIDE KICK\n" +
                    "\nREVERSE TURNING KICK\n" +
                    "\nBACK KICK\n" +
                    "\nFRONT SNAP KICK\n" +
                    "\n3-STEP SPARRING\n" +
                    "\nKNIFEHAND GUARDING BLOCK IN L-STANCE\n" +
                    "\nSITTING STANCE DOUBLE PUNCH",
            sevntheory="Ahn Chang ho, sometimes An Chang-ho was a Korean" +
                    "independence activist and one of the early leaders of the Korean-American immigrant" +
                    "community in the United States. He is also referred to as his pen name Do san. He established" +
                    "the Shinminhoe when he returned to Korea from the US in 1907. It was the most important" +
                    "organisation to fight the Japanese occupation of Korea. He established the Young Korean" +
                    "Academy in San Francisco in 1913 and was a key member in the founding of the Provisional" +
                    "Government of the Republic of Korea in Shanghai in 1919. An is one of two men believed to" +
                    "have written the lyrics of the Aegukga. (The South Korean national anthem). Besides his work" +
                    "for the Independence movement, Do San wanted to reform the Korean people&#39;s character and" +
                    "the entire social system of Korea. Educational reform and modernising schools were two key" +
                    "efforts of Do San. He was the father of actor Philip Ahn and U.S. Navy officer Susan Ahn Cuddy.",

            eightintro="YELLOW – SIGNIFIES EARTH, FROM WHICH A PLANT SPROUTS AND TAKES ROOT AS THE TAEKWON-DO FOUNDATION IS BEING LAID.\n" +
                    "\n" +
                    "GREEN – SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP\n" +
                    "\n" +
                    "PATTERN –  \n" +
                    "\n" +
                    "DAN-GUN IS NAMED AFTER THE HOLY DAN GUN, THE LEGENDARY FOUNDER OF KOREA IN THE YEAR 2333 B.C. \n" +
                    "\n" +
                    "21 MOVEMENTS, PATTERN DIAGRAM CAPITAL I",

            eightquiz="Name the three sections of the body in Korean?\n ANSWER: Nopunde, kaunde & najunde\n" +
                    "\nForefist in Korean?\n ANSWER: Ap-joomuk\n" +
                    "\nForearm in Korean?\n ANSWER: Palmok\n" +
                    "\nWhen was Taekwon-Do brought to England?\n ANSWER: 1967 by Master Rhee Ki ha - first" +
                    "school in Coventry\n" +
                    "\nWhat part of the foot is used for front snap kick? \n ANSWER:Ap KUMCHI\n" +
                    "\nWhat part of the foot is used for side piercing kick? \n ANSWER:(Balkal? - Foots word)\n" +
                    "\nExplain twin forearm block and name it in Korean. (Sang Palmok Makgi)\n" +
                    "\nExplain knife-hand guarding block and name it in Korean?\n ANSWER: Sonkal Daebi Makgi" +
                    "\nName three different kicks in Korean?\n ANSWER: Front snap kick (Ap cha Busigi), Rising Kick" +
                    "(Ap Chaolligi), Side kick (Yop chagi).\n" +
                    "\nWhat is inwards and outwards in Korean? Anaero & Bakaero\n" +
                    "\nWhat is upward and downward in Korean?\n ANSWER: Ollyo & Naeryo\n" +
                    "\nName 5 stances?\n ANSWER: Gunnun Sogi (WALKING), Annun Sogi (SITTING), Niunja (L-" +
                    "STANCE), Charyot (ATTENTION) & Narani Chunbi (READY)\n" +
                    "\nName four blocks ?\n ANSWER:- Twin Forearm Block (Sang Palmok makgi) Knife-hand guarding" +
                    "block (Sonkal daebi makgi), Middle section inner forearm block (Kaunde an Palmok" +
                    "makgi) & Lower section outer forearm block (Najunde Bakat Palmok makgi).\n" +
                    "\n"
                    ,
            eighttips="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n"+"\nBOW WHEN ENTERING DOJANG\n" +
                    "\nLINE UP\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
                    "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
                    "\n20 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
                    "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
                    "\nPATTERN  DAN-GUN\n" +
                    "\nPREVIOUS GRADE PATTERN\n" +
                    "\nSAJO JURIGI NO 1\n" +
                    "\nDAJO JURIGI NO 2\n" +
                    "\nSAJO MAKI\n" +
                    "\nL-STANCE TWIN FOREARM BLOCK\n" +
                    "\nTURNING KICK\n" +
                    "\nSIDE KICK\n" +
                    "\nBACK KICK\n" +
                    "\n3-STEP SPARRING\n" +
                    "\nKNIFEHAND GUARDING BLOCK IN L-STANCE\n" +
                    "\nSITTING STANCE DOUBLE PUNCH",
            eighttheory="Dangun Wanggeom was the legendary" +
                    "founder of GOJOSEON the first ever KOREAN Kingdom. He is said to be the &quot;Grandson of" +
                    "Heaven&quot; and &quot;Son of a Bear&quot; and to have founded the kingdom in 2333 BC. (Before Christ)." +
                    "The earliest recorded version of the Dangun legend appears in the 13th-century SAMGUK" +
                    "YUSA, which cites China&#39;s Book of Wei and Korea&#39;s lost historical record Gogi." +
                    "Dangun&#39;s ancestry legend begins with his grandfather Hwanin the &quot;Lord of Heaven&quot;. Hwanin" +
                    "had a son, Hwanung who yearned to live on the earth among the valleys and the mountains." +
                    "Hwanin permitted Hwanung and 3,000 followers to descend onto Baekdu Mountain, where" +
                    "Hwanung founded the Sinsi &quot;City of God&quot;). Along with his ministers of clouds, rain and" +
                    "wind, he instituted laws and moral codes and taught humans various arts, medicine, and" +
                    "agriculture.\n" +
                    "\n" +
                    "A tiger and a bear prayed to Hwanung that they might become human. Upon hearing their" +
                    "prayers, Hwanung gave them 20 cloves of garlic and a bundle of mugwort, ordering them to" +
                    "eat only this sacred food and remain out of the sunlight for 100 days. The tiger gave up after" +
                    "about twenty days and left the cave. However, the bear persevered and was transformed into" +
                    "a woman. The bear and the tiger are said to represent two tribes that sought the favor of the" +
                    "heavenly prince.\n" +
                    "The bear-woman Ungnyeo was grateful and made offerings to Hwanung. However, she" +
                    "lacked a husband, and soon became sad and prayed beneath a &quot;divine birch&quot; tree to be blessed" +
                    "with a child. Hwanung, moved by her prayers, took her for his wife and soon she gave birth to a son" +
                    "named Dangun Wanggeom.",
            ninei="WHITE BELT – SIGNIFIES THE BEGINNING STUDENT, SOMEONE WHO HAS NO PREVIOUS KNOWLEDGE OF TAEKWON-DO\n" +
                    "\n" +
                    "YELLOW – SIGNIFIES EARTH, FROM WHICH A PLANT SPROUTS AND TAKES ROOT AS THE TAEKWON-DO FOUNDATION IS BEING LAID.\n" +
                    "\n" +
                    "PATTERN – CHON-JI \n" +
                    "\n" +
                    "CHON-JI LITERALLY MEANS HEAVON & EARTH. IN THE ORIENT IT IS INTERPRETED AS THE CREATION OF THE WORLD OR THE BEGINNING OF HUMAN HISTORY, THEREFORE IT IS THE INITIAL PATTERN PLAYED BY THE BEGINEER. THIS PATTERN CONSISTS OF TWO SIMILAR PARTS, ONE TO REPRESENT HEAVEN AND THE OTHER TO REPRESENT THE EARTH.\n" +
                    "\n" +
                    "19 MOVEMENTS, PATTERN SYMBOL +\n",
            nineq="What is a tenet?\n ANSWER: Your answer\n" +
                    "\nWhat are the tenets of Taekwon-Do?\n ANSWER: Courtesy, Integrity, Perseverance, Self Control," +
                    "Indomitable Spirit\n" +
                    "\nWhat do they mean?" +
                    "\n ANSWER: Courtesy : Be polite to fellow students, senior grades & instructors\n" +
                    "Integrity : Honesty\n" +
                    "Perseverance : Keep trying, keep trying to develop and achieve your targets\n" +
                    "Self Control : Never lose your temper\n" +
                    "Indomitable Spirit : Show courage and belief when confronted with difficult" +
                    "situations, never stop believing\n" +
                    "\nWhen was Taekwon-Do formed? \n ANSWER:11 th April 1955\n" +
                    "\nWhat does ITF stand for \n ANSWER: International Taekwondo Federation\n" +
                    "\nName the founder of Taekwon-Do \n ANSWER: General Choi Hong Hi 9 th Degree Grand Master\n" +
                    "\nWhat is walking stance in Korean? \n ANSWER: Gunnon Sogi\n" +
                    "\nWhat is sitting stance in Korean? \n ANSWER: Annun Sogi\n" +

                    "\nWhat is L stance in Korean? \n ANSWER: Niunja Sogi\n" +
                    "\nMEANING OF PATTERN CHON-JI, NUMBER OF MOVES & SYMBOL OF" +
                    "PATTERN",
            ninet="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n"+"\nBOW WHEN ENTERING DOJANG\n" +
                    "\nLINE UP\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
                    "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
                    "\n20 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
                    "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
                    "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
                    "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
                    "\nPATTERN - CHON JI\n" +
                    "\nSAJO JURIGI NO 1\n" +
                    "\nDAJO JURIGI NO 2\n" +
                    "\nSAJO MAKI",
            ninetheory="Chon Ji is the foundation of all the 24 patterns. The" +
                    "stances and various movements once mastered form the basis of all the other patterns." +
                    "The meaning of Chon-Ji relates to the eastern philosophy of duality: Heaven & Earth, Dark" +
                    "& Light, Hot & Cold, Fire & Water. This is represented in the Korean Flag. The first part of" +
                    "this pattern is said to represent the Earth whereas the second part is said to signify the" +
                    "Heavens. Referred to as â€œHeavenly Lakeâ€ which is a real lake situated on the border" +
                    "between present day North Korea and China. The lake is found in a crater on the top of" +
                    "Paektu mountain (also spelt Baekdu) and is said to be the birth place of the Holy Dan Gun." +
                    "(refer to the next pattern in the Chang-Hon series  Dan- Gun)." +
                    "\nFURTHER TERMINOLOGY \n \nSTRIKE = TAERIGI \nBALL OF FOOT = AP KUMCHI \nRISING BLOCK = CHOOKYO MAKGI\n" +
                    " 3 STEP SPARRING = SAMBO MATSOKI \nFRONT SNAP KICK = AP CHA BUSIGI",
            teni="PLEASE NOTE THIS INFORMATION HAS BEEN PUBLISHED FOR ITF\n" +
                    "STUDENTS, HOWEVER WTF STUDENTS WILL GAIN BENEFIT FROM REVISING\n" +
                    "WITH THIS INFO.\n" +
                    "THIS APP HAS ALSO BEEN PRODUCED TO AID TAEKWON-DO STUDENTS\n" +
                    "WITH THEORY. IT IS ALSO IMPORTANT TO UNDERSTAND THAT DIFFERENT\n" +
                    "ORGANISATIONS MAY ASK ADDITIONAL QUESTIONS OR MAY REQUESTS OR\n" +
                    "PROVIDE YOU WITH DIFFERENT QUESTIONS.\n" +
                    "PLEASE CONSIDER THIS INFORMATION HAS BEEN PROVIDED FROM THE\n" +
                    "EXPERIENCE OF COMPLETING A 1 ST DEGREE BLACK BELT WITHIN AN\n" +
                    "ESTABLISHED ITF ORGANISATION",
            tenq="Where does Taekwon-Do come from?\n ANSWER : Korea (1950's)\n" +
                    "\nWhat is the name of your instructor?\n\n ANSWER :YOUR ANSWER" +
                    "\nWhat is your training suit in Korean? \n ANSWER :Dobok\n" +
                    "\nWhat is the training hall in Korean?\n ANSWER : Dojang\n" +
                    "\nWhat are the three rules of concentration?\n ANSWER : Focus your eyes, mind &" +
                    "body\n" +
                    "\nHow do we get power in our movements?\n ANSWER : Sign wave, Ki-ap, Timing," +
                    "Speed, Excelleration\n" +
                    "\nWhat are the benefits from doing Taekwon-Do?\n ANSWER : YOUR ANSWER\n" +
                    "\nWhat are Tae, Kwon and Do in English?\n ANSWER : Foot, Hand, Way",
            tent="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n"+"\nBOW WHEN ENTERING DOJANG\n" +
                    "LINE UP\n" +
                    "\nATTENTION STANCE\n" +
                    "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
                    "\n10 X PRESS UP'S, 10 X SIT UP'S\n" +
                    "\n10 X RISING KICKS (BOTH LEGS)\n" +
                    "\nSITTING STANCE PUNCHES\n" +
                    "\nRISING BLOCKS\n" +
                    "\nMIDDLE BLOCK\n" +
                    "\nWALKING STANCE PUNCH + MIDDLE REVERSE PUNCH\n" +
                    "\nSAJO JURIGI NO 1\n" +
                    "\nDAJO JURIGI NO 2\n" +
                    "\nSAJO MAKI",
            tentheory="",



    fstintro= "RED – SIGNIFIES DANGER, CAUTIONING THE STUDENT TO EXERCISE CONTROL WHILST WARNING THE " +
            "OPPONENT TO STAY AWAY.\n" +
            "\n" +
            "BLACK – SIGNIFIES MATURITY AND PROFICIENCY IN TAE KWON-DO. ALSO INDICATES THE WEARERS " +
            "IMPERVIOUSNESS TO DARKNESS AND FEAR.\n" +
            "\n" +
            "PATTERN – CHOONG-MOO\n" +
            "\n" +
            "Choong-Moo was the name given to the great Admiral Yi Sun Sin of the Yi Dynasty. " +
            "He was reputed to have invented the first armoured battleship (Kobukson) in 1592, " +
            "which was the precursor of the present day submarine. The reason that this pattern ends " +
            "with a left hand attack is to symbolize his regrettable death, having no chance to show his" +
            " unrestrained potentiality checked by the forced reservation of his loyalty to the king.\n" +
            "\n" +
            "30 MOVEMENTS, PATTERN DIAGRAM CAPITAL I\n" +
            "\n",




    fstquiz="What is the purpose of the 360 jump and spin into Knifehand guarding block in Choong-Moo? " +
            "\n ANSWER:To avoid a sweep or low attack and to disorientate the opponent.\n" +
            "\n" +
            "\nWhat is the Korean for X-knifehand checking block?\n ANSWER: Kyocha Sonkal Momchau Makgi\n" +
            "\nWhat is the Korean for upward twin palm block?\n  ANSWER: Ollyo Sang Sonbadak Makgi\n" +
            "\nWhat is the Korean for flying side kick?\n ANSWER Twimyo Yop Chagi\n" +
            "\nWhat is the Korean for sliding & explain it? \n ANSWER: Mikulgi  It is an effective technique for covering a long distance in one smooth motion.\n" +
            "\nWhich 3 patterns have release moves. Demo & explain the difference between them?\n ANSWER:  Do-san, Joong-Gun and Hwa-Rang.\n" +
            "\n" +
            "\nName 10 different blocks. Both in Korean & English? \n ANSWER: Najunde Bakat Palmok makgi, Kaunde An palmok makgi, Najunde Sonkal makgi, Chookyo makgi, Sang Palmok makgi, Sonkal Daebi makgi, Hechyo makgi, Dollymio makgi, Golcho makgi, Sang Sonkal makgi, Doo Palmok makgi, Digutcha makgi, Noolyo makgi, Sonkal Dung makgi, Kyocha Chookyo makgi, Kyocha Noollo makgi, miro makgi, San makgi, Ollyo Sang Sonbabak makgi and Kyocha Sonkal Momchau makgi.\n" +
            "\n" +
            "\nName 8 different kicks. Both in Korean & English? \n ANSWER: Apcha busigi (front snap kick), Yop chajirugi (side piercing kick), Dollyo chagi (turning kick), Dwit chajirugi (back piercing kick), Bandae Dollyo chagi (reverse turning kick), Naeryo chagi (downward or axe kick), Noollo chagi (pressing kick), Yonsok chagi (consecutive kick), Bituro chagi (twisting kick), Twimyo Yop chagi (flying side kick).\n" +
            "\nName 8 different stances. Both in Korean & English? \n ANSWER: Gunnon (Walking), Annun " +
            "(Sitting), Niunja(L stance), Narani (Parrallel), Charyot (Attention), Gojong (fixed)," +
            " Moa (closed), Goburyo (bending), Kyocha ( 'X' ), Nachuo (Low), Dwitbal (rear foot), Soojik (vertical), Sogi (Stance).\n" +
            "\n" +
            "\nName 8 different hand parts. Both in Korean & English? \n ANSWER: Fore fist (Ap Joomuk)," +
            " Back fist (Dung Joomuk), Side fist (Yop Joomuk), Knifehand (Sonkal), Reverse Knifehand " +
            "(Sonkal Dung), Finger tips (Sonkut), Arc Hand (Bandal Son), Elbow (Palkup), Palm (Sonbadak)," +
            " Open fist/palm heel (Pyun Joomuk)\n" +
            "\nName 5 different foot parts. Both in Korean & English? \n ANSWER: Ball of the foot (Apkumchi), Foot sword (Balkal), Instep (Baldung), Back Heel (Dwichook), Toes (Balkut), Back sole (Dwitkumchi), Reverse Foot sword (Balkal Dung) Knee (Moorup).\n" +
            "\nWhat is the Korean for Inward Reverse Knifehand strike? \n ANSWER: Anaero Ap Sonkal Dung Taerigi\n" +
            "\n" +
            "\nName the 4 parts of your forearm. Both in English and Korean? \n ANSWER:Bakat (outer) An (inner) Dung (back) Mit Palmok (forearm belly)\n" +
            "\nWhy do we perform patterns? \n ANSWER: To practice both offensive & defensive techniques against imaginary opponent, or opponents. To build strength, speed, balance and timing with your body.\n" +
            "\n" +
            "\nWhy do we learn the meanings of patterns? \n ANSWER: To gain inspiration and remember the history of those who came before us.\n" +
            "\n" +
            "\nWhat is Twisting kick in Korean & what part of the foot is used. Also what section for what height kick? \n ANSWER: Bituro Chagi â€“ Ball of the foot (Apkumchi). Low kick to the front, Middle kick to the 45 degree line and High kick over your shoulder to the side & behind you.\n" +
            "\nName 3 different fingertip thrusts. Both in Korean & English? \n ANSWER: Upset Finger tip thrust : Dwijibun Sonkut Tulgi, Flat finger tip thrust : Opun Sonkut Tulgi, Straight finger tip thrust: Sun Sonkut Tulgi.\n" +
            "\n" +
            "\nWhat does the colour Black signify, What is meant by impervious to darkness & fear? \n ANSWER: Black is the opposite of white, therefore signifying maturity and proficiency in Taekwon-Do. It also indicates the wearer's imperviousness to darkness and fear.\n" +
            "\nWhat is the Korean for Vertical stance & explain it? \n ANSWER: Soojik Sogi â€“ body weight 60/40, rear leg is primary as most weight is on it, both feet pointed 15 degrees inward, length of 1 shoulder width between big toes.\n" +
            "\n" +
            "\nHow have you supported your club and organization? \n ANSWER: Think of all the things you have done over the years.\n" +
            "\n" +
            "\nWhy do we do free sparring and 2 on 1 sparring? \n ANSWER: To learn how to block, evade and counter against more than one attacker.\n" +
            "\n" +
            "\nWhat is the difference between ITF & WTF Taekwon-do? \n ANSWER: ITF is Traditional based which allows punching to the head and WTF is more Sport based with no punching to the head\n" +
            "\n" +
            "\nWhat is the Korean for Head? \n ANSWER: Mori\n" +
            "\n" +
            "\nWho's your greatest inspiration in Taekwon-do? \n ANSWER: Your Answer!\n" +
            "\nWhy did you start Taekwon-do? Your Answer!\n" +
            "\nWhat is your greatest moment in Taekwon-do? \n ANSWER: Your Answer!\n" +
            "\nIf you had to pick a 6th tenet of Taekwon-do what would it be and why?\n ANSWER: Your Answer!\n" +
            "\nWhat is the difference between a martial artist & a sports person? \n ANSWER: Your Answer!\n" +
            "\nWhy do you want to be a Blackbelt? \n ANSWER: Your Answer!",


    fsttips="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG\n"+"BOW WHEN ENTERING DOJANG\n" +
            "\nLINE UP\n" +
            "\nATTENTION STANCE\n" +
            "\nBOW TO INSTRUCTOR & GRADING BOARD \n" +
            "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
            "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
            "\nIDENTIFY YOURSELF\n" +
            "\nPATTERNS  ALL UP TO CHOONG-MOO\n" +
            "\nPATTERN OF YOUR CHOICE\n" +
            "\nKICKING COMBINATIONS  MAYBE UP TO 4 TYPES OF KICKS (If you do Black Belt Pre- grading training your organisation may inform you of the routine a few days before the grading).\n" +
            "\nFREE SPARRING  FULL CONTACT, BUT YOU NEED TO SHOW CONTROL\n" +
            "\n1-STEP SPARRING : MIDDLE PUNCH ATTACKS BOTH SIDES - YOUR DEFENSE OR MOVEMENT OF LINE AND SINGLE COUNTER (ENSURE COUNTER AIMS AT DISABLING THE OPPONENT, QUICK & FAST)\n" +
            "\nTHEORY QUESTIONS AT EXAMINEERS TABLE\n" +
            "\nBREAKING  NORMAL ONE HAND & FOOT TECHNIQUE (However, this may vary dependant on your association  you will need to find this out)\n" +
            "\nBREAKING  IF YOU ARE A JUNIOR STUDENT YOU WILL BE ASKED TO SHOW A HAND & KICKING TECHNIQUE WITH A PAD\n" +
            "\nSPIRIT TEST  THIS IS PHYSICALLY DEMANDING SO ENSURE YOU ARE PHYSICALLY PREPARED TO WORK HARD FOR 30-45 Minutes\n" +
            "\n" +
            "\nENSURE ALL TECHNIQUES ARE PERFORMED CORRECTLY."+"\nNew: ITF CODE\n" +
            "\n" +
            "\nI shall observe the tenets of Taekwon-do.\n" +
            "\nI shall respect the instructor and seniors.\n" +
            "\nI shall never misuse Taekwon-do.\n" +
            "\nI shall be a champion of freedom and justice.\n" +
            "\nI shall build a more peaceful world.",

            fsttheory= "THEORY\n\n"+"Yi Sun-sin who saved Choson Korea from the brink of collapse during the Japanese invasion" +
                    " of 1592.  He is still dearly cherished in the hearts of Koreans today. " +
                    "In a nationwide survey conducted in April 2005, Yi Sun-sin was chosen as the greatest figure in Korean history. \n" +
                    "\n" +
                    "Admiral Yi achieved a battle record that no one in history has ever \n" +
                    "matched. The 23 battles that he fought at sea, Admiral Yi was never once " +
                    "defeated. Overcoming formidable odds in terms of numbers of ships and troops, " +
                    "he led his navy to victory in every engagement he fought during seven \n" +
                    "years of war with the Japanese, losing in total only two ships of his own.\n" +
                    "\n" +
                    "His whole career might be summarized by saying that, although he had no lessons from past history to serve as a guide, he waged war on the sea as it should be waged if it is to produce definite results, and ended by making the supreme sacrifice of a defender of his country. \n" +
                    "\n" +
                    "The following is an extract from a paper published by the Japanese Institute of Korean Studies. \n" +
                    "\n" +
                    "If there ever were an Admiral worthy of the name of ‘god of war’, that one is Yi Sun-sin. \n" +
                    "\n" +
                    "Admiral Yi, in contrast, has been held as an object of admiration and reverence even among the Japanese, whose minds were swayed by his pure and absolute loyalty to his country and people, his brilliant use of strategy and tactics which led invariably to victory, his invincible courage that overcame every adverse circumstance, and his unbending integrity. This \n" +
                    "admiration is apparent in the many speeches and writings by Japanese military officers and historians which speak of Admiral Yi.\n" +
                    "\n" +
                    "Yi was a supreme naval commander even on the basis of the limited literature of the Seven Years War, and despite the fact that his bravery and brilliance are not known to the West, since he had the misfortune to be born in Choson. \n" +
                    "\n" +
                    "Anyone who can be compared to Yi should be better than Michiel de Ruyter from Netherlands. Nelson is far behind Yi in terms of personal character and \n" +
                    "integrity. Yi was the inventor of the iron-clad warship known as the Turtle Ship (Geobukseon). He was a truly great commander and a master of the naval tactics of three hundred years ago.\n" +
                    "\n" +
                    "Sato Destaro (1866-1942), a vice-admiral of the Japanese Navy,\n" +
                    "A Military History of the Emperor, said Yi Sun-sin the Korean general who defeated the Japanese in every one of the battles at sea when \n" +
                    "Toyotomi Hideyoshi’s troops invaded Choson Korea. He was unique among Choson civil and military officers for his honesty and incorruptibility, and in terms of leadership and tactics, as well as loyalty and courage, he was an \n" +
                    "ideal commander almost like a miracle. He was a renowned admiral before the time of Nelson, and has never yet had an equal in world history. Although the existence of this figure grew to be almost forgotten in Korea, the admiration of his memory was handed down in Japan through generations so that his tactics and accomplishments were researched and subjected to close study when the Japanese Navy was established during the Meiji period.\n"+"Anyone who can be compared to Yi should be better than Michiel de Ruyter from Netherlands." +
                    " Nelson is far behind Yi in terms of personal character and \n" +
            "integrity. Yi was the inventor of the iron-clad warship known as the Turtle Ship (Geobukseon). He was a " +
                    "truly great commander and a master of the naval tactics of three hundred years ago.\n" +
            "\n" +
            "Sato Destaro (1866-1942), a vice-admiral of the Japanese Navy,\n" +
            "A Military History of the Emperor, said Yi Sun-sin the Korean general who defeated the Japanese in every one of the battles at sea when \n" +
            "Toyotomi Hideyoshi's troops invaded Choson Korea. He was unique among Choson civil and military officers for his honesty and incorruptibility, and in terms of leadership and tactics, as well as loyalty and courage, he was an \n" +
            "ideal commander almost like a miracle. He was a renowned admiral before the time of Nelson, and has never yet had an equal in world history. Although the existence of this figure grew to be almost forgotten in Korea, the admiration of his memory was handed down in Japan through generations so that his tactics and accomplishments were researched and subjected to close study when the Japanese Navy was established during the Meiji period.\n" +
            "\n" +
            "Yi is often compared with Admiral Nelson and Admiral Togo. All three men were heroes who fought for the destiny of their countries and saved their countrymen from foreign invasion by the securing of key naval victories. \n" +
            "However, the circumstances of Nelson's Battle at Trafalgar and of Togo's Battle at Tsushima differ strikingly from those of the Battle of Myongnyang fought by Admiral Yi.\n" +
            "\n" +
            "At the Battle of Trafalgar, England, a nation traditionally strong on the sea, was facing an enemy who was at that time inexperienced in naval warfare, and who commanded a fleet not much larger than her own (27 English ships against 33 French and Spanish ships). In the case of the Battle of Tsushima, also, the Japanese navy had the upper hand in many respects. The Russian crews of the Baltic fleet which opposed them were exhausted after a seven-month voyage which had taken them halfway round the world; the Arctic-born Russian crews had suffered greatly from outbreaks of disease as they sailed through the equator area. Taking this into account, it is of little surprise that an intensively trained Japanese Navy, in high morale and fighting near the mainland of Japan, emerged victorious over the dispirited Russian forces.\n" +
            "\n" +
            "He was demoted on 2 occasions to a foot soldier, this is what is referred to as his forced reservation of his loyalty to the king. As he was demoted then re-instated.\n" +
            "\n" +
            "On 15th December 1598 during a battle with the Japanese at Sachon bay Yi was struck by a stray bullet which entered his left armpit. The bullet was fatal and Yi died moments later.\n",


    sndintro="RED – SIGNIFIES DANGER, CAUTIONING THE STUDENT TO EXERCISE CONTROL WHILST WARNING THE OPPONENT TO STAY AWAY.\n" +
            "\n" +
            "BLACK – SIGNIFIES MATURITY AND PROFICIENCY IN TAE KWON-DO. ALSO INDICATES THE WEARERS IMPERVIOUSNESS TO DARKNESS AND FEAR.\n" +
            "\n" +
            "PATTERN – HWA RANG\n" +
            "\n" +
            "HWA-RANG is named after the Hwa Rang youth group which originated in the Silla Dynasty around 600 A.D." +
            " This group eventually became the driving force for the unification of the 3 kingdoms of Korea." +
            " The 29 movements of this pattern refer to the 29th Infantry division where Taekwon-do developed into maturity.\n" +
            "\n" +
            "37 MOVEMENTS, PATTERN DIAGRAM CAPITAL I\n",

    sndquiz="How many moves in Hwa-Rang? \n ANSWER: 29\n" +
            "\nMeaning of Hwa-Rang? \n ANSWER: Hwa-Rang is named after the Hwa-Rang youth group which " +
            "originated in the Silla Dynasty about 600 A.D. " +
            "This group eventually became the driving force for the unification of the three kingdoms of Korea ." +
            " The 29 movements refer to the 29th Infantry Division, where Tae Kwon-Do developed into maturity.\n" +
            "\nWhat is the 1st move of Hwa-Rang. Both in Korean & English and explain its purpose? \n ANSWER: Palm Pushing Block " +
            ": Sonbadak Miro Makgi\n" +
            "\nDemo & what is the Korean for downward Knifehand strike?\n ANSWER: Naeryo Sonkal Taerigi\n" +
            "\nWhat is the Korean for the upward punch in Hwa-Rang? \n ANSWER:` Ollyo Jirugi\n" +
            "\nWhat is the Korean for 'X' fist pressing block?\n ANSWER: Kyocha Joomuk Noollo makgi\n" +
            "\nWhat is the Korean for sliding & explain it? \n ANSWER: Mikulgi : It is an effective technique for covering a " +
            "long distance in one smooth motion.\n" +
            "\nWhich 3 patterns have release moves. Demo & explain the difference between them? \n ANSWER: Do-san, " +
            "Joong-Gun and Hwa-Rang.\n" +
            "\nName 8 different blocks. Both in Korean & English? \n ANSWER: Najunde Bakat Palmok makgi, Kaunde" +
            " An palmok makgi, Najunde Sonkal makgi, Chookyo makgi, Sang Palmok makgi, Sonkal Daebi makgi, Hechyo makgi, Dollymio makgi, Golcho makgi, Sang Sonkal makgi, Doo Palmok makgi, Digutcha makgi, Noolyo makgi, Sonkal Dung makgi, Kyocha Chookyo makgi, Kyocha Noollo makgi, miro makgi and San makgi\n" +
            "\nName 7 different kicks. Both in Korean & English? \n ANSWER: Apcha busigi (front snap kick)," +
            " Yop chajirugi (side piercing kick), Dollyo chagi (turning kick), Dwit chajirugi (back piercing kick), " +
            "Bandae Dollyo chagi (reverse turning kick), Naeryo chagi (downward or axe kick), Noollo chagi (pressing kick), Yonsok chagi (consecutive kick), Bituro chagi (twisting kick)\n" +
            "\n" +
            "\nName 7 different stances. Both in Korean & English? \n ANSWER: Gunnon (Walking), Annun (Sitting)," +
            " Niunja(L stance), Narani (Parrallel), Charyot (Attention), Gojong (fixed), Moa (closed), " +
            "Goburyo (bending), Kyocha ( â€œXâ€ ), Nachuo (Low), Dwitbal (rear foot), Soojik (vertical), Sogi (Stance).\n" +
            "\nName 8 different hand parts. Both in Korean & English? \n ANSWER: Fore fist (Ap Joomuk), Back fist (Dung Joomuk), " +
            "Side fist (Yop Joomuk), Knifehand (Sonkal), Reverse Knifehand (Sonkal Dung), Finger tips (Sonkut), " +
            "Arc Hand (Bandal Son), Elbow (Palkup), Palm (Sonbadak), Open fist/palm heel (Pyun Joomuk)\n" +
            "\n" +
            "\nName 5 different foot parts. Both in Korean & English? \n ANSWER: Ball of the foot (Apkumchi), " +
            "Foot sword (Balkal), Instep (Baldung), Back Heel (Dwichook), Toes (Balkut), Back sole (Dwitkumchi), " +
            "Reverse Foot sword (Balkal Dung) Knee (Moorup).\n" +
            "\nWhen performing L-stance & punch in Hwa-Rang. Is it obverse or reverse & why? \n ANSWER: Obverse (Baro), as rear " +
            "leg is the primary leg and the rear hand used.\n" +
            "\nWhat is the ready position in Hwa-Rang. Both in Korean & English? \n ANSWER:" +
            " Closed ready stance 'C' Moa jumbi sogi 'C' (Hands 10cm from the lower abdomen)\n" +
            "\n" +
            "\nName the 4 parts of your forearm. Both in English and Korean? \n ANSWER: Bakat (outer), An (inner)," +
            " Dung (back), Mit Palmok (forearm belly)\n" +
            "\nWhy do we perform patterns? \n ANSWER: To practice both offensive & defensive techniques " +
            "against imaginary opponent, or opponents. To build strength, speed, balance and timing with your body.\n" +
            "\nWhy do we learn the meanings of patterns? \n ANSWER: To gain inspiration and remember the history " +
            "of those who came before us.\n" +
            "\nWhat is Twisting kick in Korean & what part of the foot is used? \n ANSWER: Bituro Chagi - Ball of the foot (Apkumchi).\n" +
            "\nName 3 different fingertip thrusts. Both in Korean & English? \n ANSWER:" +
            " Upset Finger tip thrust - Dwijibun Sonkut Tulgi, Flat finger tip thrust - Opun Sonkut Tulgi, " +
            "Straight finger tip thrust - Sun Sonkut Tulgi.\n" +
            "\nWhat does the colour Black signify, What is meant by impervious to darkness & fear? \n ANSWER: Black is the opposite " +
            "of white, therefore signifying maturity and proficiency in Taekwon-Do." +
            " It also indicates the wearer's imperviousness to darkness and fear.\n" +
            "\n" +
            "\nHow have you supported your club and organization? \n ANSWER: Think of all the things you have done over the years.\n" +
            "\nWhy do we do free sparring and 2 on 1 sparring? \n ANSWER: To learn how to block, evade and counter against more than one attacker.\n" +
            "\nWhat is the difference between ITF & WTF Taekwon-do? \n ANSWER: ITF is Traditional" +
            " based which allows punching to the head and WTF is more Sport based with no punching to the head\n" +
            "\n" +
            "Why do we perform 1-step sparring? \n ANSWER: To practice and learn distance, timing and correct target areas\n" +
            "\n" +
            "Why do we perform patterns? \n ANSWER: To practice both offensive & defensive " +
            "techniques against imaginary opponent, or opponents. To build strength, speed, balance and timing with your body.\n" +
            "\n" +
            "Why do we learn the meanings of patterns? \n ANSWER: To gain inspiration and remember the history of those who came before us.\n",

    sndtips="BOW WHEN ENTERING DOJANG\n" +
            "\nLINE UP\n" +
            "\nATTENTION STANCE\n" +
            "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
            "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
            "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
            "\nIDENTIFY YOURSELF\n" +
            "\n20 X PRESS UP'S, 20 X SIT UP'S, 30 x STAR JUMPS, \n" +
            "\n10 X RISING KICKS (BOTH LEGS)\n" +
            "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
            "\n10 x SITTING STANCE DOUNBLE PUNCH\n" +
            "\n4 x FORWARDS & 4 X BACKWARDS MIDDLE INNER FOREARM BLOCK, REVERSE MIDDLE PUNCH WALKING STANCE\n" +
            "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
            "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
            "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
            "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
            "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
            "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
            "\nAS ABOVE ALL BLOCKS UP TO GRADE\n" +
            "\nPATTERN - HWA RANG  \n" +
            "\nALL PREVIOUS GRADE PATTERNS\n" +
            "\nSAJO JURIGI NO 1, SAJO JURIGI NO 2, SAJO MAKI\n" +
            "\nL-STANCE TWIN FOREARM BLOCK\n" +
            "\nTURNING KICK, SIDE KICK, REVERSE TURNING KICK, BACK KICK POSSIBLE A COMBINATION OF 4 KICKS\n" +
            "\n3-STEP SPARRING + 2-STEP SPARRING + 2 STEP SPARRING EXTRA + 1-STEP SPARRING\n" +
            "\n1-STEP SEMI FREE SPARRING (3 STEP ATTACKS - YOUR DEFENSE OR MOVEMENT OF LINE AND SINGLE COUNTER) ENSURE COUNTER AIMS AT DISABLING THE OPPONENT\n" +
            "\nKNIFEHAND GUARDING BLOCK IN L-STANCE, SITTING STANCE DOUBLE PUNCH\n" +
            "\nFREE SPARRING, DEMONSTRATION OF 2 X KICK TO PAD, DEMONSTRATION OF 2 X STRIKES TO PAD\n" +
            "\nPattern - Students Choice excluding Toi-Gye\n" +
            "\nPattern - Set by the Grading Examiner\n" +
            "BE PREPARED FOR QUESTIONS DURING GRADING\n" +
            "\nTWISTING KICK - Bituro Chagi\n" +
            "\nTHINGS TO FOCUS ON - ENSURE NEW TECHNIQUES ARE PERFORMED CORRECTLY, W SHAPE BLOCK, DO NOT LEAN FORWARD " +
            "ON CIRRCULAR BLOCK, ENSURE YOU HAVE CORRECT TARGET AREAS FOR ALL ATTACKS, EXECUTION OF X STANCE," +
            " HIGH DOUBLE FOREARM BLOCKS, CORRECT HAND POSITION FOR HOOKING BLOCK, POWER, TIMING, CONCENTRATION," +
            " STRAIGHT FINGER TIP THRUST IN CORRECT POSITION, MAINTAIN GOOD CIRCULAR BLOCK, BENDING READY STANCE," +
            " TWIN VERTICAL PUNCH (2-STEP SPARRING), ACCURACY WHEN SPARRING AND 1 STEP SPARRING COUNTER ATTACKS " +
            "MUST BE TARGETING A POINT WHICH COULD DISABLE YOUR ATTACKER (NO CONTACT).\n" +
            "\nNew:\n" +
            "\nTwisting Kick – Bituro\n" +
            "\nSweeping – Hullyo\n" +
            "\nInstep – Baldung",

            sndtheoy= "Elite group of Loyalist to the king." +
                    " 3 kingdoms were Silla , Goguryeo & Baekje. Known as 'The Flower boys' they" +
                    " were educational institutions in the 10th Century, members gathered for all aspects of study," +
                    " arts, culture & stemming mainly from Buddhism. Chinese sources referred to the physical beauty of " +
                    "the Flower boys. Few Koreans are said  to have known anything the history of the Hwa Rang until after " +
                    "the liberation of 1945, after which the Hwa Rang were elevated to a symbolic importance.\n" +
                    "\n",


    thrdintro="BLUE – SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES.\n" +
            "\n" +
            "RED – SIGNIFIES DANGER, CAUTIONING THE STUDENT TO EXERCISE CONTROL WHILST WARNING THE OPPONENT TO STAY AWAY.\n" +
            "\n" +
            "PATTERN – TOI-GYE\n" +
            "\n" +
            "TOI-GYE is the penname of the noted scholar Yi-Hwang. (16th Century A.D) an authority on neo-Confucianism. The 37 movements of the pattern refer to his birthplace on the 37degree latitude, and the diagram represents scholar.\n" +
            "\n" +
            "37 MOVEMENTS, PATTERN DIAGRAM CAPITAL I\n",

    thrdquiz="How many moves in Toi-Gye? \n ANSWER: 37\n" +
            "\nMeaning of Toi-Gye? \n ANSWER: As above\n" +
            "\nWhat is the 2nd and 5th move of Toi-Gye. Both in Korean & English and explain its target area? \n ANSWER:" +
            " Upset Finger tip thrust - Dwijibun Sonkut Tulgi - Lower Abdomen\n" +
            "\nDemo & what is the Korean for slow twin side elbow thrust? \n ANSWER: Sang Yop Palkup Tulgi - performed in slow motion\n" +
            "\nWhat is the Korean for 'W' shape block? \n ANSWER: San makgi\n" +
            "\nWhat is the Korean for 'X' fist pressing block? \n ANSWER: Kyocha Joomuk Noollo makgi\n" +
            "\nWhat is the Korean for twin front grasp & explain it? \n ANSWER: Sang Ap Japki\n" +
            "\nWhat is the Korean for upward Knee kick & explain it? \n ANSWER: Ollyo Moorup Chagi\n" +
            "\n" +
            "\nName 8 different blocks. Both in Korean & English? \n ANSWER: Najunde Bakat Palmok makgi, " +
            "Kaunde An palmok makgi, Najunde Sonkal makgi, Chookyo makgi, Sang Palmok makgi, Sonkal Daebi makgi, " +
            "Hechyo makgi, Dollymio makgi, Golcho makgi, Sang Sonkal makgi, Doo Palmok makgi, Digutcha makgi," +
            " Noolyo makgi, Sonkal Dung makgi, Kyocha Chookyo makgi, Kyocha Noollo makgi, miro makgi and San makgi\n" +
            "\n" +
            "\nName 6 different kicks. Both in Korean & English? \n ANSWER: Apcha busigi (front snap kick), " +
            "Yop chajirugi (side piercing kick), Dollyo chagi (turning kick), Dwit chajirugi (back piercing kick)," +
            " Bandae Dollyo chagi (reverse turning kick), Naeryo chagi (downward or axe kick)," +
            " Noollo chagi (pressing kick), Yonsok chagi (consecutive kick)\n" +
            "\n" +
            "\nName 7 different stances. Both in Korean & English? \n ANSWER: Gunnon (Walking), Annun (Sitting)," +
            " Niunja(L stance), Narani (Parrallel), Charyot (Attention), Gojong (fixed), Moa (closed), Goburyo (bending), " +
            "Kyocha ( 'X' ), Nachuo (Low), Dwitbal (rear foot) Sogi (Stance).\n" +
            "\n" +
            "\nName 7 different hand parts. Both in Korean & English? \n ANSWER: Fore fist (Ap Joomuk), " +
            "Back fist (Dung Joomuk), Side fist (Yop Joomuk), Knifehand (Sonkal), Reverse Knifehand (Sonkal Dung)," +
            " Finger tips (Sonkut), Arc Hand (Bandal Son), Elbow (Palkup), Palm (Sonbadak), Open fist/palm heel (Pyun Joomuk)\n" +
            "\n" +
            "\nName 5 different foot parts. Both in Korean & English? \n ANSWER: Ball of the foot (Apkumchi)," +
            " Foot sword (Balkal), Instep (Baldung), Back Heel (Dwichook), Toes (Balkut), Back sole (Dwitkumchi), " +
            "Reverse Foot sword (Balkal Dung) Knee (Moorup).\n" +
            "\n" +
            "\nName 6 different hand attacks from pattern Joong-Gun. Both in Korean & English? \n ANSWER: Upward elbow strike" +
            " - Wi Palkup Taerigi, Angle punch - Giokja Jirugi, Twin upset punch - Sang Dwijibo Jirugi, Twin Vertical punch -" +
            " Sang Sewo Jirugi, Ap Fore fist punch - Ap joomuk Jirugi, Backfist Strike - Dung Joomuk Taerigi\n" +
            "\n" +
            "What is the ready position in Toi-Gye. Both in Korean & English? \n ANSWER:" +
            " Closed ready stance 'B' - Moa jumbi sogi 'B' (Hands 15cm from the Navel)\n" +
            "\n" +
            "\nWhy do we perform 1-step sparring? \n ANSWER: To practice and learn distance, timing and correct target areas\n" +
            "\n" +
            "\nWhy do we perform patterns? \n ANSWER: To practice both offensive & defensive techniques against imaginary opponent, or opponents. To build strength, speed, balance and timing with your body.\n" +
            "\n" +
            "\nWhy do we learn the meanings of patterns? \n ANSWER: To gain inspiration and remember the history of those who came before us.\n" +
            "\n" +
            "\nHow do we generate power into W-shape block? \n ANSWER: By using our body weight whilst turning and landing into the block.\n" +
            "\n" +
            "\nName 3 different fingertip thrusts. Both in Korean & English? \n ANSWER: Upset Finger tip thrust - Dwijibun " +
            "Sonkut Tulgi, Flat finger tip thrust - Opun Sonkut Tulgi, Straight finger tip thrust Sun Sonkut Tulgi.\n" +
            "\n" +
            "\nWhat does the colour red signify? \n ANSWER: Red signifies danger, cautioning the student to exercise control, whilst warning the opponent to stay away.\n" +
            "\n" +
            "\nWhat is the Korean for Low Double Forearm Pushing Block? \n ANSWER: Najunde Doo Palmok Miro Makgi\n" +
            "\n" +
            "Questions from previous Gradings! \n ANSWER: Remember and know your previous Theory such as :\n" +
            "\n" +
            "\nWhy one step sparring?\n ANSWER: Main benefit is developing fast reflexes as the defending student you do" +
            " not know what attack is coming.\n" +
            "\nTell me more about historical figures? Your answer\n" +
            "\nMeaning of Joong- Gun \n ANSWER: As above\n" +
            "\nWhat is the pattern diagram for Joong-Gun? \n ANSWER: CAPITAL I\n" +
            "\nWhat does the diagram represent?\n ANSWER: Scholar\n" +
            "\nWhat do we get from performing a pattern?\n ANSWER: Inspiration\n" +
            "\nWho inspires you?\n ANSWER: Your answer\n" +
            "\nWho is your Inspiration from Taekwon-do?\n ANSWER: Your answer\n" +
            "\n" +
            "\nWhy do you do TKWDO?\n ANSWER: Your answer\n" +
            "\nName and explain all blocking & offensive moves up to your grade?\n See previous theory.\n",


    thrdtips="MAKE SURE YOU WARM UP BEFORE ENTERING DOJANG"+"\nBOW WHEN ENTERING DOJANG\n" +
            "\nLINE UP\n" +
            "\nATTENTION STANCE\n" +
            "\nBOW TO INSTRUCTOR & GRADING EXAMINEER\n" +
            "\nKNOW YOUR INSTRUCTORS NAME, GRADE\n" +
            "\nKNOW THE GRADING EXAMINEERS NAME, GRADE\n" +
            "\nIDENTIFY YOURSELF\n" +
            "\n20 X PRESS UP'S, 20 X SIT UP'S, 30 x STAR JUMPS, \n" +
            "\n10 X RISING KICKS (BOTH LEGS)\n" +
            "\n10 X SITTING STANCE PUNCHES BOTH SIDES\n" +
            "\n10 x SITTING STANCE DOUNBLE PUNCH\n" +
            "\n4 x FORWARDS & 4 X BACKWARDS MIDDLE INNER FOREARM BLOCK, REVERSE MIDDLE PUNCH WALKING STANCE\n" +
            "\n4 X FORWARDS & 4 X BACKWARDS RISING BLOCKS\n" +
            "\n4 X FORWARDS & 4 X BACKWARDS MIDDLE BLOCK\n" +
            "\n4 X FORWARDS WALKING STANCE MIDDLE OBVERSE PUNCH\n" +
            "\n4 X FORWARDS WALKING STANCE MIDDLE BLOCK + MIDDLE REVERSE PUNCH\n" +
            "\nGUARDING STANCE FRONT SNAP KICK DOUBLE PUNCH\n" +
            "\n4 X FORWARDS WALKING STANCE LOW BLOCK THEN RISING BLOCK\n" +
            "\nAS ABOVE ALL BLOCKS UP TO GRADE\n" +
            "\nPATTERN  TOI-GYE x 2\n" +
            "\nALL PREVIOUS GRADE PATTERNS\n" +
            "\nSAJO JURIGI NO 1, SAJO JURIGI NO 2, SAJO MAKI\n" +
            "\nL-STANCE TWIN FOREARM BLOCK\n" +
            "\nTURNING KICK, SIDE KICK, REVERSE TURNING KICK, BACK KICK POSSIBLE A COMBINATION OF 4 KICKS\n" +
            "\n3-STEP SPARRING + 2-STEP SPARRING + 2 STEP SPARRING EXTRA + 1-STEP SPARRING\n" +
            "\n1-STEP SEMI FREE SPARRING (3 STEP ATTACKS - YOUR DEFENSE OR MOVEMENT OF LINE AND SINGLE COUNTER) ENSURE COUNTER AIMS AT DISABLING THE OPPONENT\n" +
            "\nKNIFEHAND GUARDING BLOCK IN L-STANCE, SITTING STANCE DOUBLE PUNCH\n" +
            "\nFREE SPARRING, DEMONSTRATION OF 2 X KICK TO PAD, DEMONSTRATION OF 2 X STRIKES TO PAD\n" +
            "\nPattern  Students Choice excluding Toi-Gye\n" +
            "\nPattern  Set by the Grading Examiner\n" +
            "\nBE PREPARED FOR QUESTIONS DURING GRADING\n" +
            "\n" +
            "THINGS TO FOCUS ON â€“ ENSURE NEW TECHNIQUES ARE PERFORMED CORRECTLY, W SHAPE BLOCK, DO NOT LEAN FORWARD ON CIRRCULAR BLOCK, ENSURE YOU HAVE CORRECT TARGET AREAS FOR ALL ATTACKS, EXECUTION OF X STANCE, HIGH DOUBLE FOREARM BLOCKS, CORRECT HAND POSITION FOR HOOKING BLOCK, POWER, TIMING, CONCENTRATION, STRAIGHT FINGER TIP THRUST IN CORRECT POSITION, MAINTAIN GOOD CIRCULAR BLOCK, BENDING READY STANCE, TWIN VERTICAL PUNCH (2-STEP SPARRING), ACCURACY WHEN SPARRING AND 1 STEP SPARRING COUNTER ATTACKS MUST BE TARGETING A POINT WHICH COULD DISABLE YOUR ATTACKER (NO CONTACT)\n" +
            "\n" +
            "\nNew:\n" +
            "\nFLYING : TWIMYO\n" +
            "\nW-SHAPE BLACK : SAN MAKGI\n" +
            "\nLOW DOUBLE FOREARM PUSHING BLOCK : NAJUNDE DOO PALMOK MAKGI\n" +
            "\nGRASPING : JAPKI\n",

    thrdtheory= "Yi Hwang 1501-1570, was one of" +
            " the most prominent Korean Confucian Scholars of the Josean Dynasty the " +
            "other being his younger contemporary Yi I (Yul-Gok) However, Yi Hwang was " +
            "a key figure of the Neo Confucian literati, he established the Yeongnam School and " +
            "set up the Dosan Seowon a private Confucian academy. Yi Hwang is often referred to by " +
            "his penname Toi-Gye (Retreating Creek).\n" +
            "\n" +
            "He was a child prodigy, learned the Anaclets of Confucius from his uncle at" +
            " 12 and started writing poetry soon after. Around the age of 20 he immersed " +
            "himself in the study of Neo Confucianism. He passed his preliminary exams and " +
            "became a government official. He also passed the civil service exams with top " +
            "honours in 1534 and continued his scholarly pursuits whilst working for the government. " +
            "He went on to take up many government positions but became disillusioned by the power " +
            "struggles and left principle office. However, he was continuously bought back into various " +
            "government positions, he was appointed to work at the Royal court, became governor of Danyang & Punggi.\n" +
            "\n" +
            "He was the author of many books, he was a talented in" +
            " calligraphy, poetry, and published 316 works in 467 publications and in " +
            "7 languages. Yi Hwang is depicted on the current South Korean 1000 won note.\n",
            a,b,c,d;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.typgradpage);
//        findid();

        imageButton=findViewById(R.id.detail_id);
      //  textButton=findViewById(R.id.star_id);
        intro =findViewById(R.id.intro);
        quiz=findViewById(R.id.quickquiz);
        tips =findViewById(R.id.quicktip);
        theory=findViewById(R.id.advancedtheory);

        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        intro.setTypeface(tf);
        quiz.setTypeface(tf);
        tips.setTypeface(tf);
        theory.setTypeface(tf);

//        String fontPath = "ArialBold.ttf";
//        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
////        // Applying font
//        intro.setTypeface(tf);
//        quiz.setTypeface(tf);
//        tips.setTypeface(tf);
//        theory.setTypeface(tf);

        Intent intent=getIntent();
        int val =intent.getIntExtra("val",0);
//        if (val==0){
//            //10th kup
//
////            a=fstintro;
////            b=fstquiz;
////            c=fsttips;
////            d=fsttheory;
//            a=teni;
//            b=tenq;
//            c=tent;
//            d=tentheory;
//
//        }
        if (val==1){

            //9th kup
            a=ninei;
            b=nineq;
            c=ninet;
            d=ninetheory;

        }
        if (val==2){
            a=eightintro;
            b=eightquiz;
            c=eighttips;
            d=eighttheory;

        }
        if (val==3){
            a=sevnintro;
            b=sevnquiz;
            c=sevntips;
            d=sevntheory;

        }
        if (val==4){
            a=sixintro;
            b=sixquiz;
            c=sixtips;
            d=sixtheory;

        }
        if (val==5){
            a=fiveintro;
            b=fivequiz;
            c=fivetips;
            d=fivetheory;

        }
        if (val==6){
            a=fourthkuptext;
            b=fourquizz;
            c=fourtipss;
            d=fourtheoryy;

        }
        if (val==7){
            a=thrdintro;
            b=thrdquiz;
            c=thrdtips;
            d=thrdtheory;

        }
        if (val==8){
            a=sndintro;
            b=sndquiz;
            c=sndtips;
            d=sndtheoy;
        }
        if (val==9){
            a=fstintro;
            b=fstquiz;
            c=fsttips;
            d=fsttheory;

        }
//        if (val==10){
//            a=fstintro;
//            b=fstquiz;
//            c=fsttips;
//            d=fsttheory;
//
//        }



        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                procedure_dialog(fourthkupdetail);

            }
        });

//        textButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                procedure_dialog(fourthkuptext);
//
//            }
//        });

        intro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(Typegradpage.this,DataPresentation.class);
                intent.putExtra("value", a);

                startActivity(intent);
            }
        });

        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(Typegradpage.this,DataPresentation.class);
                intent.putExtra("value", b);

                startActivity(intent);
            }
        });

        tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(Typegradpage.this,DataPresentation.class);
                intent.putExtra("value", c);

                startActivity(intent);
            }
        });

        theory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(Typegradpage.this,DataPresentation.class);
                intent.putExtra("value", d);

                startActivity(intent);
            }
        });



    }

    private void findid() {


        imageButton=findViewById(R.id.detail_id);
       // textButton=findViewById(R.id.star_id);
        intro =findViewById(R.id.intro);
        quiz=findViewById(R.id.quickquiz);
        tips =findViewById(R.id.quicktip);
        theory=findViewById(R.id.advancedtheory);

        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        intro.setTypeface(tf);
        quiz.setTypeface(tf);
        tips.setTypeface(tf);
        theory.setTypeface(tf);
    }

    public void procedure_dialog(String a){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.datapresentation);
        final TextView datatext=dialog.findViewById(R.id.data_text);
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        datatext.setTypeface(tf);
        datatext.setText(a);
        dialog.show();

    }
}
