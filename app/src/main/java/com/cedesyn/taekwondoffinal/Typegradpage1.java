package com.cedesyn.taekwondoffinal;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Typegradpage1 extends AppCompatActivity {
    Button quiz,tips;
    String val="0";
    ImageView detail;
    String teni="PLEASE NOTE THIS INFORMATION HAS BEEN PUBLISHED FOR ITF\n" +
            "STUDENTS, HOWEVER WTF STUDENTS WILL GAIN BENEFIT FROM REVISING\n" +
            "WITH THIS INFO.\n" +
            "THIS APP HAS ALSO BEEN PRODUCED TO AID TAEKWON-DO STUDENTS\n" +
            "WITH THEORY. IT IS ALSO IMPORTANT TO UNDERSTAND THAT DIFFERENT\n" +
            "ORGANISATIONS MAY ASK ADDITIONAL QUESTIONS OR MAY REQUESTS OR\n" +
            "PROVIDE YOU WITH DIFFERENT QUESTIONS.\n" +
            "PLEASE CONSIDER THIS INFORMATION HAS BEEN PROVIDED FROM THE\n" +
            "EXPERIENCE OF COMPLETING A 1 ST DEGREE BLACK BELT WITHIN AN\n" +
            "ESTABLISHED ITF ORGANISATION";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.typrgradpage1);

        quiz=(Button)findViewById(R.id.quickquiz1);
        tips=(Button)findViewById(R.id.quicktip1);
        detail=(ImageView)findViewById(R.id.detail_id);

        detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                procedure_dialog(teni);
            }
        });

        quiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
val="0";
                Intent intent =new Intent(Typegradpage1.this,DataPresentation1.class);
                intent.putExtra("val",val);
                startActivity(intent);
            }
        });

        tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val="1";
                Intent intent =new Intent(Typegradpage1.this,DataPresentation1.class);
                intent.putExtra("val",val);
                startActivity(intent);
            }
        });

    }
    public void procedure_dialog(String a){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.datapresentation3);
        final TextView datatext=dialog.findViewById(R.id.data_text1);
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        datatext.setTypeface(tf);
        datatext.setText(a);
        dialog.show();

    }
}
