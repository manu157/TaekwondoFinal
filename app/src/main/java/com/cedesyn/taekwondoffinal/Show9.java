package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show9 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar9;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show9); snackbar9=(ConstraintLayout)findViewById(R.id.snacks9);

        btn=(Button)findViewById(R.id.ans9);


        ques=(TextView)findViewById(R.id.qstn9);
        ans1=(TextView) findViewById(R.id.o19);
        ans2=(TextView) findViewById(R.id.o29);
        ans3=(TextView) findViewById(R.id.o39);
        ans4=(TextView) findViewById(R.id.o49);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("BLACK SIGNIFIES ?");
        ans1.setText("HONOUR & RESPECT");
        ans2.setText("PEACE & WAR");
        ans3.setText("WAR & RESPECT");
        ans4.setText("MATURITY AND PROFICIENCY");



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "DANGER ", Snackbar.LENGTH_LONG);
                    snackBar9.show();
                }

                if(count==2){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "MATURITY AND PROFICIENCY", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==3){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "30", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==4){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Admiral Yi Sun Sin", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==5){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "The first armoured battleship", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==6){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Submarine", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==7){
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "I", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==8) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "The King", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }
                if(count==9) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "To avoid a sweep", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }
                if(count==10) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Kyocha Sonkal Momchau Makgi", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }if(count==11) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, ", Black is the opposite of white, therefore signifying maturity and proficiency in Taekwon-Do", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }if(count==12) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "60/40", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }if(count==13) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "1 shoulder width between big toes", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }
                if(count==14) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Mori", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }
                if(count==15) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Taekwon-do", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }
                if(count==16) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Your instructor and seniors", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==17) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "True ", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==18) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "True ", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==18) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "2", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==19) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "He was struck by a stray bullet which entered his left armpit", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                } if(count==20) {
                    Snackbar snackBar9 = Snackbar.make(snackbar9, "Turtle Ship", Snackbar.LENGTH_LONG);
                    snackBar9.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){   img1.setVisibility(View.GONE);
                    ques.setText("BLACK SIGNIFIES ?");
                    ans1.setText("HONOUR & RESPECT");
                    ans2.setText("PEACE & WAR");
                    ans3.setText("WAR & RESPECT");
                    ans4.setText("MATURITY AND PROFICIENCY");

                } if(count==2){
                    ques.setText("How many moves in pattern Choong-Moo ?");
                    ans1.setText("28");
                    ans2.setText("26");
                    ans3.setText("30");
                    ans4.setText("29");

                } if(count==3){
                    ques.setText("Choong-Moo was the name given to which great Admiral?");
                    ans1.setText("Admiral Yi Sun Sin");
                    ans2.setText("Admiral Yi Sun Din");
                    ans3.setText("Admiral Yi To Me");
                    ans4.setText("Admiral Yi Sun Twin");

                } if(count==4){
                    ques.setText("Admiral Yi Sun Sin was reputed to have invented what?");
                    ans1.setText("The first armoured battleship");
                    ans2.setText("The first navy seals");
                    ans3.setText("The first Special forces team");
                    ans4.setText("The Submarine");

                } if(count==5){
                    ques.setText("The Kobukson was referred to as what?");
                    ans1.setText("Motorbike");
                    ans2.setText("Ship");
                    ans3.setText("Submarine");
                    ans4.setText("Train");


                } if(count==6){
                    ques.setText("What is the pattern DIAGRAM for Choong-Moo?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("U");
                    ans4.setText("I");

                } if(count==7){
                    ques.setText("Admiral Yi Sun Sin had forced reservation of loyalty to who?");
                    ans1.setText("His Dog");
                    ans2.setText("His Mother");
                    ans3.setText("His Father");
                    ans4.setText("The King");

                } if(count==8) {
                    ques.setText("What is the purpose of the 360 jump and spin into Knifehand guarding block in Choong-Moo?");
                    ans1.setText("To practice jumping");
                    ans2.setText("To learn how to spin in the air");
                    ans3.setText("To avoid a sweep");
                    ans4.setText("To Learn control");

                }if(count==9) {
                    ques.setText("What is the Korean for X-knifehand checking block?");
                    ans1.setText("Kyocha Sonkal Momchau Makgi");
                    ans2.setText("Kyocha Nollyo");
                    ans3.setText("Kyocha Joomk");
                    ans4.setText("Nollyo Makgi");

                }if(count==10) {
                    ques.setText("What is meant by impervious to darkness & fear?");
                    ans1.setText("You are ready for your black belt");
                    ans2.setText("You are Mature");
                    ans3.setText("It means you are proficient in Taekwon-do");
                    ans4.setText("Black is the opposite of white");

                }if(count==11) {
                    ques.setText("What is the body weight for vertical stance?");
                    ans1.setText("90/10");
                    ans2.setText("70/30");
                    ans3.setText("50/50");
                    ans4.setText("60/40");

                }if(count==12) {
                    ques.setText("What is the length of vertical stance?");
                    ans1.setText("1 shoulder width between ankles");
                    ans2.setText("1 shoulder width between big toes");
                    ans3.setText("1 shoulder width between balls of your feet");
                    ans4.setText("1 shoulder width between little toes");

                }if(count==13) {
                    ques.setText("What is the Korean for Head?");
                    ans1.setText("Mori");
                    ans2.setText("Yori");
                    ans3.setText("Nollyo");
                    ans4.setText("Pulmok");

                }if(count==14) {
                    ques.setText(" The ITF code of conduct recommends you should observe the tenets of what? ");
                    ans1.setText("Taekwon-do)");
                    ans2.setText("Swimming");
                    ans3.setText("Honour");
                    ans4.setText("War");

                }if(count==15) {
                    ques.setText("The ITF code of conduct recommends you respect who?");
                    ans1.setText("Your instructor and seniors");
                    ans2.setText("Other people");
                    ans3.setText("Instructor");
                    ans4.setText("Seniors");

                }if(count==16) {
                    ques.setText("The ITF code recommends you should never misuse Taekwon-do?");
                    ans1.setText("True");
                    ans2.setText("False");
//                    ans3.setText("Sir Dan Gun");
//                    ans4.setText("None of the above");

                }if(count==17) {
                    ques.setText("The ITF code recommends you should try and build a more peaceful world?");
                    ans1.setText("True");
                    ans2.setText("False");
//                    ans3.setText("Sir Dan Gun");
//                    ans4.setText("None of the above"); How many times was Admiral Yi Sun Sin demote to a foot soldier? 3, 4, 5, 2


                }if(count==18) {
                    ques.setText("How many times was Admiral Yi Sun Sin demote to a foot soldier?");
                    ans1.setText("3");
                    ans2.setText("4");
                    ans3.setText("5");
                    ans4.setText("2");

                }if(count==19) {
                    ques.setText("How did Admiral Yi Sun Sin lose his life?");
                    ans1.setText("He was struck by 3 bullets");
                    ans2.setText("He was struck by a cannon ball");
                    ans3.setText("He was struck by his niece with a sword");
                    ans4.setText("He was struck by a stray bullet which entered his left armpit");

                }if(count==20) {   img1.setVisibility(View.GONE);
                    ques.setText("The Kobukson was referred to as what?");
                    ans1.setText("Duck ship");
                    ans2.setText("Stealth Ship");
                    ans3.setText("Turtle Ship");
                    ans4.setText("Armed Ship");

                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){   img2.setVisibility(View.GONE);
                    ques.setText("BLACK SIGNIFIES ?");
                    ans1.setText("HONOUR & RESPECT");
                    ans2.setText("PEACE & WAR");
                    ans3.setText("WAR & RESPECT");
                    ans4.setText("MATURITY AND PROFICIENCY");

                } if(count==2){
                    ques.setText("How many moves in pattern Choong-Moo ?");
                    ans1.setText("28");
                    ans2.setText("26");
                    ans3.setText("30");
                    ans4.setText("29");

                } if(count==3){
                    ques.setText("Choong-Moo was the name given to which great Admiral?");
                    ans1.setText("Admiral Yi Sun Sin");
                    ans2.setText("Admiral Yi Sun Din");
                    ans3.setText("Admiral Yi To Me");
                    ans4.setText("Admiral Yi Sun Twin");

                } if(count==4){
                    ques.setText("Admiral Yi Sun Sin was reputed to have invented what?");
                    ans1.setText("The first armoured battleship");
                    ans2.setText("The first navy seals");
                    ans3.setText("The first Special forces team");
                    ans4.setText("The Submarine");

                } if(count==5){
                    ques.setText("The Kobukson was referred to as what?");
                    ans1.setText("Motorbike");
                    ans2.setText("Ship");
                    ans3.setText("Submarine");
                    ans4.setText("Train");


                } if(count==6){
                    ques.setText("What is the pattern DIAGRAM for Choong-Moo?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("U");
                    ans4.setText("I");

                } if(count==7){
                    ques.setText("Admiral Yi Sun Sin had forced reservation of loyalty to who?");
                    ans1.setText("His Dog");
                    ans2.setText("His Mother");
                    ans3.setText("His Father");
                    ans4.setText("The King");

                } if(count==8) {
                    ques.setText("What is the purpose of the 360 jump and spin into Knifehand guarding block in Choong-Moo?");
                    ans1.setText("To practice jumping");
                    ans2.setText("To learn how to spin in the air");
                    ans3.setText("To avoid a sweep");
                    ans4.setText("To Learn control");

                }if(count==9) {
                    ques.setText("What is the Korean for X-knifehand checking block?");
                    ans1.setText("Kyocha Sonkal Momchau Makgi");
                    ans2.setText("Kyocha Nollyo");
                    ans3.setText("Kyocha Joomk");
                    ans4.setText("Nollyo Makgi");

                }if(count==10) {
                    ques.setText("What is meant by impervious to darkness & fear?");
                    ans1.setText("You are ready for your black belt");
                    ans2.setText("You are Mature");
                    ans3.setText("It means you are proficient in Taekwon-do");
                    ans4.setText("Black is the opposite of white");

                }if(count==11) {
                    ques.setText("What is the body weight for vertical stance?");
                    ans1.setText("90/10");
                    ans2.setText("70/30");
                    ans3.setText("50/50");
                    ans4.setText("60/40");

                }if(count==12) {
                    ques.setText("What is the length of vertical stance?");
                    ans1.setText("1 shoulder width between ankles");
                    ans2.setText("1 shoulder width between big toes");
                    ans3.setText("1 shoulder width between balls of your feet");
                    ans4.setText("1 shoulder width between little toes");

                }if(count==13) {
                    ques.setText("What is the Korean for Head?");
                    ans1.setText("Mori");
                    ans2.setText("Yori");
                    ans3.setText("Nollyo");
                    ans4.setText("Pulmok");

                }if(count==14) {
                    ques.setText(" The ITF code of conduct recommends you should observe the tenets of what? ");
                    ans1.setText("Taekwon-do)");
                    ans2.setText("Swimming");
                    ans3.setText("Honour");
                    ans4.setText("War");

                }if(count==15) {
                    ques.setText("The ITF code of conduct recommends you respect who?");
                    ans1.setText("Your instructor and seniors");
                    ans2.setText("Other people");
                    ans3.setText("Instructor");
                    ans4.setText("Seniors");

                }if(count==16) {
                    ques.setText("The ITF code recommends you should never misuse Taekwon-do?");
                    ans1.setText("True");
                    ans2.setText("False");
//                    ans3.setText("Sir Dan Gun");
//                    ans4.setText("None of the above");

                }if(count==17) {
                    ques.setText("The ITF code recommends you should try and build a more peaceful world?");
                    ans1.setText("True");
                    ans2.setText("False");
//                    ans3.setText("Sir Dan Gun");
//                    ans4.setText("None of the above"); How many times was Admiral Yi Sun Sin demote to a foot soldier? 3, 4, 5, 2


                }if(count==18) {
                    ques.setText("How many times was Admiral Yi Sun Sin demote to a foot soldier?");
                    ans1.setText("3");
                    ans2.setText("4");
                    ans3.setText("5");
                    ans4.setText("2");

                }if(count==19) {
                    ques.setText("How did Admiral Yi Sun Sin lose his life?");
                    ans1.setText("He was struck by 3 bullets");
                    ans2.setText("He was struck by a cannon ball");
                    ans3.setText("He was struck by his niece with a sword");
                    ans4.setText("He was struck by a stray bullet which entered his left armpit");

                }if(count==20) {   img1.setVisibility(View.GONE);
                    ques.setText("The Kobukson was referred to as what?");
                    ans1.setText("Duck ship");
                    ans2.setText("Stealth Ship");
                    ans3.setText("Turtle Ship");
                    ans4.setText("Armed Ship");

                }
                if(count ==20){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }


            }
        });


    }
}
