package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show6 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar6;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show6);
        snackbar6=(ConstraintLayout)findViewById(R.id.snacks6);

        btn=(Button)findViewById(R.id.ans6);


        ques=(TextView)findViewById(R.id.qstn6);
        ans1=(TextView) findViewById(R.id.o16);
        ans2=(TextView) findViewById(R.id.o26);
        ans3=(TextView) findViewById(R.id.o36);
        ans4=(TextView) findViewById(R.id.o46);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("What does Blue signify? ");
        ans1.setText("SIGNIFIES THE HEAVENS");
        ans2.setText("BLUE SIGNIFIES EARTH");
        ans3.setText("BLUE SIGNIFIES DANGER");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "SIGNIFIES THE HEAVENS ", Snackbar.LENGTH_LONG);
                    snackBar6.show();
                }

                if(count==2){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "SIGNIFIES DANGER", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                } if(count==3){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "32", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                } if(count==4){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Ahn Joong-Gun", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                } if(count==5){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Mr Ahn’s age when he was executed in Lui-Shung prison in 1910", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                } if(count==6){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "I", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                } if(count==7){
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Scholar", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                } if(count==8) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Inspiration", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==9) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "4 directional punch", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==10) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "No", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }if(count==11) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "four directional block", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }if(count==12) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Turning Kick", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }if(count==13) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Not given", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==14) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Sonkal Dung", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==15) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "not given", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==16) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "B", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==17) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Dwit Bal Sogi", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }if(count==18) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Low Stance", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==19) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "A four time prime minister of Japan", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }
                if(count==20) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Marksman", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }if(count==21) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Japanese", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }if(count==22) {
                    Snackbar snackBar6 = Snackbar.make(snackbar6, "Hanged", Snackbar.LENGTH_LONG);
                    snackBar6.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){ img2.setVisibility(View.GONE);
                    ques.setText("What does Blue signify? ");
                    ans1.setText("SIGNIFIES THE HEAVENS");
                    ans2.setText("BLUE SIGNIFIES EARTH");
                    ans3.setText("BLUE SIGNIFIES DANGER");
                   // ans4.setText("21");

                } if(count==2){
                    ques.setText("What does Red signify?");
                    ans1.setText("SIGNIFIES DANGER");
                    ans2.setText("RED SIGNIFIES EARTH");
                    ans3.setText("SIGNIFIES EARTH");
                    ans4.setText("RED SIGNIFIES CAUTION");

                } if(count==3){
                    ques.setText("How many moves in pattern JOONG-GUN?");
                    ans1.setText("42");
                    ans2.setText("37");
                    ans3.setText("30");
                    ans4.setText("32");


                } if(count==4){

                    ques.setText("Who assassinated Hiro-Bumi ito?");
                    ans1.setText("YUL-GOK");
                    ans2.setText("DAN-GUN");
                    ans3.setText("Ahn Joong-Gun");
                    ans4.setText("Mr Jones");


                } if(count==5){
                    ques.setText("What does the 32 movements of pattern JOONG-GUN refer to?");
                    ans1.setText("Mr Ahn’s age when he was executed in Lui-Shung prison in 1910");
                    ans2.setText("Year of the pattern");
                    ans3.setText("When Taekwon-do matured");
                    ans4.setText("The events from 11th April 1955");


                } if(count==6){
                    ques.setText("What is the PATTERN DIAGRAM for JOONG-GUN?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("Y");
                    ans4.setText("I");

                } if(count==7){
                    ques.setText("What does the pattern diagram from JOONG-GUN represent?");
                    ans1.setText("Scholar");
                    ans2.setText("TEACHER");
                    ans3.setText("Writer");
                    ans4.setText("Activist");

                } if(count==8) {
                    ques.setText("What do we get from performing a pattern?");
                    ans1.setText("Inspiration");
                    ans2.setText("Time");
                    ans3.setText("History");
                    ans4.setText("Practice");

                }if(count==9) {
                    ques.setText("What is SAJO JURIGI in English?");
                    ans1.setText("4 directional punch");
                    ans2.setText("4 blocks");
                    ans3.setText("4 directional block");
                    ans4.setText("first pattern learnt");

                }if(count==10) {
                    ques.setText("Is SAJO JURIGI a pattern?");
                    ans1.setText("Yes");
                    ans2.setText("No");
//                    ans3.setText("Ap chagi");
//                    ans4.setText("Sang Sonkul Makgi");

                }if(count==11) {
                    ques.setText("What is SAJO MAKI in English?");
                    ans1.setText("Four directional punch");
                    ans2.setText("four directional strike");
                    ans3.setText("four directional block");
                    ans4.setText("four blocks");

                }if(count==12) {
                    ques.setText("What is Dollyo Chagi in English?");
                    ans1.setText("Turning Kick");
                    ans2.setText("Rising Kick");
                    ans3.setText("Back Kick");
                    ans4.setText("Reverse Turning Kick");

                }
                if(count==13) {
                    ques.setText("Knee section outer forearm block");
                    ans1.setText("Moorup");
                    ans2.setText("Bakat");
                    ans3.setText("Palmok");
                    ans4.setText("makgi");

                }if(count==14) {
                    ques.setText("What is Reverse Knifehand in Korean?");
                    ans1.setText("Sonkal Dung");
                    ans2.setText("Joong-Gun");
                    ans3.setText("Ap Joomuk");
                    ans4.setText("Yop Joomuk");

                }if(count==15) {
                    ques.setText("Wgat is Side Sole?");
                    ans1.setText("Yop Balbadak");
                    ans2.setText("Bandalson");
                    ans3.setText("Joomuk");
                    ans4.setText("Balkal");

                }if(count==16) {
                    ques.setText("In pattern Joong-Gun is the Closed Ready Stance?");
                    ans1.setText("A");
                    ans2.setText("B");
                    ans3.setText("C");
                    ans4.setText("D");

                }if(count==17) {
                    ques.setText("What is Rear Foot Stance in Korean?");
                    ans1.setText("Nachuo Sogi");
                    ans2.setText("Annun Sogi");
                    ans3.setText("Gunnun Sogi");
                    ans4.setText("Dwit Bal Sogi");
                }if(count==18) {
                    ques.setText("What is Nachuo Sogi in English?");
                    ans1.setText("High Stance");
                    ans2.setText("Low Stance");
                    ans3.setText("Sitting Stance");
                    ans4.setText("Walking Stance");
                }if(count==19) {
                    ques.setText("Ito Hirobumi was what?");
                    ans1.setText("A four time prime minister of Japan");
                    ans2.setText("A Monk");
                    ans3.setText(" A Train driver");
                    ans4.setText("None of the above");

                }
                if(count==20) {
                    ques.setText("Joong-Gun was a trained?");
                    ans1.setText("Chef");
                    ans2.setText("Taekwondoist");
                    ans3.setText("Marksman");
                    ans4.setText("Not in love");

                }
                if(count==21) {
                    ques.setText("Who imprisoned Joong-Gun for his actions?");
                    ans1.setText("French");
                    ans2.setText("Russians");
                    ans3.setText("Spanish");
                    ans4.setText("Japanese");

                }
                if(count==22) { img1.setVisibility(View.GONE);
                    ques.setText("How was Joong-Gun executed?");
                    ans1.setText("Shot");
                    ans2.setText("Shot by Firing Squad");
                    ans3.setText("Hanged");
                    ans4.setText("Whilst sleeping");

                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){ img2.setVisibility(View.GONE);
                    ques.setText("What does Blue signify? ");
                    ans1.setText("SIGNIFIES THE HEAVENS");
                    ans2.setText("BLUE SIGNIFIES EARTH");
                    ans3.setText("BLUE SIGNIFIES DANGER");
                    // ans4.setText("21");

                } if(count==2){
                    ques.setText("What does Red signify?");
                    ans1.setText("SIGNIFIES DANGER");
                    ans2.setText("RED SIGNIFIES EARTH");
                    ans3.setText("SIGNIFIES EARTH");
                    ans4.setText("RED SIGNIFIES CAUTION");

                } if(count==3){
                    ques.setText("How many moves in pattern JOONG-GUN?");
                    ans1.setText("42");
                    ans2.setText("37");
                    ans3.setText("30");
                    ans4.setText("32");


                } if(count==4){

                    ques.setText("Who assassinated Hiro-Bumi ito?");
                    ans1.setText("YUL-GOK");
                    ans2.setText("DAN-GUN");
                    ans3.setText("Ahn Joong-Gun");
                    ans4.setText("Mr Jones");


                } if(count==5){
                    ques.setText("What does the 32 movements of pattern JOONG-GUN refer to?");
                    ans1.setText("Mr Ahn’s age when he was executed in Lui-Shung prison in 1910");
                    ans2.setText("Year of the pattern");
                    ans3.setText("When Taekwon-do matured");
                    ans4.setText("The events from 11th April 1955");


                } if(count==6){
                    ques.setText("What is the PATTERN DIAGRAM for JOONG-GUN?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("Y");
                    ans4.setText("I");

                } if(count==7){
                    ques.setText("What does the pattern diagram from JOONG-GUN represent?");
                    ans1.setText("Scholar");
                    ans2.setText("TEACHER");
                    ans3.setText("Writer");
                    ans4.setText("Activist");

                } if(count==8) {
                    ques.setText("What do we get from performing a pattern?");
                    ans1.setText("Inspiration");
                    ans2.setText("Time");
                    ans3.setText("History");
                    ans4.setText("Practice");

                }if(count==9) {
                    ques.setText("What is SAJO JURIGI in English?");
                    ans1.setText("4 directional punch");
                    ans2.setText("4 blocks");
                    ans3.setText("4 directional block");
                    ans4.setText("first pattern learnt");

                }if(count==10) {
                    ques.setText("Is SAJO JURIGI a pattern?");
                    ans1.setText("Yes");
                    ans2.setText("No");
//                    ans3.setText("Ap chagi");
//                    ans4.setText("Sang Sonkul Makgi");

                }if(count==11) {
                    ques.setText("What is SAJO MAKI in English?");
                    ans1.setText("Four directional punch");
                    ans2.setText("four directional strike");
                    ans3.setText("four directional block");
                    ans4.setText("four blocks");

                }if(count==12) {
                    ques.setText("What is Dollyo Chagi in English?");
                    ans1.setText("Turning Kick");
                    ans2.setText("Rising Kick");
                    ans3.setText("Back Kick");
                    ans4.setText("Reverse Turning Kick");

                }
                if(count==13) {
                    ques.setText("Knee section outer forearm block");
                    ans1.setText("Moorup");
                    ans2.setText("Bakat");
                    ans3.setText("Palmok");
                    ans4.setText("makgi");

                }if(count==14) {
                    ques.setText("What is Reverse Knifehand in Korean?");
                    ans1.setText("Sonkal Dung");
                    ans2.setText("Joong-Gun");
                    ans3.setText("Ap Joomuk");
                    ans4.setText("Yop Joomuk");

                }if(count==15) {
                    ques.setText("Wgat is Side Sole?");
                    ans1.setText("Yop Balbadak");
                    ans2.setText("Bandalson");
                    ans3.setText("Joomuk");
                    ans4.setText("Balkal");

                }if(count==16) {
                    ques.setText("In pattern Joong-Gun is the Closed Ready Stance?");
                    ans1.setText("A");
                    ans2.setText("B");
                    ans3.setText("C");
                    ans4.setText("D");

                }if(count==17) {
                    ques.setText("What is Rear Foot Stance in Korean?");
                    ans1.setText("Nachuo Sogi");
                    ans2.setText("Annun Sogi");
                    ans3.setText("Gunnun Sogi");
                    ans4.setText("Dwit Bal Sogi");
                }if(count==18) {
                    ques.setText("What is Nachuo Sogi in English?");
                    ans1.setText("High Stance");
                    ans2.setText("Low Stance");
                    ans3.setText("Sitting Stance");
                    ans4.setText("Walking Stance");
                }if(count==19) {
                    ques.setText("Ito Hirobumi was what?");
                    ans1.setText("A four time prime minister of Japan");
                    ans2.setText("A Monk");
                    ans3.setText(" A Train driver");
                    ans4.setText("None of the above");

                }
                if(count==20) {
                    ques.setText("Joong-Gun was a trained?");
                    ans1.setText("Chef");
                    ans2.setText("Taekwondoist");
                    ans3.setText("Marksman");
                    ans4.setText("Not in love");

                }
                if(count==21) {
                    ques.setText("Who imprisoned Joong-Gun for his actions?");
                    ans1.setText("French");
                    ans2.setText("Russians");
                    ans3.setText("Spanish");
                    ans4.setText("Japanese");

                }
                if(count==22) {img1.setVisibility(View.GONE);
                    ques.setText("How was Joong-Gun executed?");
                    ans1.setText("Shot");
                    ans2.setText("Shot by Firing Squad");
                    ans3.setText("Hanged");
                    ans4.setText("Whilst sleeping");

                }
                if(count ==22){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }

            }
        });


    }
}
