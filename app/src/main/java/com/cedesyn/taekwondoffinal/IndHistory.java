package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by jerin android on 07-03-2018.
 */

public class IndHistory extends AppCompatActivity {
    public String parent,child;
    String wh1,yes1,yeb1,ges1,geb1,bls1,blb1,res1,reb1,bs1,bdb1,wh11,yes11,yeb11,ges11,geb11,bls11,blb11,res11,reb11,
            bs11,bdb11;
    TextView iqshow,iqshowsub;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.individualquiz);
        iqshowsub=(TextView)findViewById(R.id.inquiowsub);
        iqshow =(TextView)findViewById(R.id.inqui) ;

        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        iqshow.setTypeface(tf);
        iqshowsub.setTypeface(tf);

        //10    white
        wh11="white belt means Signifies innocence, as that of the beginning student who has no previous knowledge of Taekwon-Do.\n";

        //9 yellowstripe
        yes11="Chon Ji is the foundation of all the 24 patterns. The stances and various movements once mastered form the basis of " +
                "all the other patterns. The meaning of Chon-Ji relates to the eastern philosophy of duality: Heaven & Earth," +
                " Dark & Light, Hot & Cold, Fire & Water. This is represented in the Korean Flag. The first part of this pattern " +
                "is said to represent the Earth whereas the second part is said to signify the Heavens. Referred to as " +
                "“Heavenly Lake” which is a real lake situated on the border between present day North Korea and China. " +
                "The lake is found in a crater on the top of Paektu mountain (also spelt Baekdu) and is said to be the birth " +
                "place of the Holy Dan Gun. (refer to the next pattern in the Chang-Hon series – Dan – Gun). ";

        //8 yellow belt
        yeb11="Dangun Wanggeom was the legendary founder of GOJOSEON the first ever KOREAN Kingdom. He is said to be the \"Grandson of Heaven\" and \"Son of a Bear\" and to have founded the kingdom in 2333 BC. (Before Christ). The earliest recorded version of the Dangun legend appears in the 13th-century SAMGUK YUSA, which cites China's Book of Wei and Korea's lost historical record Gogi. \n" +
                "Dangun's ancestry legend begins with his grandfather Hwanin the \"Lord of Heaven\". Hwanin had a son, Hwanung who yearned to live on the earth among the valleys and the mountains. Hwanin permitted Hwanung and 3,000 followers to descend onto Baekdu Mountain, where Hwanung founded the Sinsi \"City of God\"). Along with his ministers of clouds, rain and wind, he instituted laws and moral codes and taught humans various arts, medicine, and agriculture. \n" +
                "A tiger and a bear prayed to Hwanung that they might become human. Upon hearing their prayers, Hwanung gave them 20 cloves of garlic and a bundle of mugwort, ordering them to eat only this sacred food and remain out of the sunlight for 100 days. The tiger gave up after about twenty days and left the cave. However, the bear persevered and was transformed into a woman. The bear and the tiger are said to represent two tribes that sought the favor of the heavenly prince.\n" +
                "The bear-woman Ungnyeo was grateful and made offerings to Hwanung. However, she lacked a husband, and soon became sad and prayed beneath a \"divine birch\" tree to be blessed with a child. Hwanung, moved by her prayers, took her for his wife and soon she gave birth to a son named Dangun Wanggeom. \n";

        //7 green stripe
        ges11="Ahn Chang ho, sometimes An Chang-ho was a Korean independence activist and one of the early leaders " +
                "of the Korean-American immigrant community in the United States. He is also referred to as his pen name Do san. He established the Shinminhoe when he returned to Korea from the US in 1907. It was the most important organisation to fight the Japanese occupation of Korea. He established the Young Korean Academy in San Francisco in 1913 and was a key member in the founding of the Provisional Government of the Republic of Korea in Shanghai in 1919. An is one of two men believed to have written the lyrics of the Aegukga. (The South Korean national anthem). Besides his work for the Independence movement, Do San wanted to reform the Korean people's character and the entire social system of Korea. Educational reform and modernising " +
                "schools were two key efforts of Do San. He was the father of actor Philip Ahn and U.S. Navy officer Susan Ahn Cuddy.";

        //6 green belt
        geb11="WON-HYO WAS REGARDED AS ONE OF THE GREATEST THINKERS IN KAREAN BUDDISM. HE WAS ALSO A WRITER WHO PRODUCED EIGHTY-SIX WORKS, MANY ARE STILL IN USE TODAY. HE WAS A SCHOLAR AND AN INFLUENTIAL FIGURE IN THE DEVELOPMENT OF THE EAST ASIAN BUDDIST INTELLECTUAL TRADITIONS. HE WAS ALSO FAMOUS FOR BRINGING BUDDISM TO THE GENERAL PUBLIC BY WAY OF SINGING AND DANCING IN THE STREETS. HE HAD A SON CALLED SOEL CHONG WHO BECAME ONE OF THE GREAT CONFICIAN SCHOLARS OF SILLA.\n" +
                "\n" +
                "READY STANCE – OFTEN REFERRED TO AS REPRESENTING OPPOSITES (KOREAN FLAG) HOT/COLD, HARD/SOFT, FIRE/WATER, " +
                "BUT THE COMMON ANSWER IS WAR/PEACE. (CLENCHED RIGHT FIST FOR WAR & SOFT LEFT HAND OVER FIST FOR PEACE. SHOULD BE HELD " +
                "30CM FROM YOUR PHILTRUM. THERE IS ALSO ANOTHER THEORY WHICH REFERS TO BUDDIST CHANTING WITH A WOODEN PERCUSSION INSTRUMENT " +
                "CALLED A MOKTAK. CLOSED READY STANCE A,B & C ARE REFERRED TO AS THE DIFFERENT POSITIONS BUDDIST WOULD " +
                "HOLD THE MOKTAK WHEN CHANTING OR RECITING. ANOTHER THEORY SUGGEST THE STANCES CONVEY A SENSE OF CALM, HUMILITY & MODESTY.\n";

    // 5  blue stripe
        bls11="YUL-GUK WAS REGARDED AS ONE OF THE GREATEST SCHOLARS. HE WAS NICKNAMED THE CONFUSIUS OF KOREA." +
                " HE WROTE APPROX 280 PUBLICATIONS AND WAS ALSO A GOVERNMENT OFFICIAL. HE WAS A GREAT CONFUSIAN SCHOLAR. " +
                "BORN IN GANGWON PROVINCE, BY THE AGE OF 7 HE HAD COMPLETED HIS LESSONS ON CONFUCIAN CLASSICS. " +
                "PASSED THE CIVIL SERVICE LITERARY EXAM BEFORE HE WAS 13, MARRIED AT 22, " +
                "STUDIED CONFUCIANISM AND BEGAN WRITING WITH MUCH ACCLAIM." +
                " HE TOOK UP MANY MINISTERIAL POLITICAL POSITIONS IN KOREAN GOVERNMENT BUT LEFT OFFICE IN 1583 AND DIED " +
                "THE FOLLOWING YEAR.";

       //4  Blue belt
        blb11="JOONG-GUN ASSASSINATED Ito Hirobumi, a four time prime minister of Japan and former resident general of Korea." +
                " He was an independence activist and was a trained marksman. On 26th October 1909 he assassinated Hiro Bumo Ito " +
                "for reasons such because the Japanese were forcing the use of Japanese bank notes in Korea, Disbanding Korean army," +
                "Plundering Korean railroads, mines & forest, massacring innocent Koreans, dethroning the emperor Gojong. Joong-Gun " +
                "used a gun and shot Ito 3 times at Harbin Railway Station hiding a gun in his lunch box, he was arrested by Russian " +
                "guards, handed over to the Japanese and imprisoned for his actions. Executed whilst in prison, Joong-Gun requested his " +
                "execution be by a prisoner of war (firing squad as honour) however, he was refused this and was hanged, " +
                "this was on the basis the Japanese viewed him as a common criminal.\n";

       //3  red stripe
        res11="Yi Hwang 1501-1570, was one of the most prominent Korean Confucian Scholars of the Josean Dynasty " +
                "the other being his younger contemporary Yi I (Yul-Gok) However, Yi Hwang was a key figure of " +
                "the Neo Confucian literati, he established the Yeongnam School and set up the Dosan Seowon a" +
                " private Confucian academy. Yi Hwang is often referred to by his penname Toi-Gye (Retreating Creek).\n" +
                "\n" +
                "He was a child prodigy, learned the Anaclets of Confucius from his uncle at 12 and started " +
                "writing poetry soon after. Around the age of 20 he immersed himself in the study of Neo Confucianism. " +
                "He passed his preliminary exams and became a government official. He also passed the civil service exams " +
                "with top honours in 1534 and continued his scholarly pursuits whilst working for the government. " +
                "He went on to take up many government positions but became disillusioned by the power struggles and left" +
                " principle office. However, he was continuously bought back into various government positions, h" +
                "e was appointed to work at the Royal court, became governor of Danyang & Punggi.\n" +
                "\n" +
                "He was the author of many books, he was a talented in calligraphy, poetry, and published 316 works in" +
                "467 publications and in 7 languages. Yi Hwang is depicted on the current South Korean 1000 won note.\n";

       //2  red belt
        reb11="Elite group of Loyalist to the king. 3 kingdoms were Silla , Goguryeo & Baekje. Known as “The Flower boys” " +
                "they were educational institutions in the 10th Century, members gathered for all aspects of study, arts," +
                " culture & stemming mainly from Buddhism. Chinese sources referred to the physical beauty of the Flower boys." +
                " Few Koreans are said  to have known  " +
                "the Hwa Rang until after the liberation of 1945, after which the Hwa Rang were elevated to a symbolic importance.";

       //1  black stripe
        bs11="Yi Sun-sin who saved Choson Korea from the brink of collapse during the Japanese invasion of 1592.  He is still dearly cherished in the hearts of Koreans today. In a nationwide survey conducted in April 2005, Yi Sun-sin was chosen as the greatest figure in Korean history. \n" +
                "\n" +
                "Admiral Yi achieved a battle record that no one in history has ever \n" +
                "matched. The 23 battles that he fought at sea, Admiral Yi was never once defeated. Overcoming formidable odds in terms of numbers of ships and troops, he led his navy to victory in every engagement he fought during seven \n" +
                "years of war with the Japanese, losing in total only two ships of his own.\n" +
                "\n" +
                "His whole career might be summarized by saying that, although he had no lessons from past history to serve as a guide, he waged war on the sea as it should be waged if it is to produce definite results, and ended by making the supreme sacrifice of a defender of his country. \n" +
                "\n" +
                "The following is an extract from a paper published by the Japanese Institute of Korean Studies. \n" +
                "\n" +
                "If there ever were an Admiral worthy of the name of ‘god of war’, that one is Yi Sun-sin. \n" +
                "\n" +
                "Admiral Yi, in contrast, has been held as an object of admiration and reverence even among the Japanese, whose minds were swayed by his pure and absolute loyalty to his country and people, his brilliant use of strategy and tactics which led invariably to victory, his invincible courage that overcame every adverse circumstance, and his unbending integrity. This \n" +
                "admiration is apparent in the many speeches and writings by Japanese military officers and historians which speak of Admiral Yi.\n" +
                "\n" +
                "Yi was a supreme naval commander even on the basis of the limited literature of the Seven Years War, and despite the fact that his bravery and brilliance are not known to the West, since he had the misfortune to be born in Choson. \n" +
                "\n" +
                "Anyone who can be compared to Yi should be better than Michiel de Ruyter from Netherlands. Nelson is far behind Yi in terms of personal character and \n" +
                "integrity. Yi was the inventor of the iron-clad warship known as the Turtle Ship (Geobukseon). He was a truly great commander and a master of the naval tactics of three hundred years ago.\n" +
                "\n" +
                "Sato Destaro (1866-1942), a vice-admiral of the Japanese Navy,\n" +
                "A Military History of the Emperor, said Yi Sun-sin the Korean general who defeated the Japanese in every one of the battles at sea when \n" +
                "Toyotomi Hideyoshi’s troops invaded Choson Korea. He was unique among Choson civil and military officers for his honesty and incorruptibility, and in terms of leadership and tactics, as well as loyalty and courage, he was an \n" +
                "ideal commander almost like a miracle. He was a renowned admiral before the time of Nelson, and has never yet had an equal in world history. Although the existence of this figure grew to be almost forgotten in Korea, the admiration of his memory was handed down in Japan through generations so that his tactics and accomplishments were researched and subjected to close study when the Japanese Navy was established during the Meiji period.\n" +
                "\n" +
                "Yi is often compared with Admiral Nelson and Admiral Togo. All three men were heroes who fought for the destiny of their countries and saved their countrymen from foreign invasion by the securing of key naval victories. \n" +
                "However, the circumstances of Nelson’s Battle at Trafalgar and of Togo’s Battle at Tsushima differ strikingly from those of the Battle of Myongnyang fought by Admiral Yi.\n" +
                "\n" +
                "At the Battle of Trafalgar, England, a nation traditionally strong on the sea, was facing an enemy who was at that time inexperienced in naval warfare, and who commanded a fleet not much larger than her own (27 English ships against 33 French and Spanish ships). In the case of the Battle of Tsushima, also, the Japanese navy had the upper hand in many respects. The Russian crews of the Baltic fleet which opposed them were exhausted after a seven-month voyage which had taken them halfway round the world; the Arctic-born Russian crews had suffered greatly from outbreaks of disease as they sailed through the equator area. Taking this into account, it is of little surprise that an intensively trained Japanese Navy, in high morale and fighting near the mainland of Japan, emerged victorious over the dispirited Russian forces.\n" +
                "\n" +
                "He was demoted on 2 occasions to a foot soldier, this is what is referred to as his forced reservation of his loyalty to the king. As he was demoted then re-instated.\n" +
                "\n" +
                "On 15th December 1598 during a battle with the Japanese at Sachon bay Yi was struck by a stray bullet which entered his left armpit. The bullet was fatal and Yi died moments later.\n" +
                "\n" +
                "Look into the Kobukson (Turtle Ship)";

      //1st degree black belt
        bdb11="Black Dan belt";



        wh1="white belt";
        yes1="CHON JI (Yellow Stripe) 9'th Kup ";
        yeb1="DAN GUN (Yellow Belt) 8'th Kup ";
        ges1="DO SAN (Green Stripe) 7'th Kup ";
        geb1="WON HYO(Green belt) 6'th kup";
        bls1="YUL GOK(Blue Stripe) 5'th kup ";
        blb1="JOONG GUN(Blue Belt) 4'th kup";
        res1="TOI GYE(Red Stripe) 3'rd kup";
        reb1="HWA RANG(Red Belt) 2'nd kup";
        bs1="CHOONG MOO(Black Stripe) 1'st kup ";
        bdb1="Black Dan belt";
        Intent ii = getIntent();
        parent= ii.getStringExtra("parentpos");
//    child= ii.getStringExtra("chldpos");
//        Toast.makeText(this, ""+child, Toast.LENGTH_SHORT).show();
//        Toast.makeText(this, ""+parent, Toast.LENGTH_SHORT).show();
        SharedPreferences sp = getDefaultSharedPreferences(getApplicationContext());
        final String parp=sp.getString("parentpos","");
        final String childp=sp.getString("childpos","");


        switch (parp){

            case "0":
                iqshow.setText(wh1);
                iqshowsub.setText(wh11);
                break;
            case "1":
                iqshow.setText(yes1);
                iqshowsub.setText(yes11);
                break;
            case "2":
                iqshow.setText(yeb1);
                iqshowsub.setText(yeb11);
                break;
            case "3":
                iqshow.setText(ges1);
                iqshowsub.setText(ges11);
                break;
            case "4":
                iqshow.setText(geb1);
                iqshowsub.setText(geb11);
                break;
            case "5":
                iqshow.setText(bls1);
                iqshowsub.setText(bls11);
                break;
            case "6":
                iqshow.setText(blb1);
                iqshowsub.setText(blb11);
                break;
            case "7":
                iqshow.setText(res1);
                iqshowsub.setText(res11);
                break;
            case "8":
                iqshow.setText(reb1);
                iqshowsub.setText(reb11);
                break;
            case "9":
                iqshow.setText(bs1);
                iqshowsub.setText(bs11);
                break;
            case "10":
                iqshow.setText(bdb1);
                iqshowsub.setText(bdb11);
                break;



        }

    }}
