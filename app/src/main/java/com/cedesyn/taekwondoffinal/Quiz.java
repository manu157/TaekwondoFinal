package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;

/**
 * Created by jerin android on 02-02-2018.
 */

public class Quiz extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz);

        ExpandableListView custom =(ExpandableListView)findViewById(R.id.cusexp);

        final ArrayList<Team>team =getData();


        //create and bind with adapter
        CustomQuizAdapter adapter =new CustomQuizAdapter(Quiz.this,team);
        custom .setAdapter(adapter);


        custom.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
    @Override
    public boolean onChildClick(ExpandableListView parent, View view, int groupposi, int childposi, long id) {
      //  Toast.makeText(getApplicationContext(), (CharSequence) team.get(groupposi), Toast.LENGTH_SHORT).show();
        return false;
    }
});

        custom.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
    @Override
    public boolean onGroupClick(ExpandableListView Parent, View view, int groupposi, long l) {
        switch (groupposi) {
            case 0:
            //    Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show.class));

             //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
               // Log.d(">>>", "" + groupPosition);
                break;
            case 1:
              //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show1.class));
              //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
             //   Log.d(">>>", "" + groupPosition);
                break;
            case 2:
              //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show2.class));
              //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
           //     Log.d(">>>", "" + groupPosition);
                break;
            case 3:
             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show3.class));
             //   Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
          //      Log.d(">>>", "" + groupPosition);
                break;
            case 4:
             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show4.class));
             //   Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
            case 5:
             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show5.class));
              //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
            case 6:
             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show6.class));
            //    Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
            case 7:
             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show7.class));
            //    Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
            case 8:
               // Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show8.class));
                //Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
            case 9:
             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show9.class));
            //    Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
            case 10:
              //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Quiz.this,Show10.class));
             //   Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                break;
//            case 11:
//             //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(Quiz.this,Show11.class));
//              //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
//                break;

        }

        return false;
    }
});
    }


    private ArrayList<Team>getData(){
        Team tp1 =new Team("White Belt 10'th kup");
        Team tp2 =new Team("Yellow Stripe 9'th kup");
        Team tp3 =new Team("Yellow Belt 8'th kup");
        Team tp4 =new Team("Green Stripe 7'th kup");
        Team tp5 =new Team("Green Belt 6'th kup");
        Team tp6 =new Team("Blue Stripe 5'th kup");
        Team tp7 =new Team("Blue Belt 4'th kup");
        Team tp8 =new Team("Red Stripe 3'rd kup");
        Team tp9 =new Team("Red Belt 2'nd kup");
        Team tp10 =new Team("1'st Kup to 1'st Degree Black Belt");
//        Team tp11=new Team("1'st Kup to 1'st Degree Black Belt");


        ArrayList<Team> allTeams =new ArrayList<Team>();
        allTeams.add(tp1);
        allTeams.add(tp2);
        allTeams.add(tp3);
        allTeams.add(tp4);
        allTeams.add(tp5);
        allTeams.add(tp6);
        allTeams.add(tp7);
        allTeams.add(tp8);
        allTeams.add(tp9);
        allTeams.add(tp10);
//        allTeams.add(tp11);

        return allTeams;
    }
}
