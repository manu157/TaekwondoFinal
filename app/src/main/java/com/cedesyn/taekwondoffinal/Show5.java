package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show5  extends AppCompatActivity{
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar5;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show5);
        snackbar5=(ConstraintLayout)findViewById(R.id.snacks5);

        btn=(Button)findViewById(R.id.ans5);


        ques=(TextView)findViewById(R.id.qstn5);
        ans1=(TextView) findViewById(R.id.o15);
        ans2=(TextView) findViewById(R.id.o25);
        ans3=(TextView) findViewById(R.id.o35);
        ans4=(TextView) findViewById(R.id.o45);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("A MOKTAK WAS?");
        ans1.setText("A HOUSE");
        ans2.setText("A WOODEN PERCUSSIAN INSTRUMENT");
        ans3.setText("A HOME");
        ans4.setText("A PERSON");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "A WOODEN PERCUSSIAN INSTRUMENT ", Snackbar.LENGTH_LONG);
                    snackBar5.show();
                }

                if(count==2){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "A SENSE OF CALM, HUMILITY & MODESTY", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                } if(count==3){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                } if(count==4){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES. ", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                } if(count==5){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "Great Philosopher", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                } if(count==6){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "38", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                } if(count==7){
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "Groin", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                } if(count==8) {
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "To gain distance", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                }
                if(count==9) {
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "TAEKWON-DO was formed", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                }
                if(count==10) {
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "I", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                }if(count==11) {
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "High Double Forearm block", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                }if(count==12) {
                    Snackbar snackBar2 = Snackbar.make(snackbar5, "JAO Matsoki", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==13) {
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "Balkal", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                }
                if(count==14) {
                    Snackbar snackBar5 = Snackbar.make(snackbar5, "Bandalson", Snackbar.LENGTH_LONG);
                    snackBar5.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){img2.setVisibility(View.INVISIBLE);
                    ques.setText("A MOKTAK WAS?");
                    ans1.setText("A HOUSE");
                    ans2.setText("A WOODEN PERCUSSIAN INSTRUMENT");
                    ans3.setText("A HOME");
                    ans4.setText("A PERSON");

                } if(count==2){
                    ques.setText("MOA CHUNBI SOGI CAN CONVEY?");
                    ans1.setText("A SENSE OF CALM, HUMILITY & MODESTY");
                    ans2.setText("A SENSE OF TIME, LOVE & TENDERNESS");
                    ans3.setText("A SENSE OF HOPE, FAITH & GLORY");
                    ans4.setText("A SENSE OF HEART, FEAR & HONESTY");

                } if(count==3){
                    ques.setText("GREEN");
                    ans1.setText("SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP");
                    ans2.setText(" SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES");
                    ans3.setText("SIGNIFIES DANGER");
                    ans4.setText("SIGNIFIES WAR & PEACE");


                } if(count==4){
                    ques.setText("BLUE");
                    ans1.setText("SIGNIFIES THE HEAVENS TOWARDS WHITCH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN AEKWON-DO PROGRESSES");
                    ans2.setText("SIGNIFIES THE PLANTS GROWTH");
                    ans3.setText("SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP");
                    ans4.setText("SIGNIFIES HAPPINESS");

                } if(count==5){
                    ques.setText("Was Yul-Gok?");
                    ans1.setText("Great Philosopher");
                    ans2.setText("Great Taekwon-do student");
                    ans3.setText("Great Poet");
                    ans4.setText("Great Speaker");


                } if(count==6){
                    ques.setText("Numbers of moves in pattern Yul-Gok?");
                    ans1.setText("37");
                    ans2.setText("38");
                    ans3.setText("28");
                    ans4.setText("32");

                } if(count==7){
                    ques.setText("Which target area is the focus when performing front snap kick?");
                    ans1.setText("Groin");
                    ans2.setText("head");
                    ans3.setText("chest");
                    ans4.setText("knee");

                } if(count==8) {
                    ques.setText("Why perform X-stance?");
                    ans1.setText("To gain distance");
                    ans2.setText("to slide");
                    ans3.setText("to try it");
                    ans4.setText("keep in time");

                }if(count==9) {
                    ques.setText("What happened on April 11th 1955?");
                    ans1.setText("TAEKWON-DO was formed");
                    ans2.setText("General Choi was born");
                    ans3.setText("Taekwon-do was brought to England");
                    ans4.setText("ITF was formed");

                }if(count==10) {
                    ques.setText("Symbol of pattern Yul-Gok?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("S");
                    ans4.setText("I");

                }if(count==11) {

                    ques.setText("Name a defensive move from Yul-Gok?");
                    ans1.setText("High Double Forearm block");
                    ans2.setText("Front snap kick");
                    ans3.setText("rising block");
                    ans4.setText("upper palm block");

                }if(count==12) {
                    ques.setText("What is free sparring?");
                    ans1.setText("ILBO matsoki");
                    ans2.setText("IBO Matsoki");
                    ans3.setText("SANG Matsoki");
                    ans4.setText("JAO Matsoki");

                }if(count==13) {
                    ques.setText("Which is a foot part?");
                    ans1.setText("Ap Joomuk");
                    ans2.setText("Dung Joomuk");
                    ans3.setText("Balkal");
                    ans4.setText("Yop Joomuk");
//                    Which is a foot part? Ap Joomuk, Dung Joomuk, Balkal, Yop Joomuk, Bandalson, Sonkut, Sonkul, Sonbadak
//
//                    Which is a hand part? Balkal, Balkal Dung, Dwit Chook, Bandalson, Dwit Kumchi,


                }if(count==14) {img1.setVisibility(View.GONE);
                    ques.setText("Which is a hand part?");
                    ans1.setText("Balkal");
                    ans2.setText("Balkal Dung");
                    ans3.setText("Dwit Chook");
                    ans4.setText("Bandalson");

                }

                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){      img2.setVisibility(View.GONE);
                    ques.setText("A MOKTAK WAS?");
                    ans1.setText("A HOUSE");
                    ans2.setText("A WOODEN PERCUSSIAN INSTRUMENT");
                    ans3.setText("A HOME");
                    ans4.setText("A PERSON");

                } if(count==2){
                    ques.setText("MOA CHUNBI SOGI CAN CONVEY?");
                    ans1.setText("A SENSE OF CALM, HUMILITY & MODESTY");
                    ans2.setText("A SENSE OF TIME, LOVE & TENDERNESS");
                    ans3.setText("A SENSE OF HOPE, FAITH & GLORY");
                    ans4.setText("A SENSE OF HEART, FEAR & HONESTY");

                } if(count==3){
                    ques.setText("GREEN");
                    ans1.setText("SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP");
                    ans2.setText(" SIGNIFIES THE HEAVENS TOWARDS WHICH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN TAEKWON-DO PROGRESSES");
                    ans3.setText("SIGNIFIES DANGER");
                    ans4.setText("SIGNIFIES WAR & PEACE");


                } if(count==4){
                    ques.setText("BLUE");
                    ans1.setText("SIGNIFIES THE HEAVENS TOWARDS WHITCH THE PLANT MATURES INTO A TOWERING TREE AS TRAINING IN AEKWON-DO PROGRESSES");
                    ans2.setText("SIGNIFIES THE PLANTS GROWTH");
                    ans3.setText("SIGNIFIES THE PLANTS GROWTH AS TAEKWON-DO SKILLS BEGIN TO DEVELOP");
                    ans4.setText("SIGNIFIES HAPPINESS");

                } if(count==5){
                    ques.setText("Was Yul-Gok?");
                    ans1.setText("Great Philosopher");
                    ans2.setText("Great Taekwon-do student");
                    ans3.setText("Great Poet");
                    ans4.setText("Great Speaker");


                } if(count==6){
                    ques.setText("Numbers of moves in pattern Yul-Gok?");
                    ans1.setText("37");
                    ans2.setText("38");
                    ans3.setText("28");
                    ans4.setText("32");

                } if(count==7){
                    ques.setText("Which target area is the focus when performing front snap kick?");
                    ans1.setText("Groin");
                    ans2.setText("head");
                    ans3.setText("chest");
                    ans4.setText("knee");

                } if(count==8) {
                    ques.setText("Why perform X-stance?");
                    ans1.setText("To gain distance");
                    ans2.setText("to slide");
                    ans3.setText("to try it");
                    ans4.setText("keep in time");

                }if(count==9) {
                    ques.setText("What happened on April 11th 1955?");
                    ans1.setText("TAEKWON-DO was formed");
                    ans2.setText("General Choi was born");
                    ans3.setText("Taekwon-do was brought to England");
                    ans4.setText("ITF was formed");

                }if(count==10) {
                    ques.setText("Symbol of pattern Yul-Gok?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("S");
                    ans4.setText("I");

                }if(count==11) {

                    ques.setText("Name a defensive move from Yul-Gok?");
                    ans1.setText("High Double Forearm block");
                    ans2.setText("Front snap kick");
                    ans3.setText("rising block");
                    ans4.setText("upper palm block");

                }if(count==12) {
                    ques.setText("What is free sparring?");
                    ans1.setText("ILBO matsoki");
                    ans2.setText("IBO Matsoki");
                    ans3.setText("SANG Matsoki");
                    ans4.setText("JAO Matsoki");

                }if(count==13) {
                    ques.setText("Which is a foot part?");
                    ans1.setText("Ap Joomuk");
                    ans2.setText("Dung Joomuk");
                    ans3.setText("Balkal");
                    ans4.setText("Yop Joomuk");
//                    Which is a foot part? Ap Joomuk, Dung Joomuk, Balkal, Yop Joomuk, Bandalson, Sonkut, Sonkul, Sonbadak
//
//                    Which is a hand part? Balkal, Balkal Dung, Dwit Chook, Bandalson, Dwit Kumchi,


                }if(count==14) {img1.setVisibility(View.GONE);
                    ques.setText("Which is a hand part?");
                    ans1.setText("Balkal");
                    ans2.setText("Balkal Dung");
                    ans3.setText("Dwit Chook");
                    ans4.setText("Bandalson");

                }
                if(count ==14){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }


            }
        });


    }
}
