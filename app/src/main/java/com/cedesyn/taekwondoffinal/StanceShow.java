package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 22-03-2018.
 */

public class StanceShow extends AppCompatActivity {
    String aa,bb,cc,dd,ee,aa1,bb1,cc1,dd1,ee1,ff,ff1,gg,gg1;
   TextView heads,descpt;
   ImageView img;
    String check;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.showstance);

        heads=(TextView)findViewById(R.id.head);
        descpt=(TextView)findViewById(R.id.descrpt);
        img=(ImageView)findViewById(R.id.stan);


        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
//        // Applying font
        heads.setTypeface(tf);
        descpt.setTypeface(tf);

        Intent yu = getIntent();
        check= yu.getStringExtra("A");
      //  Toast.makeText(this, ""+check, Toast.LENGTH_SHORT).show();

        aa="ATTENTION STANCE";
        aa1=" \nAttention Stance (Charyot Sogi) \n " +
                "\nThis stance is the formal attention stance used to" +
                "ready for bowing.  It is also the stance that Junior" +
                "grades should adopt when talking to instructors or" +
                "examiners.\nWeight: 50:50\n" +
                "\nFeet: heels together and feet angled at 45 degrees.\n" +
                "\nWidth: na.\n" +
                "\nLength: na.\n" +
                "\nOther: Arms should be held out to the sides, fists" +
                "clenched and elbows slightly bent.";

        bb="CLOSED STANCE";
        bb1="\nClosed Stance (Moa Junbi Sogi)"
+                "\nThis stance is the opening stance for many " +
                "patterns.  There are 3 closed stances (a,b and c) as " +
                "the hand positions vary in different " +
                "patterns.\nWeight:50:50\n" +
                "\nFeet: together and facing forward.\n" +
                "\nLength: na\n" +
                "\nA: Seen in Won Hyo Tul. the hands are held 30" +
                "cm from head and level with the philtrum.  One" +
                "fist is clenched and the other, held loosely," +
                "envelops the first.\n" +
                "\nB: to be updated.\n" +
                "\nC: to be updated.";

        cc="WALKING STANCE";
        cc1=" \nWalking Stance (Gunnun Sogi)" +
                "\nThis stance is very stable and is useful for moving" +
                "forward and backwards into punches, grabs," +
                "throws and many hand techniques.\nWeight: 50:50\n" +
                "\nFeet: Front foot straight forward, back foot 25" +
                "degrees.  Front leg bent with knee above heel.\n" +
                " Back leg straight and locked.\n" +
                "\n" +
                "Width: 1 shoulders width from centre of instep to" +
                "centre of instep.\n" +
                "\nLength: 1.5 shoulders width from big toes to big" +
                " toe.\n" +
                "\nOther: Feet should be pulling to grip floor and" +
                "keep stance strong.";

        dd="L STANCE";
        dd1="\nL Stance (Niunja Sogi)" +
                "\nThis is a very mobile stance and the weight " +
                "distribution allows the fighter to kick with the " +
                "front leg without noticeably shifting weight." +
                "This stance is very close to the fighting stances " +
                "seen in sparring and boxing.\nWeight: 70% on the" +
                "back leg. 30% on the front leg.\n" +
                "\nFeet: Both feet should turn in 15 degrees.\n" +
                "\nWidth: 2.5 cm between the big toe on the front " +
                "foot and the heel of the rear leg.\n" +
                "\nLength: 1.5 shoulders width front the big toes of " +
                "the front foot to the foot sword of the rear foot.\n" +
                "\nOther:  As most weight is on the back foot, any" +
                "punch from the front hand is called a reverse" +
                "punch and any punch from the back hand is called" +
                "an obverse punch. If the right leg is back this is" +
                "called a right stance.";

        ee="PARALLEL READY STANCE";
        ee1="\nParallel Ready Stance (Naranhi Junbi Sogi)" +
                "\nThe is a very common stance as it is used at the" +
                "beginning of many patterns and in response to the" +
                "Junbi command.\nWeight: 50:50\n" +
                "\nFeet: Facing forward and parallel. \n" +
                "\nWidth: One shoulders width from the outside of" +
                "each foot.\n" +
                "\nLength: na\n" +
                "\n" +
                "\nOther: Fists should be clenched and held in front " +
                "of the body at belt height.  Each should be 5cm " +
                "apart and 10cm from the abdomen.  Elbows " +
                "should be bent 30 degrees and be 10cm from the " +
                "floating ribs.  Lower arms should be bent 40 " +
                "degrees.\n" +
                "\nThe junbi motions is often accompanied by a " +
                "large inhale and exhale which is believed to be a " +
                "concentrated release of Ki energy.";

        ff="REAR FOOT STANCE";
        ff1=" \nRear Foot Stance (Dwit Bal Sogi)" +
                "\nThis stance gives the ability to adjust distance " +
                "from an opponent or kick, using the front foot and " +
                "without the need to shift weight.\nWeight: the " +
                "encyclopedia says “most on back foot” other " +
                "experts say 90:10\n" +
                "\nFeet: The front foot is tuned in 25 degrees and the " +
                "back foot is tuned in 15 degrees.\n" +
                "\nLength: 1 shoulders width between the small toes " +
                "\nOther: This stance is often confused with the " +
                "measurements of the vertical stance.";

        gg="SITTING STANCE";
        gg1="\nSitting Stance (Annun Sogi)" +
                "\nThis stance is wide and low.  It is present in most " +
                "eastern martial arts and is often used to strengthen " +
                "leg muscles as the stance is held by tensing leg " +
                "muscles and scraping side soles.\nWeight: 50:50\n" +
                "\nFeet: facing forward and parallel, knees bent and" +
                "above ball of foot\n" +
                "\nWidth: 1.5 shoulders width between the big toes\n" +
                "\nLength: na\n" +
                "\nOther: The most important feature of this stance " +
                "is that the weight is dropped low (making the " +
                "person appear heavier) and the feet make a wide " +
                "base (making the person more stable on one " +
                "plane). Though this stance is often associated with " +
                "punching, the stability and mass it creates makes " +
                "it very versatile.  Some of its many uses include: " +
                "punching, take downs (Do San, Kwang Gae) " +
                "Grappling (see Judo for similarities) and escapes " +
                "i.e Bear hug, being dragged).";


        switch (check) {

            case "1":
                heads.setText(aa);
                descpt.setText(aa1);
                img.setImageResource(R.drawable.attention);
                break;

            case "2":
                heads.setText(bb);
                descpt.setText(bb1);
                img.setImageResource(R.drawable.closed_stance);
                break;

            case "3":
                heads.setText(cc);
                descpt.setText(cc1);
                img.setImageResource(R.drawable.walkingstance);
                break;

            case "4":
                heads.setText(dd);
                descpt.setText(dd1);
                img.setImageResource(R.drawable.lstance);
                break;

            case "5":
                heads.setText(ee);
                descpt.setText(ee1);
                img.setImageResource(R.drawable.parallels);
                break;

            case "6":
                heads.setText(ff);
                descpt.setText(ff1);
                img.setImageResource(R.drawable.rearfootstance);
                break;

            case "7":
                heads.setText(gg);
                descpt.setText(gg1);
                img.setImageResource(R.drawable.sitstanc);
                break;

        }


    }


    }

