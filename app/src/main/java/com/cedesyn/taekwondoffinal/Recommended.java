package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

/**
 * Created by jerin android on 31-01-2018.
 */

public class Recommended extends AppCompatActivity {
    TextView txt,txtp;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recommended);
        txt =(TextView)findViewById(R.id.texti);
        txtp=(TextView)findViewById(R.id.textp);

        String fontPath = "ArialBold.ttf";
//        // text view label
//       TextView txtGhost = (TextView) findViewById(R.id.ghost);
//        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
//        // Applying font
        txt.setTypeface(tf);
        txtp.setTypeface(tf);
        txtp.setText("RECOMMENDATIONS FOR TAEKWON-DO STUDENTS");
      //  "RECOMMENDATIONS FOR TAEKWON-DO STUDENTS\n"+
        txt.setMovementMethod(new ScrollingMovementMethod());
        txt.setText("Students should always practice their current Taekwondo techniques outside of class. This will be patterns, hand techniques, kicking, set sparring, breaking (with a pad) techniques and line work etc... In addition we would also encourage students to concentrate on their theory regularly. This app will help with theory and further your knowledge of ITF style Taekwondo. \n" +
                "\nYou should always compliment your training with other forms of exercise. Stretching will help with flexibility however you must ensure you perform stretches properly. \n" +
                "\nIf the student wishes to improve their techniques & improve their Taekwondo ability and performance we would suggest various forms of exercise to compliment training. The important question is what & how do the student want to develop or improve?  \n" +
                "\nThere are different physical exercises to help an individual, these are :\n" +
                "\nCardio, \n" +
                "\nPower (muscle strength) \n" +
                "\nEndurance, \n" +
                "\nFlexibility \n" +
                "\nSpeed\n" +
                "\nAgility\n" +
                "\nBalance\n" +
                "\n\nCo-ordination\n" +
                "\nMany forms of exercise will complement a Taekwondo student. This could include other sports such as badminton, squash, cricket, football, rugby, jogging, walking, tennis, running, netball, trampoline, gymnastics and attending the gym. All the exercises will increase your heart rate, improve cardio, improve balance, timing, co-ordination, endurance. If attending gym to increase power/strength it would be advised to perform the movements correctly to reduce any stress on spine. I would advise a good warm up before any exercise and cool down to aid recovery of muscle groups.  \n" +
                "\nGood exercises could be Yoga, Body balance, Pilates as these will aid balance co-ordination.  Most focus on strength, flexibility and breathing whilst helping with mental wellbeing. They also focus on different forms of stretching.  \n" +
                "\nAdditional exercises around the home could be jumping jacks, ski jumps, plank & side plank, heel touches, leg raises, flutter kicks, incline push ups, chair dips, tricep push ups, seated shoulder press, lateral raises, bicep curls, hammer curls, squats, jumping squats, lunges, walking lunges, glute kickbacks & hip extensions. \n" +
                "If you use Frequency, Intensity, Time & Type (FITT) this will aid planning to manage your additional training & your Taekwondo training.\n" +
                "\nIt is also important to consider that the body needs to recover, so rest is important. A good method is to use 1 day on then 1 day off.\n");

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(Recommended.this,MainActivity.class));
        finish();
    }
}
