package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by jerin android on 02-02-2018.
 */

public class NumberofMoves extends AppCompatActivity {
public String parent,child;
TextView showq;
String wh,yes,yeb,ges,geb,bls,blb,res,reb,bs,bdb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
setContentView(R.layout.numberofmoves);

       
showq =(TextView)findViewById(R.id.textq) ;
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
//        // Applying font
        showq.setTypeface(tf);
 wh="White Belt 10'th kup has 14 movements.";
 yes="Yellow Stripe 9'th kup has 19 movements.";
 yeb="Yellow Belt 8'th kup has 21 movements.";
 ges="Green Stripe 7'th kup has 24 movements.";
geb="Green Belt 6'th kup has 28 movements.";
bls+="Blue Stripe 5'th kup has 38 movements.";
blb="Blue Belt 4'th kup has 32 movements.";
res="Redback Stripe 3'rd kup has 37 movements.";
reb="Redback Belt 2'nd kup has 29 movements.";
bs="Black Stripe 1'st kup has 30 movements.";
bdb="1'st Degree Black Belt has 39 movements.";
        Intent ii = getIntent();
        parent= ii.getStringExtra("parentpos");
        child= ii.getStringExtra("chldpos");
       // Toast.makeText(this, ""+child, Toast.LENGTH_SHORT).show();
     //   Toast.makeText(this, ""+parent, Toast.LENGTH_SHORT).show();


        switch (parent){

            case "0":
         showq.setText(wh);
         break;
            case "1":
         showq.setText(yes);
         break;
     case "2":
         showq.setText(yeb);
         break;
     case "3":
         showq.setText(ges);
         break;
     case "4":
         showq.setText(geb);
         break;
     case "5":
         showq.setText(bls);
         break;
     case "6":
         showq.setText(blb);
         break;
     case "7":
         showq.setText(res);
         break;
     case "8":
         showq.setText(reb);
         break;
     case "9":
         showq.setText(bs);
         break;
     case "10":
         showq.setText(bdb);
         break;



 }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(NumberofMoves.this,Belts.class));
        finish();
    }
}
