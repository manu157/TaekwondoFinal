package com.cedesyn.taekwondoffinal;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jerin android on 02-03-2018.
 */

public class TypgradAdapter extends BaseExpandableListAdapter {
    private Context c;
    private ArrayList<Team1> team1;
    private LayoutInflater inflater;

    public TypgradAdapter(Context c, ArrayList<Team1> team1)
    {
        this .c=c;
        this.team1=team1;
        inflater=(LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //GET A SINGLE PLAYER


    //GET PLAYER ID
    @Override
    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }



    //GET TEAM
    @Override
    public Object getGroup(int groupPos) {
        // TODO Auto-generated method stub
        return team1.get(groupPos);
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    //GET NUMBER OF TEAMS
    @Override
    public int getGroupCount() {
        // TODO Auto-generated method stub
        return team1.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 0;
    }

    //GET TEAM ID
    @Override
    public long getGroupId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    //GET TEAM ROW
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        //ONLY INFLATE XML TEAM ROW MODEL IF ITS NOT PRESENT,OTHERWISE REUSE IT
        if(convertView == null)
        {
            convertView=inflater.inflate(R.layout.team1, null);
        }

        //GET GROUP/TEAM ITEM
        Team1 t=(Team1) getGroup(groupPosition);

        //SET GROUP NAME
        TextView nameTv=(TextView) convertView.findViewById(R.id.textViews1);
        ImageView img=(ImageView) convertView.findViewById(R.id.imageViews1);

        String name=t.Namesw;
        nameTv.setText(name);

        //ASSIGN TEAM IMAGES ACCORDING TO TEAM NAME

        if(name=="Typical Grading For 10'th kup")
        {
            img.setImageResource(R.drawable.whitequ);
        }else if(name=="Typical Grading For 9'th kup")
        {
            img.setImageResource(R.drawable.whiteyellowstripe);
        }else if(name=="Typical Grading For 8'th kup")
        {
            img.setImageResource(R.drawable.yellow);
        }
        else if(name=="Typical Grading For 7'th kup")
        {
            img.setImageResource(R.drawable.yellowgreen);
        }
        else if(name=="Typical Grading For 6'th kup")
        {
            img.setImageResource(R.drawable.green);
        }else if(name=="Typical Grading For 5'th kup")
        {
            img.setImageResource(R.drawable.greenblue);
        }else if(name=="Typical Grading For 4'th kup")
        {
            img.setImageResource(R.drawable.blue);
        }else if(name=="Typical Grading For 3'rd kup")
        {
            img.setImageResource(R.drawable.bluered);
        }else if(name=="Typical Grading For 2'nd kup")
        {
            img.setImageResource(R.drawable.red);
        }else if(name=="Typical Grading For 1'st Kup ") {
            img.setImageResource(R.drawable.redblack);
        }
//        else if(name=="1'st Kup to 1'st Degree Black Belt") {
//            img.setImageResource(R.drawable.black);
//        }
        //SET TEAM ROW BACKGROUND COLOR
        convertView.setBackgroundColor(Color.WHITE);


        return convertView;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }



    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }

}
