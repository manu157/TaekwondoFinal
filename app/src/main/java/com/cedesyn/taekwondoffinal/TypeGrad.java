package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by jerin android on 02-03-2018.
 */

public class TypeGrad extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.typegrad);

        ExpandableListView customs = (ExpandableListView) findViewById(R.id.typexp);

        final ArrayList<Team1> team1 = getData();


        //create and bind with adapter
        TypgradAdapter adapter = new TypgradAdapter(TypeGrad.this, team1);
        customs.setAdapter((ExpandableListAdapter) adapter);

        customs.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView Parent, View view, int groupposi, long l) {
                switch (groupposi) {
                    case 0:
                        //    Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(TypeGrad.this,Typegradpage1.class);
                        intent.putExtra("val",0);
                        startActivity(intent);
                      //     Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        // Log.d(">>>", "" + groupPosition);
                        break;
                    case 1:
                        //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",1);
                        startActivity(intent);
                    //    Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();

                        //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        //   Log.d(">>>", "" + groupPosition);
                        break;
                    case 2:
                        //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",2);
                        startActivity(intent);
                    //    Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();

                        //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        //     Log.d(">>>", "" + groupPosition);
                        break;
                    case 3:
                        //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",3);
                        startActivity(intent);
                 //       Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        //      Log.d(">>>", "" + groupPosition);
                        break;
                    case 4:
                        //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",4);
                        startActivity(intent);
                   //     Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        //   Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
                        //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",5);
                        startActivity(intent);
                      //  Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();
//  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        break;
                    case 6:
                        //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",6);
                        startActivity(intent);
                   //     Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();
//    Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        break;
                    case 7:
                        //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",7);
                        startActivity(intent);
                        Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        //    Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        break;
                    case 8:
                      //  Toast.makeText(TypeGrad.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",8);                 //          Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();

                        startActivity(intent);

                        break;
                    case 9:
                        //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",9);                  //         Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();

                        startActivity(intent);                        //    Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        break;
                    case 10:
                        //  Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
                        intent=new Intent(TypeGrad.this,Typegradpage.class);
                        intent.putExtra("val",10);
                        startActivity(intent);                         //  Toast.makeText(TypeGrad.this, ""+groupposi, Toast.LENGTH_SHORT).show();

                        //   Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
                        break;
//                    case 11:
//                        //   Toast.makeText(Quiz.this, "WE CAN DO ANYTHING", Toast.LENGTH_SHORT).show();
//                        intent=new Intent(TypeGrad.this,Typegradpage.class);
//                        intent.putExtra("val",11);
//                        startActivity(intent);                        //  Toast.makeText(Quiz.this, ""+groupposi, Toast.LENGTH_SHORT).show();
//                        break;

                }

                return false;
            }
        });
    }

    private ArrayList<Team1> getData () {
        Team1 tp1 = new Team1("Typical Grading For 10'th kup");
        Team1 tp2 = new Team1("Typical Grading For 9'th kup");
        Team1 tp3 = new Team1("Typical Grading For 8'th kup");
        Team1 tp4 = new Team1("Typical Grading For 7'th kup");
        Team1 tp5 = new Team1("Typical Grading For 6'th kup");
        Team1 tp6 = new Team1("Typical Grading For 5'th kup");
        Team1 tp7 = new Team1("Typical Grading For 4'th kup");
        Team1 tp8 = new Team1("Typical Grading For 3'rd kup");
        Team1 tp9 = new Team1("Typical Grading For 2'nd kup");
        Team1 tp10 = new Team1("Typical Grading For 1'st Kup ");
       // Team1 tp11 = new Team1("1'st Kup to 1'st Degree Black Belt");


        ArrayList<Team1> allTeams1 = new ArrayList<Team1>();
        allTeams1.add(tp1);
        allTeams1.add(tp2);
        allTeams1.add(tp3);
        allTeams1.add(tp4);
        allTeams1.add(tp5);
        allTeams1.add(tp6);
        allTeams1.add(tp7);
        allTeams1.add(tp8);
        allTeams1.add(tp9);
        allTeams1.add(tp10);
     //   allTeams1.add(tp11);

        return allTeams1;
        }
    }
