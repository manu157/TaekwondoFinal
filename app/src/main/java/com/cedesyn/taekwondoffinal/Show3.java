package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show3 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar3;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show3); 
        snackbar3=(ConstraintLayout)findViewById(R.id.snacks3);

        btn=(Button)findViewById(R.id.ans3);


        ques=(TextView)findViewById(R.id.qstn3);
        ans1=(TextView) findViewById(R.id.o13);
        ans2=(TextView) findViewById(R.id.o23);
        ans3=(TextView) findViewById(R.id.o33);
        ans4=(TextView) findViewById(R.id.o43);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("How many moves in Do-San?");
        ans1.setText("29");
        ans2.setText("31");
        ans3.setText("23");
        ans4.setText("24");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "24 ", Snackbar.LENGTH_LONG);
                    snackBar3.show();
                }

                if(count==2){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Dollyo Chagi", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                } if(count==3){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Wedging", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                } if(count==4){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Thrust", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                } if(count==5){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Son", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                } if(count==6){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "SAMBO Matsoki", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                } if(count==7){
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Jappyolsol Tae", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                } if(count==8) {
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Aim for specific target areas", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                }
                if(count==9) {
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Sonkul", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                }
                if(count==10) {
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Balkal & Footsword", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                }if(count==11) {
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Jao Matsoki", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                }if(count==12) {
                    Snackbar snackBar3 = Snackbar.make(snackbar3, "Straight edged S", Snackbar.LENGTH_LONG);
                    snackBar3.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){
                    img2.setVisibility(View.INVISIBLE);
                    ques.setText("How many moves in Do-San?");
                    ans1.setText("29");
                    ans2.setText("31");
                    ans3.setText("23");
                    ans4.setText("24");

                } if(count==2){
                    ques.setText("What is Turning kick in Korean?");
                    ans1.setText("Dwit Chagi");
                    ans2.setText("Dollyo Chagi");
                    ans3.setText("Moa Chunbi Sogi");
                    ans4.setText("Yop Chagi");

                } if(count==3){
                    ques.setText("What is Hechyo in English?");
                    ans1.setText("Timing");
                    ans2.setText("Strike");
                    ans3.setText("Wedging");
                    ans4.setText("Making");


                } if(count==4){
                    ques.setText("What is Tulgi in English?");
                    ans1.setText("Thrust");
                    ans2.setText("Straight");
                    ans3.setText("Fingerip");
                    ans4.setText("Knifehand");

                } if(count==5){
                    ques.setText("What is Straight in Korean?");
                    ans1.setText("Sonkut");
                    ans2.setText("Sun");
                    ans3.setText("Son");
                    ans4.setText("Block");


                } if(count==6){
                    ques.setText("What is 3 Step Sparring in Korean?");
                    ans1.setText("IBO Matsoki");
                    ans2.setText("ILBO Matsoki");
                    ans3.setText("JAO Matsok");
                    ans4.setText("SAMBO Matsoki");

                } if(count==7){
                    ques.setText("What is release move in Korean?");
                    ans1.setText("AP Tae");
                    ans2.setText("Bow Tie");
                    ans3.setText("Jappyolsol Tae");
                    ans4.setText("Tae chi");

                } if(count==8) {
                    ques.setText(" Name 1 benefit from doing 3 step sparring??");
                    ans1.setText("Aim for specific target areas");
                    ans2.setText("you can rest");
                    ans3.setText("you can talk");
                    ans4.setText("you can stop focusing");

                }if(count==9) {
                    ques.setText(" Name 1 hand part in Korean?");
                    ans1.setText("Sonkul");
                    ans2.setText("Thumb");
                    ans3.setText("Knuckle");
                    ans4.setText("Palkup");

                }if(count==10) {
                    ques.setText(" Name 1 foot part in English and Korean?");
                    ans1.setText("Balkal & Footsword");
                    ans2.setText("Balkup & Footsword");
                    ans3.setText("Balding & Footsword");
                    ans4.setText("Baldal & Footsword");

                }if(count==11) {
                    ques.setText("What is free sparring in Korean?");
                    ans1.setText("Ibo Matsoki");
                    ans2.setText("ILbo Matsoki");
                    ans3.setText("Sambo Matsoki");
                    ans4.setText("Jao Matsoki");

                }if(count==12) {
                    img1.setVisibility(View.GONE);
                    ques.setText("What is pattern diagram for Do-San?");
                    ans1.setText("S");
                    ans2.setText("+");
                    ans3.setText("I");
                    ans4.setText("X");

                }if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){
                    img2.setVisibility(View.GONE);
                    ques.setText("How many moves in Do-San?");
                    ans1.setText("29");
                    ans2.setText("31");
                    ans3.setText("23");
                    ans4.setText("24");

                } if(count==2){
                    ques.setText("What is Turning kick in Korean?");
                    ans1.setText("Dwit Chagi");
                    ans2.setText("Dollyo Chagi");
                    ans3.setText("Moa Chunbi Sogi");
                    ans4.setText("Yop Chagi");

                } if(count==3){
                    ques.setText("What is Hechyo in English?");
                    ans1.setText("Timing");
                    ans2.setText("Strike");
                    ans3.setText("Wedging");
                    ans4.setText("Making");


                } if(count==4){
                    ques.setText("What is Tulgi in English?");
                    ans1.setText("Thrust");
                    ans2.setText("Straight");
                    ans3.setText("Fingerip");
                    ans4.setText("Knifehand");

                } if(count==5){
                    ques.setText("What is Straight in Karean?");
                    ans1.setText("Sonkut");
                    ans2.setText("Sun");
                    ans3.setText("Son");
                    ans4.setText("Block");


                } if(count==6){
                    ques.setText("What is 3 Step Sparring in Korean?");
                    ans1.setText("IBO Matsoki");
                    ans2.setText("ILBO Matsoki");
                    ans3.setText("JAO Matsok");
                    ans4.setText("SAMBO Matsoki");

                } if(count==7){
                    ques.setText("What is release move in Korean?");
                    ans1.setText("AP Tae");
                    ans2.setText("Bow Tie");
                    ans3.setText("Jappyolsol Tae");
                    ans4.setText("Tae chi");

                } if(count==8) {
                    ques.setText(" Name 1 benefit from doing 3 step sparring??");
                    ans1.setText("Aim for specific target areas");
                    ans2.setText("you can rest");
                    ans3.setText("you can talk");
                    ans4.setText("you can stop focusing");

                }if(count==9) {
                    ques.setText(" Name 1 hand part in Korean?");
                    ans1.setText("Sonkul");
                    ans2.setText("Thumb");
                    ans3.setText("Knuckle");
                    ans4.setText("Palkup");

                }if(count==10) {
                    ques.setText(" Name 1 foot part in English and Korean?");
                    ans1.setText("Balkal & Footsword");
                    ans2.setText("Balkup & Footsword");
                    ans3.setText("Balding & Footsword");
                    ans4.setText("Baldal & Footsword");

                }if(count==11) {
                    ques.setText("What is free sparring in Korean?");
                    ans1.setText("Ibo Matsoki");
                    ans2.setText("ILbo Matsoki");
                    ans3.setText("Sambo Matsoki");
                    ans4.setText("Jao Matsoki");

                }if(count==12) {img1.setVisibility(View.GONE);
                    ques.setText("What is pattern diagram for Do-San?");
                    ans1.setText("S");
                    ans2.setText("+");
                    ans3.setText("I");
                    ans4.setText("X");

                }
                if(count ==12){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }


            }
        });


    }
}
