package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show1 extends AppCompatActivity { Button btn;
    ImageView img1,img2;
    int count=1;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show1);

        snackbar1=(ConstraintLayout)findViewById(R.id.snacks1);

        btn=(Button)findViewById(R.id.ans1);


        ques=(TextView)findViewById(R.id.qstn1);
        ans1=(TextView) findViewById(R.id.o11);
        ans2=(TextView) findViewById(R.id.o21);
        ans3=(TextView) findViewById(R.id.o31);
        ans4=(TextView) findViewById(R.id.o41);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);

        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);

        if (count==1){
            img2.setVisibility(View.GONE);
        }


        count =1;
        ques.setText("How many moves in Chon-ji?");
        ans1.setText("23");
        ans2.setText("26");
        ans3.setText("19");
        ans4.setText("21");;


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "19 ", Snackbar.LENGTH_LONG);
                    snackBar1.show();
                }

                if(count==2){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "Integrity", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==3){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "1955", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==4){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "General Choi 9th Degree", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==5){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "Gunnun Sogi", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==6){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "Annun Sogi", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==7){
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "Niunja Sogi", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==8) {
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "5", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                }
                if(count==9) {
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "Yop Chagi", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                } if(count==10) {
                    Snackbar snackBar1 = Snackbar.make(snackbar1, "Taerigi", Snackbar.LENGTH_LONG);
                    snackBar1.show();

                }

            }
        });



        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){
                    img2.setVisibility(View.INVISIBLE);
                    ques.setText("How many moves in Chon-ji?");
                    ans1.setText("23");
                    ans2.setText("26");
                    ans3.setText("19");
                    ans4.setText("21");

                } if(count==2){
                    ques.setText("Name one of the tenets of Taekwon-Do?");
                    ans1.setText("Integrity");
                    ans2.setText("Respect");
                    ans3.setText("Loyalty");
                    ans4.setText("Obey");

                } if(count==3){
                    ques.setText("When was Taekwon-Do formed?");
                    ans1.setText("1955");
                    ans2.setText("1948");
                    ans3.setText("1966");
                    ans4.setText("1956");

                } if(count==4){
                    ques.setText("Name the founder of Taekwon-Do?");
                    ans1.setText("General Mustard 9th Dan");
                    ans2.setText("General Choi 9th Degree");
                    ans3.setText("General Lee 9th Degree");
                    ans4.setText("General Motors");

                } if(count==5){
                    ques.setText("What is Walking stance in Korean?");
                    ans1.setText("Annum Sogi");
                    ans2.setText("Niunja sogi");
                    ans3.setText("Nacho sogi");
                    ans4.setText("Gunnun Sogi");


                } if(count==6){
                    ques.setText("What is Sitting stance in Korean?");
                    ans1.setText("Annun Sogi");
                    ans2.setText("Gojong Sogi");
                    ans3.setText("Dwit Sogi");
                    ans4.setText("Gobruryo Sogi");

                } if(count==7){
                    ques.setText("What is L stance in Korean?");
                    ans1.setText("Gunnun Sogi");
                    ans2.setText("Gojong Sogi");
                    ans3.setText("Nacho Sogi");
                    ans4.setText("Niunja Sogi");

                } if(count==8) {


                    ques.setText("How many Tenets are there?");
                    ans1.setText("3");
                    ans2.setText("4");
                    ans3.setText("5");
                    ans4.setText("6");

                }if(count==9) {
                    ques.setText("What is side kick in Korean?");
                    ans1.setText("Dwit Chagi");
                    ans2.setText("Dollyo Chagi");
                    ans3.setText("Bandae Dollyo Chagi");
                    ans4.setText("Yop Chagi");

                }if(count==10) {
                    img1.setVisibility(View.GONE);
                    ques.setText("What is strike in Korean?");
                    ans1.setText("Jurigi");
                    ans2.setText("Makgi");
                    ans3.setText("Chagi");
                    ans4.setText("Taerigi");



                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }



            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
               // Toast.makeText(Show1.this, ""+String.valueOf(count), Toast.LENGTH_SHORT).show();
                if(count==1){
                    img2.setVisibility(View.GONE);

                    ques.setText("How many moves in Chon-ji?");
                    ans1.setText("23");
                    ans2.setText("26");
                    ans3.setText("19");
                    ans4.setText("21");

                } if(count==2){
                    ques.setText("Name one of the tenets of Taekwon-Do?");
                    ans1.setText("Integrity");
                    ans2.setText("Respect");
                    ans3.setText("Loyalty");
                    ans4.setText("Obey");

                } if(count==3){
                    ques.setText("When was Taekwon-Do formed?");
                    ans1.setText("1955");
                    ans2.setText("1948");
                    ans3.setText("1966");
                    ans4.setText("1956");

                } if(count==4){
                    ques.setText("Name the founder of Taekwon-Do?");
                    ans1.setText("General Mustard 9th Dan");
                    ans2.setText("General Choi 9th Degree");
                    ans3.setText("General Lee 9th Degree");
                    ans4.setText("General Motors");

                } if(count==5){
                    ques.setText("What is Walking stance in Korean?");
                    ans1.setText("Annum Sogi");
                    ans2.setText("Niunja sogi");
                    ans3.setText("Nacho sogi");
                    ans4.setText("Gunnun Sogi");


                } if(count==6){
                    ques.setText("What is Sitting stance in Korean?");
                    ans1.setText("Annun Sogi");
                    ans2.setText("Gojong Sogi");
                    ans3.setText("Dwit Sogi");
                    ans4.setText("Gobruryo Sogi");

                } if(count==7){
                    ques.setText("What is L stance in Korean?");
                    ans1.setText("Gunnun Sogi");
                    ans2.setText("Gojong Sogi");
                    ans3.setText("Nacho Sogi");
                    ans4.setText("Niunja Sogi");

                } if(count==8) {
                    ques.setText("How many Tenets are there?");
                    ans1.setText("3");
                    ans2.setText("4");
                    ans3.setText("5");
                    ans4.setText("6");

                }if(count==9) {
                    ques.setText("What is side kick in Korean?");
                    ans1.setText("Dwit Chagi");
                    ans2.setText("Dollyo Chagi");
                    ans3.setText("Bandae Dollyo Chagi");
                    ans4.setText("Yop Chagi");

                }if(count==10) {
                    ques.setText("What is strike in Korean?");
                    ans1.setText("Jurigi");
                    ans2.setText("Makgi");
                    ans3.setText("Chagi");
                    ans4.setText("Taerigi");



                }
                if(count >=10){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);
                }

            }
        });


    }
}
