package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;



public class DataPresentation  extends AppCompatActivity{
    TextView textView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.datapresentation1);
        textView =findViewById(R.id.data_to_show);
        String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        textView.setTypeface(tf);
        Intent intent=getIntent();
        String string=intent.getStringExtra("value");
        textView.setText(string);

    }
}
