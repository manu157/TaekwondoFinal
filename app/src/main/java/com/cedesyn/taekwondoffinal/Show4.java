package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show4 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar4;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show4);
        snackbar4=(ConstraintLayout)findViewById(R.id.snacks4);

        btn=(Button)findViewById(R.id.ans4);


        ques=(TextView)findViewById(R.id.qstn4);
        ans1=(TextView) findViewById(R.id.o14);
        ans2=(TextView) findViewById(R.id.o24);
        ans3=(TextView) findViewById(R.id.o34);
        ans4=(TextView) findViewById(R.id.o44);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("What is 2 step sparring in Korean?");
        ans1.setText("IBO MATSOKI");
        ans2.setText("ELBOW MATSOKI");
        ans3.setText("ILBO MATSOKI");
        ans4.setText("JAO MATSOKI");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "IBO MATSOKI ", Snackbar.LENGTH_LONG);
                    snackBar2.show();
                }

                if(count==2){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "Intermediate", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==3){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "GUBURYO SOGI", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==4){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "Gojong Sogi", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==5){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "50/50", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==6){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "70/30", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==7){
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "BANDALSON", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==8) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "I", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==9) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "CLOSED READY STANCE A (MOA CHUNBI SOGI) ", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==10) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "Back Kick", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==11) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "AN HISTORICAL EVENT", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==12) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "HELP WHERE POSSIBLE", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }if(count==13) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "28", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==14) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "AP KUMCHI", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==15) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "Free sparring", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==16) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "19", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==17) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "21", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==18) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "Sitting Stance", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==19) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "Attention", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==20) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "A Monk", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==11) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "TRUE ", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==22) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "REPRESENTING WAR/PEACE", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }
                if(count==23) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "A WOODEN PERCUSSIAN INSTRUMENT, ", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                } if(count==24) {
                    Snackbar snackBar2 = Snackbar.make(snackbar4, "A SENSE OF CALM, HUMILITY & MODESTY", Snackbar.LENGTH_LONG);
                    snackBar2.show();

                }



            }
        });



        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = count + 1;
                if (count == 1) {
                    img2.setVisibility(View.INVISIBLE);
                    ques.setText("What is 2 step sparring in Korean?");
                    ans1.setText("IBO MATSOKI");
                    ans2.setText("ELBOW MATSOKI");
                    ans3.setText("ILBO MATSOKI");
                    ans4.setText("JAO MATSOKI");

                }
                if (count == 2) {
                    ques.setText("What type of student is 2 Step sparring for?");
                    ans1.setText("Beginner");
                    ans2.setText("Intermediate");
                    ans3.setText("Experienced");
                    ans4.setText("None of the above");

                }
                if (count == 3) {
                    ques.setText("What is Bending stance in Korean?");
                    ans1.setText(" GUBURYO SOGI");
                    ans2.setText("ILBO SOGI");
                    ans3.setText("DWIT BAL SOGI");
                    ans4.setText("GOJONG SOGI");


                }
                if (count == 4) {
                    ques.setText("What is Fixed stance in Korean?");
                    ans1.setText("Gojong Sogi");
                    ans2.setText("Dwit Bal sogi");
                    ans3.setText("Annun Sogi");
                    ans4.setText("Gunnun Sogi");

                }
                if (count == 5) {
                    ques.setText(" What is body weight for fixed stance?");
                    ans1.setText("60/40");
                    ans2.setText("90/10");
                    ans3.setText("50/50");
                    ans4.setText("70/30");


                }
                if (count == 6) {
                    ques.setText("What is body weight for Niunja Sogi?");
                    ans1.setText("60/40");
                    ans2.setText("50/50");
                    ans3.setText("90/10");
                    ans4.setText("70/30");

                }
                if (count == 7) {
                    ques.setText("Which of these are a hand part?");
                    ans1.setText("Yi JOOMUK");
                    ans2.setText("DUNK JOOMUK");
                    ans3.setText("TOP JOOMUK");
                    ans4.setText("BANDALSON");

                }
                if (count == 8) {
                    ques.setText("What is pattern diagram for Won-Hyo?");
                    ans1.setText("I");
                    ans2.setText("S");
                    ans3.setText("X");
                    ans4.setText("W");

                }
                if (count == 9) {
                    ques.setText(" What is starting position for Won-Hyo?");
                    ans1.setText("CLOSED READY STANCE A (MOA CHUNBI SOGI)");
                    ans2.setText("L-STANCE TWIN FOREARM BLOCK (SANG PALMOK MAKGI)");
                    ans3.setText("L-STANCE INWARD KNIFEHAND STRIKE (ANAERO SONKUL TAERIGI)");
                    ans4.setText("None of the above");



                }
                if (count == 10) {
                    ques.setText("What is DWIT CHAGI in English?");
                    ans1.setText("Back Kick");
                    ans2.setText("Turning Kick");
                    ans3.setText("Side Kick");
                    ans4.setText("Front Rising Kick");

                }
                if (count == 11) {
                    ques.setText("What do the meanings of patterns refer to?");
                    ans1.setText("AN HISTORICAL EVENT");
                    ans2.setText("GENERAL CHOI BIRTHDAY");
                    ans3.setText("A SCHOOL");
                    ans4.setText("A GRADING");

                }
                if (count == 12) {
                    ques.setText("What do you do for your TAEKWON-DO school?");
                    ans1.setText("HELP WHERE POSSIBLE");
                    ans2.setText("NOTHING");
                    ans3.setText("NOT TRY");
                   // ans4.setText("Bakaero");

                }
                if (count == 13) {
                    ques.setText("How many move in Won-Hyo?");
                    ans1.setText("29");
                    ans2.setText("31");
                    ans3.setText("26");
                    ans4.setText("28");

                }
                if (count == 14) {
                    ques.setText("Name one foot part in Korean?");
                    ans1.setText("AP KUMCHI");
                    ans2.setText("BALKAT");
                    ans3.setText("DWITCH CHOOK");
                    ans4.setText("BALDEEP");

                }
                if (count == 15) {
                    ques.setText("What is JAO MATSOKI in English?");
                    ans1.setText("1 Step sparring");
                    ans2.setText("Free sparring");
                    ans3.setText("2 step sparring");
                    ans4.setText("3 step sparring");

                }
                if (count == 16) {
                    ques.setText("How many moves in Chon-Ji");
                    ans1.setText("19");
                    ans2.setText("16");
                    ans3.setText("14");
                    ans4.setText("22");


                }if (count == 17) {
                    ques.setText("How many movements in Dan-Gun? ");
                    ans1.setText("23");
                    ans2.setText("21");
                    ans3.setText("19");
                    ans4.setText("24");

                } if (count == 18) {
                    ques.setText("What is last stance performed in Do-San called in English?");
                    ans1.setText("Rear Foot Stance");
                    ans2.setText("Low Stance");
                    ans3.setText("X Stance");
                    ans4.setText("Sitting Stance");

                }
                if (count == 19) {
                    ques.setText("What is Charyot in English?");
                    ans1.setText("Sitting");
                    ans2.setText("Jumping");
                    ans3.setText("Attention");
                    ans4.setText("Wen");

                } if (count == 20) {
                    ques.setText("WON-HYO WAS ?");
                    ans1.setText("A Monk");
                    ans2.setText("A Film star");
                    ans3.setText("A politician");
//                    ans4.setText("22");

                }
                if (count == 21) {
                    ques.setText("WON-HYO WAS? A SCHOOLAR TRUE OR FALSE?");
                    ans1.setText("True");
                    ans2.setText("False");
//                    ans3.setText("14");
//                    ans4.setText("22");

                }
                if (count == 22) {
                    ques.setText(" Moa Chunbi Sogi is often referred to as what?");
                    ans1.setText("REPRESENTING WAR/PEACE");
                    ans2.setText("REPRESENTING LOVE");
                    ans3.setText("REPRESENTING HAPPINESS");
                    ans4.setText("REPRESENTING FREEDOM");

                }
                if (count == 23) {
                    ques.setText("A MOKTAK WAS?");
                    ans1.setText("A HOUSE");
                    ans2.setText("A WOODEN PERCUSSIAN INSTRUMENT");
                    ans3.setText("A HOME");
                    ans4.setText("A PERSON");

                }
                if (count == 24) {
     //               MOA CHUNBI SOGI CAN CONVEY? A SENSE OF CALM, tgvw sxz, A SENSE OF TIME, LOVE & TENDERNESS, A SENSE OF HOPE, FAITH & GLORY, A SENSE OF HEART, FEAR & HONESTY
                    img1.setVisibility(View.GONE);
                    ques.setText("MOA CHUNBI SOGI CAN CONVEY?");
                    ans1.setText("A SENSE OF CALM");
                    ans2.setText("A SENSE OF TIME, LOVE & TENDERNESS");
                    ans3.setText("A SENSE OF HOPE, FAITH & GLORY");
                    ans4.setText("A SENSE OF HEART, FEAR & HONESTY");

                }

                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if (count == 1) {
                    img2.setVisibility(View.INVISIBLE);
                    ques.setText("What is 2 step sparring in Korean?");
                    ans1.setText("IBO MATSOKI");
                    ans2.setText("ELBOW MATSOKI");
                    ans3.setText("ILBO MATSOKI");
                    ans4.setText("JAO MATSOKI");

                }
                if (count == 2) {
                    ques.setText("What type of student is 2 Step sparring for?");
                    ans1.setText("Beginner");
                    ans2.setText("Intermediate");
                    ans3.setText("Experienced");
                    ans4.setText("None of the above");

                }
                if (count == 3) {
                    ques.setText("What is Bending stance in Korean?");
                    ans1.setText(" GUBURYO SOGI");
                    ans2.setText("ILBO SOGI");
                    ans3.setText("DWIT BAL SOGI");
                    ans4.setText("GOJONG SOGI");


                }
                if (count == 4) {
                    ques.setText("What is Fixed stance in Korean?");
                    ans1.setText("Gojong Sogi");
                    ans2.setText("Dwit Bal sogi");
                    ans3.setText("Annun Sogi");
                    ans4.setText("Gunnun Sogi");

                }
                if (count == 5) {
                    ques.setText(" What is body weight for fixed stance?");
                    ans1.setText("60/40");
                    ans2.setText("90/10");
                    ans3.setText("50/50");
                    ans4.setText("70/30");


                }
                if (count == 6) {
                    ques.setText("What is body weight for Niunja Sogi?");
                    ans1.setText("60/40");
                    ans2.setText("50/50");
                    ans3.setText("90/10");
                    ans4.setText("70/30");

                }
                if (count == 7) {
                    ques.setText("Which of these are a hand part?");
                    ans1.setText("Yi JOOMUK");
                    ans2.setText("DUNK JOOMUK");
                    ans3.setText("TOP JOOMUK");
                    ans4.setText("BANDALSON");

                }
                if (count == 8) {
                    ques.setText("What is pattern diagram for Won-Hyo?");
                    ans1.setText("I");
                    ans2.setText("S");
                    ans3.setText("X");
                    ans4.setText("W");

                }
                if (count == 9) {
                    ques.setText(" What is starting position for Won-Hyo?");
                    ans1.setText("CLOSED READY STANCE A (MOA CHUNBI SOGI)");
                    ans2.setText("L-STANCE TWIN FOREARM BLOCK (SANG PALMOK MAKGI)");
                    ans3.setText("L-STANCE INWARD KNIFEHAND STRIKE (ANAERO SONKUL TAERIGI)");
                    ans4.setText("None of the above");



                }
                if (count == 10) {
                    ques.setText("What is DWIT CHAGI in English?");
                    ans1.setText("Back Kick");
                    ans2.setText("Turning Kick");
                    ans3.setText("Side Kick");
                    ans4.setText("Front Rising Kick");

                }
                if (count == 11) {
                    ques.setText("What do the meanings of patterns refer to?");
                    ans1.setText("AN HISTORICAL EVENT");
                    ans2.setText("GENERAL CHOI BIRTHDAY");
                    ans3.setText("A SCHOOL");
                    ans4.setText("A GRADING");

                }
                if (count == 12) {
                    ques.setText("What do you do for your TAEKWON-DO school?");
                    ans1.setText("HELP WHERE POSSIBLE");
                    ans2.setText("NOTHING");
                    ans3.setText("NOT TRY");
                    // ans4.setText("Bakaero");

                }
                if (count == 13) {
                    ques.setText("How many move in Won-Hyo?");
                    ans1.setText("29");
                    ans2.setText("31");
                    ans3.setText("26");
                    ans4.setText("28");

                }
                if (count == 14) {
                    ques.setText("Name one foot part in Korean?");
                    ans1.setText("AP KUMCHI");
                    ans2.setText("BALKAT");
                    ans3.setText("DWITCH CHOOK");
                    ans4.setText("BALDEEP");

                }
                if (count == 15) {
                    ques.setText("What is JAO MATSOKI in English?");
                    ans1.setText("1 Step sparring");
                    ans2.setText("Free sparring");
                    ans3.setText("2 step sparring");
                    ans4.setText("3 step sparring");

                }
                if (count == 16) {
                    ques.setText("How many moves in Chon-Ji");
                    ans1.setText("19");
                    ans2.setText("16");
                    ans3.setText("14");
                    ans4.setText("22");


                }if (count == 17) {
                    ques.setText("How many movements in Dan-Gun? ");
                    ans1.setText("23");
                    ans2.setText("21");
                    ans3.setText("19");
                    ans4.setText("24");

                } if (count == 18) {
                    ques.setText("What is last stance performed in Do-San called in English?");
                    ans1.setText("Rear Foot Stance");
                    ans2.setText("Low Stance");
                    ans3.setText("X Stance");
                    ans4.setText("Sitting Stance");

                }
                if (count == 19) {
                    ques.setText("What is Charyot in English?");
                    ans1.setText("Sitting");
                    ans2.setText("Jumping");
                    ans3.setText("Attention");
                    ans4.setText("Wen");

                } if (count == 20) {
                    ques.setText("WON-HYO WAS ?");
                    ans1.setText("A Monk");
                    ans2.setText("A Film star");
                    ans3.setText("A politician");
//                    ans4.setText("22");

                }
                if (count == 21) {
                    ques.setText("WON-HYO WAS? A SCHOOLAR TRUE OR FALSE?");
                    ans1.setText("True");
                    ans2.setText("False");
//                    ans3.setText("14");
//                    ans4.setText("22");

                }
                if (count == 22) {
                    ques.setText(" Moa Chunbi Sogi is often referred to as what?");
                    ans1.setText("REPRESENTING WAR/PEACE");
                    ans2.setText("REPRESENTING LOVE");
                    ans3.setText("REPRESENTING HAPPINESS");
                    ans4.setText("REPRESENTING FREEDOM");

                }
                if (count == 23) {
                    ques.setText("A MOKTAK WAS?");
                    ans1.setText("A HOUSE");
                    ans2.setText("A WOODEN PERCUSSIAN INSTRUMENT");
                    ans3.setText("A HOME");
                    ans4.setText("A PERSON");

                }
                if (count == 24) {
                    //               MOA CHUNBI SOGI CAN CONVEY? A SENSE OF CALM, tgvw sxz, A SENSE OF TIME, LOVE & TENDERNESS, A SENSE OF HOPE, FAITH & GLORY, A SENSE OF HEART, FEAR & HONESTY
                    img1.setVisibility(View.GONE);
                    ques.setText("MOA CHUNBI SOGI CAN CONVEY?");
                    ans1.setText("A SENSE OF CALM");
                    ans2.setText("A SENSE OF TIME, LOVE & TENDERNESS");
                    ans3.setText("A SENSE OF HOPE, FAITH & GLORY");
                    ans4.setText("A SENSE OF HEART, FEAR & HONESTY");

                }

                if(count ==24){

                    img1.setVisibility(View.GONE);

                }else{
                    img1.setVisibility(View.VISIBLE);        }


            }
        });


    }
}
