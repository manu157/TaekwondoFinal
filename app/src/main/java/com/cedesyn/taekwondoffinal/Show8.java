package com.cedesyn.taekwondoffinal;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jerin android on 08-03-2018.
 */

public class Show8 extends AppCompatActivity {
    ImageView img1,img2;
    int count=1;
    Button btn;
    TextView ques,ans1,ans2,ans3,ans4;
    ConstraintLayout snackbar8;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show8);
        snackbar8=(ConstraintLayout)findViewById(R.id.snacks8);

        btn=(Button)findViewById(R.id.ans8);


        ques=(TextView)findViewById(R.id.qstn8);
        ans1=(TextView) findViewById(R.id.o18);
        ans2=(TextView) findViewById(R.id.o28);
        ans3=(TextView) findViewById(R.id.o38);
        ans4=(TextView) findViewById(R.id.o48);

        img1=(ImageView)findViewById(R.id.ryt);
        img2=(ImageView)findViewById(R.id.left);String fontPath = "ArialBold.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        ques.setTypeface(tf);
        ans1.setTypeface(tf);
        ans2.setTypeface(tf);
        ans3.setTypeface(tf);
        ans4.setTypeface(tf);
        if (count==1){
            img2.setVisibility(View.GONE);
        }
        count =1;
        ques.setText("RED SIGNIFIES?");
        ans1.setText("DANGER");
        ans2.setText("BEGINNER");
        ans3.setText("GROWTH");
        ans4.setText("HEAVEN");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ;
                if (count==1){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "DANGER", Snackbar.LENGTH_LONG);
                    snackBar8.show();
                }

                if(count==2){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "MATURITY & PROFICIENCY", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                } if(count==3){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "WEARERS IMPERVIOUSNESS TO DARKNESS AND FEAR", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                } if(count==4){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "29", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                } if(count==5){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Hwa Rang Youth Group", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                } if(count==6){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Silla Dynasty", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                } if(count==7){
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "600 A.D.", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                } if(count==8) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "The 29th Infantry division", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }
                if(count==9) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Maturity", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }
                if(count==10) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "I", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==11) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Palm Pushing Block", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==12) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Downward Knifehand Strike", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==13) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Answer not mentioned", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }
                if(count==14) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Kyocha Joomuk Noollo makgi", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }
                if(count==15) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Mitkulgi", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }
                if(count==16) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Not mentioned", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==17) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Obverse ", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==18) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "C", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==19) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Impervious to Darkness & Fear", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==20) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Maturity and Proficiency", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==21) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "To the King", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }if(count==22) {
                    Snackbar snackBar8 = Snackbar.make(snackbar8, "Their friends", Snackbar.LENGTH_LONG);
                    snackBar8.show();

                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count+1;
                if(count==1){ img1.setVisibility(View.GONE);
                    ques.setText("RED SIGNIFIES?");
                    ans1.setText("DANGER");
                    ans2.setText("BEGINNER");
                    ans3.setText("GROWTH");
                    ans4.setText("HEAVEN");

                } if(count==2){
                    ques.setText("BLACK SIGNIFIES?");
                    ans1.setText("MATURITY & PROFICIENCY");
                    ans2.setText("DANGER & KNOWLEDGE");
                    ans3.setText("GROWTH & MATURITY");
                    ans4.setText("PROFICIENCY & KNOWLEDGE");

                } if(count==3){
                    ques.setText("BLACK INDICATES?");
                    ans1.setText("WEARERS SKILL TO PUNCH & STRIKE");
                    ans2.setText("WEARERS IMPERVIOUSNESS TO DARKNESS AND FEAR");
                    ans3.setText("WEARERS ABILITY TO WATCH & LEARN");
                    ans4.setText("WEARERS IMPERVIOSNESS TO DARKNESS & LIGHT");

                } if(count==4){
                    ques.setText("HOW MANY MOVEMENTS IN PATTERN HWA RANG?");
                    ans1.setText("28");
                    ans2.setText("29");
                    ans3.setText("30");
                    ans4.setText("37");

                } if(count==5){
                    ques.setText("HWA-RANG is named after which youth group?");
                    ans1.setText("Dang Wang Youth Group");
                    ans2.setText("Hwa Rang Youth Group");
                    ans3.setText("Rang Wang Youth Group");
                    ans4.setText("The Youth Group");


                }

                if(count==6){
                    ques.setText("Which dynasty did the Hwa Rang originate?");
                    ans1.setText("Guburyeo");
                    ans2.setText("Bekju");
                    ans3.setText("Silla Dynasty");
                    ans4.setText("None of the above");

                } if(count==7){
                    ques.setText("Which year did the Hwa Rang originate?");
                    ans1.setText("700 A.D");
                    ans2.setText("400 A.D");
                    ans3.setText("900 A.D");
                    ans4.setText("600 A.D");

                } if(count==8) {
                    ques.setText("The 29 movements of this pattern refer to what?");
                    ans1.setText("The light division");
                    ans2.setText("The Football Division");
                    ans3.setText("The 29th Infantry division");
                    ans4.setText("None of the above");

                }if(count==9) {
                    ques.setText("The 29th Infantry division taekwondo developed what?");
                    ans1.setText("Speed");
                    ans2.setText("Grace");
                    ans3.setText("Timing");
                    ans4.setText("Maturity");

                }if(count==10) {
                    ques.setText("What is the PATTERN DIAGRAM for Hwa Rang?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("S");
                    ans4.setText("I");

                }if(count==11) {
                    ques.setText("What is the 1st move of Hwa-Rang?");
                    ans1.setText("Palm Pushing Block");
                    ans2.setText("Forearm Block");
                    ans3.setText("Upset fingertip thrust");
                    ans4.setText("None of the above");

                }if(count==12) {
                    ques.setText("What is Naeryo Sonkal Taerigi in English?");
                    ans1.setText("Upward strike");
                    ans2.setText("Knifehand block");
                    ans3.setText("upward knifehand strike");
                    ans4.setText("Downward Knifehand Strike");

                }if(count==13) {
                    ques.setText("What is the Korean for the upward punch in Hwa-Rang?");
                    ans1.setText("Ollyo Jirugi");
                    ans2.setText("Nollyo Makgi");
                    ans3.setText("Nollyo Jirugi");
                    ans4.setText("ollyo Tulgi");


                }if(count==14) {
                    ques.setText("What is the Korean for “X” fist pressing block?");
                    ans1.setText("Kyocha Joomuk Noollo makgi");
                    ans2.setText("Kyocha Joomuk");
                    ans3.setText("Ollyo Makgi");
                    ans4.setText("Kyocha Joomuk Ollyo Jirugi");

                }if(count==15) {
                    ques.setText("What is the Korean for sliding & explain it?");
                    ans1.setText("Mitkulgi");
                    ans2.setText("Dwitchook");
                    ans3.setText("DwitKumchee");
                    ans4.setText("Baldung");

                }if(count==16) {
                    ques.setText("Which is not a foot part?");
                    ans1.setText("Joomuk");
                    ans2.setText("Apkumchi");
                    ans3.setText("Foot sword");
                    ans4.setText("Baldung");

                }if(count==17) {
                    ques.setText("When performing L-stance & punch in Hwa-Rang. Is it obverse or reverse?");
                    ans1.setText("Obverse");
                    ans2.setText("reverse");
//                    ans3.setText("DwitKumchee");
//                    ans4.setText("Baldung");

                }if(count==18) {
                    ques.setText("What is the ready position in Hwa-Rang?");
                    ans1.setText("C");
                    ans2.setText("B");
                    ans3.setText("A");
                    ans4.setText("D");

                }if(count==19) {
                    ques.setText("What does the colour Black signify?");
                    ans1.setText("Love & Peace");
                    ans2.setText("Impervious to Honour & reward");
                    ans3.setText("Impervious to Danger & Darkness");
                    ans4.setText("Impervious to Darkness & Fear");

                }if(count==20) {
                    ques.setText("What does Black signify?");
                    ans1.setText("Maturity & Love");
                    ans2.setText("Maturity & Honour");
                    ans3.setText("Darkness & Light");
                    ans4.setText("Maturity and Proficiency");

                }if(count==21) {
                    ques.setText("Who were the HWA RANG loyal to?");
                    ans1.setText("To the King");
                    ans2.setText("The Gaffer");
                    ans3.setText("The Queen");
                    ans4.setText("The Scholar");

                }if(count==22) { img1.setVisibility(View.GONE);
                    ques.setText("Who did the HWA RANG have honour with?");
                    ans1.setText("Their sisters");
                    ans2.setText("Their dads");
                    ans3.setText("Their friends");
                    ans4.setText("Their lovers");

                }
                if(count<=1){
                    img2.setVisibility(View.GONE);
                }else{
                    img2.setVisibility(View.VISIBLE);
                }

            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count=count-1;
                if(count==1){ img1.setVisibility(View.GONE);
                    ques.setText("RED SIGNIFIES?");
                    ans1.setText("DANGER");
                    ans2.setText("BEGINNER");
                    ans3.setText("GROWTH");
                    ans4.setText("HEAVEN");

                } if(count==2){
                    ques.setText("BLACK SIGNIFIES?");
                    ans1.setText("MATURITY & PROFICIENCY");
                    ans2.setText("DANGER & KNOWLEDGE");
                    ans3.setText("GROWTH & MATURITY");
                    ans4.setText("PROFICIENCY & KNOWLEDGE");

                } if(count==3){
                    ques.setText("BLACK INDICATES?");
                    ans1.setText("WEARERS SKILL TO PUNCH & STRIKE");
                    ans2.setText("WEARERS IMPERVIOUSNESS TO DARKNESS AND FEAR");
                    ans3.setText("WEARERS ABILITY TO WATCH & LEARN");
                    ans4.setText("WEARERS IMPERVIOSNESS TO DARKNESS & LIGHT");

                } if(count==4){
                    ques.setText("HOW MANY MOVEMENTS IN PATTERN HWA RANG?");
                    ans1.setText("28");
                    ans2.setText("29");
                    ans3.setText("30");
                    ans4.setText("37");

                } if(count==5){
                    ques.setText("HWA-RANG is named after which youth group?");
                    ans1.setText("Dang Wang Youth Group");
                    ans2.setText("Hwa Rang Youth Group");
                    ans3.setText("Rang Wang Youth Group");
                    ans4.setText("The Youth Group");


                }

                if(count==6){
                    ques.setText("Which dynasty did the Hwa Rang originate?");
                    ans1.setText("Guburyeo");
                    ans2.setText("Bekju");
                    ans3.setText("Silla Dynasty");
                    ans4.setText("None of the above");

                } if(count==7){
                    ques.setText("Which year did the Hwa Rang originate?");
                    ans1.setText("700 A.D");
                    ans2.setText("400 A.D");
                    ans3.setText("900 A.D");
                    ans4.setText("600 A.D");

                } if(count==8) {
                    ques.setText("The 29 movements of this pattern refer to what?");
                    ans1.setText("The light division");
                    ans2.setText("The Football Division");
                    ans3.setText("The 29th Infantry division");
                    ans4.setText("None of the above");

                }if(count==9) {
                    ques.setText("The 29th Infantry division taekwondo developed what?");
                    ans1.setText("Speed");
                    ans2.setText("Grace");
                    ans3.setText("Timing");
                    ans4.setText("Maturity");

                }if(count==10) {
                    ques.setText("What is the PATTERN DIAGRAM for Hwa Rang?");
                    ans1.setText("X");
                    ans2.setText("T");
                    ans3.setText("S");
                    ans4.setText("I");

                }if(count==11) {
                    ques.setText("What is the 1st move of Hwa-Rang?");
                    ans1.setText("Palm Pushing Block");
                    ans2.setText("Forearm Block");
                    ans3.setText("Upset fingertip thrust");
                    ans4.setText("None of the above");

                }if(count==12) {
                    ques.setText("What is Naeryo Sonkal Taerigi in English?");
                    ans1.setText("Upward strike");
                    ans2.setText("Knifehand block");
                    ans3.setText("upward knifehand strike");
                    ans4.setText("Downward Knifehand Strike");

                }if(count==13) {
                    ques.setText("What is the Korean for the upward punch in Hwa-Rang?");
                    ans1.setText("Ollyo Jirugi");
                    ans2.setText("Nollyo Makgi");
                    ans3.setText("Nollyo Jirugi");
                    ans4.setText("ollyo Tulgi");


                }if(count==14) {
                    ques.setText("What is the Korean for “X” fist pressing block?");
                    ans1.setText("Kyocha Joomuk Noollo makgi");
                    ans2.setText("Kyocha Joomuk");
                    ans3.setText("Ollyo Makgi");
                    ans4.setText("Kyocha Joomuk Ollyo Jirugi");

                }if(count==15) {
                    ques.setText("What is the Korean for sliding & explain it?");
                    ans1.setText("Mitkulgi");
                    ans2.setText("Dwitchook");
                    ans3.setText("DwitKumchee");
                    ans4.setText("Baldung");

                }if(count==16) {
                    ques.setText("Which is not a foot part?");
                    ans1.setText("Joomuk");
                    ans2.setText("Apkumchi");
                    ans3.setText("Foot sword");
                    ans4.setText("Baldung");

                }if(count==17) {
                    ques.setText("When performing L-stance & punch in Hwa-Rang. Is it obverse or reverse?");
                    ans1.setText("Obverse");
                    ans2.setText("reverse");
//                    ans3.setText("DwitKumchee");
//                    ans4.setText("Baldung");

                }if(count==18) {
                    ques.setText("What is the ready position in Hwa-Rang?");
                    ans1.setText("C");
                    ans2.setText("B");
                    ans3.setText("A");
                    ans4.setText("D");

                }if(count==19) {
                    ques.setText("What does the colour Black signify?");
                    ans1.setText("Love & Peace");
                    ans2.setText("Impervious to Honour & reward");
                    ans3.setText("Impervious to Danger & Darkness");
                    ans4.setText("Impervious to Darkness & Fear");

                }if(count==20) {
                    ques.setText("What does Black signify?");
                    ans1.setText("Maturity & Love");
                    ans2.setText("Maturity & Honour");
                    ans3.setText("Darkness & Light");
                    ans4.setText("Maturity and Proficiency");

                }if(count==21) {
                    ques.setText("Who were the HWA RANG loyal to?");
                    ans1.setText("To the King");
                    ans2.setText("The Gaffer");
                    ans3.setText("The Queen");
                    ans4.setText("The Scholar");

                }if(count==22) {
                    img1.setVisibility(View.GONE);
                    ques.setText("Who did the HWA RANG have honour with?");
                    ans1.setText("Their sisters");
                    ans2.setText("Their dads");
                    ans3.setText("Their friends");
                    ans4.setText("Their lovers");

                }
                if(count==22){
                    img1.setVisibility(View.GONE);
                }else{
                    img1.setVisibility(View.VISIBLE);
                }
            }
        });


    }
}
