package com.cedesyn.taekwondoffinal;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

/**
 * Created by jerin android on 31-01-2018.
 */

public class History extends AppCompatActivity {
    TextView Hist,histor;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        Hist =(TextView)findViewById(R.id.histor);
        Hist.setMovementMethod(new ScrollingMovementMethod());
histor=(TextView)findViewById(R.id.histo);
        String fontPath = "ArialBold.ttf";

//        // Loading Font Face
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
//        // Applying font
        Hist.setTypeface(tf);
        histor.setTypeface(tf);
        histor.setText("\n‘Taekwondo (ITF and WTF)'\n");
        Hist.setMovementMethod(new ScrollingMovementMethod());
        Hist.setText(
                " Taekwondo was originally developed in Korea in the 1950s. Derived from several martial arts, with its core being tae-kwon, or Korean kick fighting, its key focus is to swiftly overcome an opponent with the use of hands and feet. The art was officially formed when a group of leading martial arts experts came together to unify their respective disciplines under a single fighting system. This inauguration took place in South Korea on 11 April 1955 with Major General Choi Hong Hi, a 9th Dan black belt, being the founder. Its roots stretch back nearly 2,000 years to an art called Hwarangdo practiced by a force of handpicked elite young loyalists during Korea's Silla Dynasty around 600 AD. Learning taekwondo became compulsory for all young men towards the end of the 10th century, but fell out of favour in the 16th century and was only kept alive by Buddhist monks. The suppression of martial arts during the Japanese occupation in 1909 furthered its decline. Following Korea's liberation in 1945, many Korean exiles returned home and introduced an improved version of taekwondo. The government, as part of its campaign to reassert national identity, officially endorsed taekwondo. Taekwondo spread worldwide from Korea in the 1960s and has been listed as an Olympic sport since 1988. Training consists of a variety of punching, kicking, dodging, jumping, parrying, and blocking techniques. Karate practices also include the splitting of wood, and the smashing of bricks and tiles with bare hands or feet. Other forms of training include sparring and learning formalised patterns of movement called hyung (similar to karate's kata). Competition is optional. Practitioners wear the gi, a plain white heavy cotton suit with a coloured belt denoting the wearer's skill level. \n" +
                        "Taekwondo is split into 2 main styles: International Taekwondo Federation (ITF) and World Taekwondo Federation (WTF). ITF fighters wear pads on hands, feet, head, groin, and shins. Punches and kicks are delivered with controlled contact (semi-contact) to the body and head. WTF fighters wear body armourer and deliver full contact punches and kicks to the body and controlled kicks to the head. The patterns or Hyung/Kata are different in each form. WTF Taekwondo has a great emphasis on sport applications. Interestingly, in large part, Taekwondo came to the UK with the Royal Air Force in the late 60s and early 70s as a result of the exposure of serving personnel to practitioners abroad.\n" +
                        "ITF/WTF split occurred in the 50's, shortly after TKD was officially recognized as a martial art form. General Choi, Hong Hi is historically recognized as the \"father\" of TKD, although when he insisted that he be allowed to share his Korean Martial Art with ALL Koreans, which would have included North Korea (he hailed from South Korea), South Korea disavowed him and the WTF was formed. WTF is typically what you will hear referred to as the \"official\" national sport martial Art of South Korea, and it is the sport you will see in the Olympic games. \n" +
                        "\n" +
                        "ITF forms are referred to as TUL, WTF as POOMSAE. ITF schools teach the original 24 patterns created by General Choi, and put a GREAT deal of emphasis on technique and control. The philosophy behind this motivation is that a practitioner who can control his or her technique to the point that a properly executed technique, performed at full speed, can be stopped within inches of striking the target, will be more effective than one who has learned only to execute the technique \"through\" the target (e.g. throw a punch, blow out a candle, but don't touch the flame trick). WTF forms, to my understanding, are a requirement for advancement, but are less complex than ITF forms, and fewer in number (10 patterns that I have been able to find in research).\n" +
                        "\n" +
                        "Ranking is slightly different as well. ITF students progress from 10 gup (white belt) to 1 gup (red belt/black tip). Typically it takes an ITF student no fewer than three years to attain 1st Dan (novice black belt). WTF students progress from 10 geup to 1 geup (note the slight difference in spelling), and start at yellow, with 1 geup being red belt with 3 black stripes. I cannot attest to the time frame for WTF advancement.\n" +
                        "\n" +
                        "There are currently two internationally recognized ITF hierarchies; one based in Canada, one based in Austria, both claiming to have true lineage to General Choi. the rift in the ITF ocurred when ITF practitioners clashed over the direction of the federation leadership. Regardless, both ITF organizations teach the original 24 patterns,and both espouse the philosophy that General Choi intended, although each in their own way. \n" +
                        "\n" +
                        "Sparring is markedly different between ITF and WTF. ITF sparring rules are very specific to require participants to exercise control and technique; points are in fact deducted, and CAN result in the forfieture of a match, if one paerson hits the other hard enough to cause them not to be able to continue. Once again, ITF emphasises CONTROL. \n" +
                        "\n" +
                        "WTF sparring is full contact, and points are in fact awarded for knockouts. \n" +
                        "\n" +
                        "The philosophical difference between having the abillity to blast through a target vs. the ability to stop just short of it has been, is, and will be debated here and everywhere as to which is more effective. However, to throw my two cents worth in, consider this: Which takes more strength - throwing a full force punch or kick that contacts an opponent with the desired result, or having the strength to throw that same full force technique, but stop it just short of contact?\n");

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(History.this,MainActivity.class));
        finish();
    }
}
